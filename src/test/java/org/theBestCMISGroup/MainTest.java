package org.theBestCMISGroup;

import org.junit.jupiter.api.Test;
import org.theBestCMISGroup.Utils.OutputCapture;

import static org.junit.jupiter.api.Assertions.assertTrue;

class MainTest {

    @SuppressWarnings("ConfusingMainMethod")
    //this is to test if "main" outputs "Hello world!" to console at any point.
    //it is an example
    @Test
    void main() {
        String output = OutputCapture.captureOutDo(() -> Main.main(new String[0]));
        assertTrue(output.contains("Hello world!"));
    }
}