package org.theBestCMISGroup;

import org.theBestCMISGroup.FileManagement.ConfigManagement.ConfigLoader;
import org.theBestCMISGroup.Mapping.Map;
import org.theBestCMISGroup.People.PersonSpawner;
import org.theBestCMISGroup.Statistics.DataCollection.DataCollector;
import org.theBestCMISGroup.Utils.MiscDataTypes.Distribution;
import org.theBestCMISGroup.Utils.Simulation.Simulation;

import java.text.ParseException;
import java.util.Date;

public class Traffic implements Simulation.Simulatable {

    public final Map map;
    public final DataCollector data;
    public final Distribution.RandomDistribution arrivalTime;
    public final PersonSpawner personSpawner;

    public Traffic(){
        map=Map.loadMapWhateverConfigIsAvailable();
        try {
            arrivalTime = Distribution.RandomDistribution.parse(ConfigLoader.getConfig().get(4));
        } catch (ParseException e) {throw new RuntimeException(e);}
        data=new DataCollector();
        personSpawner=new PersonSpawner(map, data);
    }
    @Override
    public void tick(){
        map.tickAllRoads();
    }
    @Override
    public void randomDo(){
        personSpawner.spawn();
    }
    @Override
    public Date nextEvent(){
        return null; //this returns null because we don't want to spend the processing power to calculate the next major event.
    }
    @Override
    public void startOrResumeSim(){}
    @Override
    public void finishSim(){
        data.calculate();
    }

}
