package org.theBestCMISGroup.Utils;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.function.Consumer;

public class OutputCapture {

    public static String captureOutDo(Runnable action) {
        OutputCapturer capturer = new OutputCapturer();
        capturer.startCapture();
        action.run();
        capturer.stopCapture();
        return capturer.getCaptured();
    }
    public static String copyOutDo(Runnable action) {
        OutputCopyer copyer = new OutputCopyer();
        copyer.startCopy();
        action.run();
        copyer.stopCopy();
        return copyer.getCopy();
    }
    public static class OutputCapturer{
        PrintStream realout;
        final FakePrintStream fakeOut;
        public OutputCapturer() {
            this((ignored)->{});
        }
        public OutputCapturer(Consumer<Character> onOutputChar) {
            fakeOut = new FakePrintStream(onOutputChar);
        }
        public void stopCapture() {
            System.setOut(realout);
        }
        public void startCapture() {
            realout = System.out;
            System.setOut(new PrintStream(fakeOut));
        }
        public String getCaptured() {
            return fakeOut.getStrung();
        }
    }
    public static class OutputCopyer extends OutputCapturer{
        public OutputCopyer() {
            this((ignored)->{});
        }
        public OutputCopyer(Consumer<Character> onOutputChar) {
            super((character) -> {
                System.out.write((int)character);
                onOutputChar.accept(character);
            });
        }
        public void startCopy() {
            super.startCapture();
        }
        public void stopCopy() {
            super.stopCapture();
        }
        public String getCopy() {
            return super.getCaptured();
        }
    }
    static class FakePrintStream extends PrintStream {
        final FakeOutputStream fakeOut;
        final Consumer<Character> onWrite;
        public FakePrintStream() {
            this((ignored)->{});
        }
        public FakePrintStream(Consumer<Character> onWrite) {
            super(new FakePrintStream.FakeOutputStream(onWrite));
            fakeOut = (FakePrintStream.FakeOutputStream) super.out;
            this.onWrite=onWrite;
        }

        public String getStrung() {
            return fakeOut.getStrung();
        }

        static class FakeOutputStream extends OutputStream {
            final Consumer<Character> onWrite;

            public FakeOutputStream() {
                this((ignored)->{});
            }
            public FakeOutputStream(Consumer<Character> onWrite) {
                this.onWrite=onWrite;
            }

            @Override
            public void write(int b) {
                strung=(getStrung() + (char) b);
                onWrite.accept((char) b);
            }

            String strung = "";

            public String getStrung() {
                return strung;
            }
        }
    }
}