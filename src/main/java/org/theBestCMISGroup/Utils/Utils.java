package org.theBestCMISGroup.Utils;

import java.util.Date;
import java.util.List;

public class Utils {
    public static int indexOfClosestExistant(String searchMe, String subSt1, String subSt2) {
        if (searchMe.contains(subSt1)) {
            if (searchMe.contains(subSt2)) {
                //both
                return Math.min(searchMe.indexOf(subSt1),searchMe.indexOf(subSt2));
            }
            //just 1
            return searchMe.indexOf(subSt1);
        }
        //either 2 or neither. only neither returns -1.
        return searchMe.indexOf(subSt2);
    }
    public static long timeOfDayMS(Date date) {
        return date.getTime()%(1000*60*60*24);
    }

    public static String join(List<String> strings) {
        return join(strings,"");
    }
    public static String join(List<String> strings,String delimiter) {
        StringBuilder ret = new StringBuilder();
        for (String string:strings) {
            ret.append(string).append(delimiter);
        }
        ret = new StringBuilder(ret.substring(0, ret.length() - 1));
        return ret.toString();
    }
}
