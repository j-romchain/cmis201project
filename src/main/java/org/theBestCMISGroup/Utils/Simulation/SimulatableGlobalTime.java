package org.theBestCMISGroup.Utils.Simulation;

import java.util.Date;

public class SimulatableGlobalTime {
    private SimulatableGlobalTime() {}
    public static void simulateNow(){
        time = new Date();
    }
    public static void stopTime(){
        time = new Date();
    }
    private static Date time;
    public static Date getTime() {
        if (sim) {
            return time;
        } else {
            return new Date();
        }
    }
    public static void simulateTime(Date time) {
        assert time!=null;
        sim=true;
        SimulatableGlobalTime.time = time;
    }
    private static boolean sim = false;
    public static void unSim() {
        sim=false;
    }
    public static void reSim() {
        sim=true;
    }
    public static boolean isSim() {
        return sim;
    }
    public static void incrementTimeBy1Sec() {
        incrementTime(1000);
    }
    public static void incrementTimeBy1Min() {
        incrementTime(60*1000);
    }
    public static void incrementTimeBy1Hr() {
        incrementTime(60*60*1000);
    }
    public static void incrementTimeBy1Day() {
        incrementTime(24*60*60*1000);
    }
    public static void incrementTimeBy1Year() {
        incrementTime((long)(365.25*24d*60d*60d*1000d));
    }
    public static void incrementTime(long millisecIncrement) {
        sim=true;
        time=new Date(getTime().getTime()+millisecIncrement);
    }
    public static Date simTimeDo(Date simTime,Runnable action) {//does something in a simulated time and then returns to whatever the previous state was. Returns the simtime at the end of the action.
        Date prevTime = time;
        boolean wasSim = sim;
        simulateTime(simTime);
        action.run();
        Date endTime = getTime();
        time=prevTime;
        sim=wasSim;
        return getTime();
    }
}
