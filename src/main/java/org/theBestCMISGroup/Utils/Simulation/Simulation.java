package org.theBestCMISGroup.Utils.Simulation;

import org.theBestCMISGroup.FileManagement.Utils.SVSaveManager;
import org.theBestCMISGroup.Utils.MiscDataTypes.Distribution;
import org.theBestCMISGroup.Utils.Storage.EasyList;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

public class Simulation<T extends Simulation.Simulatable> { // great frederick System.
    Date simTil;
    final long timeIncrement;
    boolean fixedIncrement;
    Date simDate;
    final T simulated;
    final Distribution.RandomDistribution randomOccuranceDistribution;
    public interface Simulatable {
        void tick();
        void randomDo();
        Date nextEvent();
        void startOrResumeSim();
        void finishSim();
    }

//
//    Distribution arrivalDistribution;
//    Processor<Job> ssq;
//    Queue<Job> jobs = new Queue<>();
    public Simulation(File simConfig, T simMe) throws FileNotFoundException, ParseException {
        this(SVSaveManager.load(simConfig),simMe);

    }
    public Simulation(EasyList<EasyList<String>> parsedSimConfig, T simMe) throws ParseException {
        this(new SimpleDateFormat().parse(parsedSimConfig.get(0).get(0)),Long.parseLong(parsedSimConfig.get(0).get(1)), Distribution.RandomDistribution.parse(parsedSimConfig.get(1)), simMe);
    }
    public Simulation(Date startDate, long timeIncrement, Distribution.RandomDistribution randomOccuranceDistribution, T simMe){
        this.simDate=startDate;
        this.simTil=simDate;
        this.timeIncrement=timeIncrement;
        this.fixedIncrement=true;
        this.simulated=simMe;
        this.randomOccuranceDistribution=randomOccuranceDistribution;
    }
    public Simulation(Date startDate, Distribution.RandomDistribution randomOccuranceDistribution, T simMe){
        this(startDate,-1,randomOccuranceDistribution,simMe);
        fixedIncrement=false;
    }
    public Simulation(Date startDate, long timeIncrement, Distribution.RandomDistribution randomOccuranceDistribution, T simMe, long simLength)  {
        this(startDate,timeIncrement,randomOccuranceDistribution,simMe);
        try {
            runFixedIncrement(simLength);
        } catch (NoIncrementException e) {
            throw new RuntimeException("this should be impossible.");
        }
    }
    public Simulation(Date startDate, Distribution.RandomDistribution randomOccuranceDistribution, T simMe, long simLength) {
        this(startDate,randomOccuranceDistribution,simMe);
        runAutoIncrement(simLength);
    }
    public Simulation(Date startDate, long timeIncrement, T simMe){
        this.simDate=startDate;
        this.simTil=simDate;
        this.timeIncrement=timeIncrement;
        this.fixedIncrement=true;
        this.simulated=simMe;
        this.randomOccuranceDistribution=new Distribution.RandomDistribution.ExponentialDistribution(1);
    }
    public Simulation(Date startDate, T simMe){
        this(startDate,-1,simMe);
        fixedIncrement=false;
    }
    public Simulation(Date startDate, long timeIncrement, T simMe, long simLength) {
        this(startDate,timeIncrement,simMe);
        try {
            runFixedIncrement(simLength);
        } catch (NoIncrementException e) {
            throw new RuntimeException("this should be impossible.");
        }
    }
    public Simulation(Date startDate, T simMe, long simLength) {
        this(startDate,simMe);
        runAutoIncrement(simLength);
    }
    public static class NoIncrementException extends Exception {}
    public void runFixedIncrement(long simLength) throws NoIncrementException {
        if (timeIncrement<=0) {
            throw new NoIncrementException();
        }
        this.fixedIncrement=true;
        runLastIncrementSetting(simLength);
    }
    public void runAutoIncrement(long simLength) {
        this.fixedIncrement=false;
        runLastIncrementSetting(simLength);
    }

    public Date getTimeOfNextRandomDo() {
        return timeOfNextRandomDo;
    }

    public Date getSimTil() {
        return simTil;
    }

    public long getTimeIncrement() {
        return timeIncrement;
    }

    private void runLastIncrementSetting(long simLength) {
        simTimeRun(simulated::startOrResumeSim);
        simTil= new Date(simTil.getTime()+simLength);
        int loopsRun=0;
        while (simDate.before(simTil)) {
            loopsRun++;
            checkIfTimeForRandomDo();
            simTimeRun(simulated::tick);
            if (fixedIncrement&timeIncrement>0) {
                incrementTime(timeIncrement);
            } else {
                AtomicReference<Date> nextEvent = new AtomicReference<>();
                simTimeRun(() -> nextEvent.set(simulated.nextEvent()));
                if ((nextEvent.get() == null || nextEvent.get().getTime() > timeOfNextRandomDo.getTime()) && timeOfNextRandomDo.getTime() - simDate.getTime() <= 1000) {
                    simDate = timeOfNextRandomDo;
//                    System.out.println("going to next randomDo");
                    incrementTime(1);
                } else {
                    if (nextEvent.get() == null) {
                        incrementTime(1000);
//                        System.out.println("defaulting to 1 sec.");
                    } else {
                        simDate = nextEvent.get();
//                        System.out.println("going to next event");
                    }
                    incrementTime(1);
                }
            }
        }
        simTimeRun(simulated::finishSim);
    }
    private Date timeOfNextRandomDo =new Date(0);
    private void checkIfTimeForRandomDo() {
        if (!simDate.before(timeOfNextRandomDo)) {
            simTimeRun(simulated::randomDo);
            timeOfNextRandomDo = new Date(simDate.getTime()+(long)(randomOccuranceDistribution.sample()*1000));
        }
    }
    private void simTimeRun(Runnable runMe) {
        simDate=SimulatableGlobalTime.simTimeDo(simDate,runMe);
    }
    private void incrementTime(long ms) {
        this.simDate=new Date(simDate.getTime()+ms);
    }


}