package org.theBestCMISGroup.Utils;

import org.theBestCMISGroup.Utils.Storage.EasyList;
import org.theBestCMISGroup.Utils.Storage.Utils.ListContains;

import java.util.function.Function;

public class PathFinder<T extends PathFinder.OccupyableLocation<T>> {
    public EasyList<T> findPath(T startT, T endT, int maxRecursion) { //returns null if no path can be found by the way
        // first lets check if they are already right next to eachother, this will be important later, tag:gjkbgriur546otk,g0
        if (startT.getPossiblePreviousLocations().contains(endT)) {
            EasyList<T> ret = new EasyList<>();
            ret.add(startT);
            ret.add(endT);
            return ret;
        }
        // finds the path by looking at all connections of connections of connections until a common connection is found, and then running itself to fill in on either side, until both sides are completely full. kindof like picking up a whole rope by picking up both ends, and then the middle, and then the middle of each half, and then the middle of each half of a half, and so on, until the rope which is actually a chain, has all of its links being held. requires alot of hands, but it works.
        EasyList<EasyList<T>> startConnections = new EasyList<>();
        EasyList<EasyList<T>> endConnections = new EasyList<>();
        startConnections.add(new EasyList<>(startT));
        endConnections.add(new EasyList<>(endT));
        T closestCommonConnection = null;
        int recurses = 0;
        //while we haven't found the middle and we haven't exceeded the max recursions
        while (closestCommonConnection == null && recurses < maxRecursion) {
            //double check we haven't found it
            closestCommonConnection = ListContains.findOverlap2d(startConnections, endConnections);
            if (closestCommonConnection == null) {
                //if not, add a layer on the start    -- and end in a moment
                {
                    EasyList<EasyList<T>> connections = new EasyList<>();
                    int TCount = 0;
                    for (int i = 0; i < startConnections.get(recurses).length(); i++) {
                        connections.set(i,startConnections.get(recurses).get(i).getPossibleFutureLocations());
                        TCount += connections.get(i).length();
                    }
                    startConnections.set(recurses + 1, new EasyList<>());
                    for (EasyList<T> connection : connections) {
                        startConnections.get(recurses + 1).addAll(connection);
                    }
                }
                //and end
                {
                    EasyList<EasyList<T>> connections = new EasyList<> ();
                    int TCount = 0;
                    for (int i = 0; i < endConnections.get(recurses).length(); i++) {
                        connections.set(i,endConnections.get(recurses).get(i).getPossiblePreviousLocations());
                        TCount += connections.get(i).length();
                    }
                    endConnections.set(recurses + 1,new EasyList<>());
                    for (EasyList<T> connection : connections) {
                        endConnections.get(recurses+1).addAll(connection);
                    }
                }
                //make sure to increment recurses
                recurses++;
                //and remove all non-paths from the lists
                EasyList<T> startConnection = startConnections.get(recurses);
                for (int i = 0; i < startConnection.length(); i++) {
                    if (!startConnection.get(i).isTransversable()) {
                        //noinspection SuspiciousListRemoveInLoop
                        startConnection.remove(i);
                    }
                }
                EasyList<T> endConnection = endConnections.get(recurses);
                for (int i = 0; i < endConnection.length(); i++) {
                    if (!endConnection.get(i).isTransversable()) {
                        //noinspection SuspiciousListRemoveInLoop
                        endConnection.remove(i);
                    }
                }
            }
        }
        //it either reached the end of recursions or found something
        closestCommonConnection = ListContains.findOverlap2d(startConnections, endConnections);
        if (closestCommonConnection == null) {
            //not found
            return null;
        } else {
            //just fill in both sides around the middle, this is what that top part was for. tag:gjkbgriur546otk,g0
            EasyList<T> firstHalf = findPath(startT, closestCommonConnection, maxRecursion);
            EasyList<T> laststHalf = findPath(closestCommonConnection, endT, maxRecursion);
            EasyList<T> path = new EasyList<>();
            path.addAll(firstHalf);
            path.addAll(laststHalf);
            return path;
        }
    }

    public T findClosest(Function<T,Boolean> match, T origin, int maxSteps) {
        EasyList<EasyList<T>> booths = new EasyList<>(new EasyList<>(origin));
        int counter=-1;
        while (counter < 100) {
            counter++;
            //for 100 steps, init and then take the previous steps booths and add their toBooths to this step's booths
            booths.lengthenSet(counter+1,new EasyList<>());
            for (T branch : booths.get(counter)) {
                if (match.apply(branch)) {
                    //if the booth I'm looking at from last step is an exit, target it, and escape the inner loop with break, and outer loop because we are now targeting an exit
                    return branch;
                }
                booths.get(counter+1).addAll(branch.getPossibleFutureLocations());
            }
        }
        return null;
    }

    public interface OccupyableLocation<T extends OccupyableLocation<T>> {
        EasyList<T> getPossibleFutureLocations();
        EasyList<T> getPossiblePreviousLocations();
        @SuppressWarnings("BooleanMethodIsAlwaysInverted")
        boolean isTransversable();
    }
}
