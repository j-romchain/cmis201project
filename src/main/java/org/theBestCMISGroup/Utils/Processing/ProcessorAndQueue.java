package org.theBestCMISGroup.Utils.Processing;

import org.theBestCMISGroup.Utils.Storage.Queue;

import java.util.function.Consumer;

public class ProcessorAndQueue<T> {
    private final Processor<T> processor;
    private final Queue<T> queue;
    public ProcessorAndQueue(Processor<T> processor) {
        this((noout)->{},processor);
    }
    public ProcessorAndQueue(Consumer<T> output, Processor<T> processor) {
        queue=new Queue<>();
        this.processor=processor;
        this.processor.replaceInput(queue::dequeue);
    }
    public void queueAndProcess(T item) {
        queue.enqueue(item);
    }

    public Processor<T> getProcessor() {
        return processor;
    }

    public Queue<T> getQueue() {
        return queue;
    }
}
