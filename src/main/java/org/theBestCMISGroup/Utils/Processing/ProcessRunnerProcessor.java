package org.theBestCMISGroup.Utils.Processing;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class ProcessRunnerProcessor<T extends ProcessRunnerProcessor.VariableProcessorProcess<D>,D> extends Processor<T> {
    private D processorData;

    public ProcessRunnerProcessor(Supplier<T> input, String processorID, D processorData) {
        super(input, processorID, new ProcessRunner<>(processorID, processorData));
    }

    public ProcessRunnerProcessor(Supplier<T> input, Consumer<T> output, String processorID,D processorData) {
        super(input, output, processorID, new ProcessRunner<>(processorID,processorData));
    }
    private static class ProcessRunner<T extends VariableProcessorProcess<D>,D> implements VariableItemProcess<T> {
        public ProcessRunner(String processorID, D processorData) {
            this.processorID=processorID;
            this.processorData=processorData;
        }
        final String processorID;
        final D processorData;
        @Override
        public Long getEstimatedProcessTimeLeftMS(T item) {
            return item.getEstimatedProcessingTimeLeftMS();
        }

        @Override
        public boolean getProcessComplete(T item) {
            return item.getProcessingComplete();
        }

        @Override
        public void pauseProcess(T item) {
            item.pauseProcessing();
        }

        @Override
        public void completeProcess(T item) {
            item.completeProcessing();
        }

        @Override
        public void startProcess(T item) {
            item.startProcessing(processorID,processorData);
        }
    }
    public interface VariableProcessorProcess<D> {
        Long getEstimatedProcessingTimeLeftMS();
        boolean getProcessingComplete();
        void pauseProcessing();
        void completeProcessing();
        void startProcessing(String processorID, D processorData);
    }

}
