package org.theBestCMISGroup.Utils.Processing;

import org.theBestCMISGroup.Utils.Simulation.SimulatableGlobalTime;

import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Processor<T> {
    private Supplier<T> input;
    private Consumer<T> output;
    private final String processorID;
    private final VariableItemProcess<T> processorsProcess;
    private T itemBeingProcessed;
    private Date processCompletionETA;

    public String getProcessorID() {
        return processorID;
    }

    public VariableItemProcess<T> getProcessorsProcess() {
        return processorsProcess;
    }

    public Processor(Supplier<T> input, String processorID, VariableItemProcess<T> processorsProcess){
        this(input,(ignore)->{}, processorID, processorsProcess);
    }
    public Processor(Supplier<T> input, Consumer<T> output, String processorID, VariableItemProcess<T> processorsProcess){
        this.input =input;
        this.output=output;
        this.processorID = processorID;
        this.processorsProcess = processorsProcess;
        checkIfProcessingComplete();
    }
    public T getItemBeingProcessed() {
        return itemBeingProcessed;
    }

    public Date getProcessCompletionETA() {
        return processCompletionETA;
    }

    public Supplier<T> getInput() {
        return input;
    }
    public void replaceInput(Supplier<T> newInput) {
        input=newInput;
        tick();
    }

    public void replaceOutput(Consumer<T> newOutput) {
        output=newOutput;
    }

    private void checkIfProcessingComplete() {
        if (processCompletionETA ==null||!SimulatableGlobalTime.getTime().before(processCompletionETA)) {
            if (itemBeingProcessed==null||processorsProcess.getProcessComplete(itemBeingProcessed)) {
                completeCurrentItemsProcess(null);
            }
        }
    }
    private T completeCurrentItemsProcess(T interruptwith) {
        //reset the ETA to null
        processCompletionETA =null;
        //finish the previous item's processing and output it if it isn't null;
        if (itemBeingProcessed!=null) {
            if (interruptwith==null || processorsProcess.getProcessComplete(itemBeingProcessed)) {
                processorsProcess.completeProcess(itemBeingProcessed);
                output.accept(itemBeingProcessed);
            } else {
                processorsProcess.pauseProcess(itemBeingProcessed);
                return itemBeingProcessed;
            }
        }
        //load in the new item, start processing it, and set a new ETA, catching if there is no new item and leaving the ETA null
        itemBeingProcessed = interruptwith==null? input.get():interruptwith;
        try {
            processorsProcess.startProcess(itemBeingProcessed);
            processCompletionETA =new Date(SimulatableGlobalTime.getTime().getTime()+ processorsProcess.getEstimatedProcessTimeLeftMS(itemBeingProcessed));
        } catch (NullPointerException ignored) {}
        return null;
    }
    public T interrupt(T interruptWith) {
        return completeCurrentItemsProcess(interruptWith);
    }
    public void tick() {
        checkIfProcessingComplete();
    }
    public interface VariableItemProcess<T> {
        Long getEstimatedProcessTimeLeftMS(T item);
        boolean getProcessComplete(T item);
        void pauseProcess(T item);
        void completeProcess(T item);
        void startProcess(T item);
    }
}
