package org.theBestCMISGroup.Utils.Storage.Utils;

public class Errors {

    public abstract static class AdditionCriteriaException extends Exception {}
    public static class DuplicateItemException extends AdditionCriteriaException {}
}
