package org.theBestCMISGroup.Utils.Storage.Utils;

import java.util.UUID;

public class ItemsWithUUIDs {
    public interface InterfaceOfItemsWithUUID {
        UUID getUUID();
    }
    public static class ItemWithUUID implements InterfaceOfItemsWithUUID {
        private final UUID uuid;
        public ItemWithUUID() {
            this.uuid = UUID.randomUUID();
        }
        public UUID getUUID() {
            return uuid;
        }
    }

    public static boolean doUnitTests() {
        boolean goodSoFar = true;
        System.out.println("testing ItemsWithUUIDs");
        System.out.println("test 1");
        //noinspection ConstantValue
        goodSoFar &= testGetUUID();
        System.out.println("ItemsWithUUIDs testing "+(goodSoFar ? "success":"fail")+"!!!");
        return goodSoFar;
    }
    public static boolean testGetUUID() {
        ItemWithUUID testITem = new ItemWithUUID();
        boolean testSuccess = testITem.getUUID().equals(testITem.uuid);
        System.out.println(testSuccess ? "success":"fail");
        return testSuccess;
    }
}

