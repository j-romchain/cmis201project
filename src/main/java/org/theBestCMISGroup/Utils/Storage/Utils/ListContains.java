package org.theBestCMISGroup.Utils.Storage.Utils;

import org.theBestCMISGroup.Utils.Storage.EasyList;

public class ListContains {
    public static <T> boolean containsItem(EasyList<T> items, T item) {
        for (T listItem : items) {
            if (listItem == item && listItem != null) {
                return true;
            }
        }
        return false;
    }

    public static <T> T findOverlap1d(EasyList<T> list1, EasyList<T> list2) {
        for (T listItem : list1) {
            if (containsItem(list2, listItem)) {
                return listItem;
            }
        }
        return null;
    }

    public static <T> boolean overlap1d(EasyList<T> list1, EasyList<T> list2) {
        return findOverlap1d(list1, list2) != null;
    }

    public static <T> T findOverlap2d(EasyList<EasyList<T>> listOlists1, EasyList<EasyList<T>> listOlists2) {
        for (EasyList<T> list1 : listOlists1) {
            for (EasyList<T> list2 : listOlists2) {
                T overlappingItem = findOverlap1d(list1, list2);
                if (overlappingItem != null) {
                    return overlappingItem;
                }
            }
        }
        return null;
    }

    public static <T> boolean overlap2d(EasyList<EasyList<T>> listOlists1, EasyList<EasyList<T>> listOlists2) {
        return findOverlap2d(listOlists1, listOlists2) != null;
    }
}