package org.theBestCMISGroup.Utils.Storage.Utils;

public abstract class TransversalCode<T> {
    public abstract T doForItem(T item, int itemNum);
}
