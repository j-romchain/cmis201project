package org.theBestCMISGroup.Utils.Storage.Utils;

import java.util.Arrays;

public class SortAndSearchUtils {
    public static void main(String[] args) {
        //put your code here
        doUnitTests();
    }
    public static boolean doUnitTests() {
        boolean goodSoFar = true;
        System.out.println("testing SortAndSearchUtils");
        System.out.println("test 1");
        goodSoFar &= testAddItemWithoutBreakingSort();
        System.out.println("test 2");
        goodSoFar &= testBinarySearch();
        System.out.println("test 3");
        goodSoFar &= testLinearSearch();
        System.out.println("test 4");
        goodSoFar &= testIsSorted();
        System.out.println("test 5");
        goodSoFar &= testSelectSort();
        System.out.println("test 6");
        goodSoFar &= testMergeSort();
        System.out.println("test 7");
        goodSoFar &= testMergeSortedLists();
        System.out.println("SortAndSearchUtils testing " + (goodSoFar ? "success" : "fail") + "!!!");
        return goodSoFar;
    }
    public static boolean testAddItemWithoutBreakingSort() {
        String[] testList = new String[1000];
        for (int i = 0; i < 999; i++) {
            addItemWithoutBreakingSort("item #" + (int) (Math.random() * 1000) + ";" + i,testList, new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        }
        addItemWithoutBreakingSort("item #4321",testList,new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        boolean result = testList[binarySearch("item #4321",testList,(Criteria.ComparableCriteria<String>) new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<String>())].equals("item #4321");
        result &= isSorted(testList,new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        System.out.println(result ? "success" : "fail");
        return result;
    }
    public static boolean testBinarySearch() {
        String[] list = new String[1000];
        for (int i = 0; i < 300; i++) {
            addItemWithoutBreakingSort("item #" + (int) (Math.random() * 1000) + ";" + i, list, new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        }
        addItemWithoutBreakingSort("item #4321", list, new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        boolean result = list[binarySearch("item #4321",list,(Criteria.ComparableCriteria<String>) new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<String>())].equals("item #4321");
        System.out.println(result ? "success":"fail");
        return result;
    }
    public static boolean testLinearSearch() {
        String[] list = new String[1000];
        for (int i = 0; i < 300; i++) {
            addItemWithoutBreakingSort("item #" + (int) (Math.random() * 1000) + ";" + i, list, new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        }
        addItemWithoutBreakingSort("item #4321", list, new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        boolean result = list[linearSearch(list, item -> item.equals("item #4321"))].equals("item #4321");
        System.out.println(result ? "success":"fail");
        return result;
    }
    public static boolean testIsSorted() {
        String[] testList = new String[1000];
        //populate
        for (int i = 0; i < 999; i++) {
            addItemWithoutBreakingSort("item #"+(int)(Math.random()*1000)+";"+i,testList,new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        }
        //manually scramble
        for (int i = 0; i < testList.length;i++){
            String temp = testList[i];
            int newPos = (int)(Math.random()*testList.length);
            testList[i]=testList[newPos];
            testList[newPos]=temp;
        }
        boolean result = !isSorted(testList,new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        //sort it
        selectionSort(testList,new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        result &= isSorted(testList,new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        System.out.println(result ? "success":"fail");
        return result;
    }

    public static boolean testSelectSort() {
        String[] testList = new String[1000];
        //populate
        for (int i = 0; i < 999; i++) {
            addItemWithoutBreakingSort("item #"+(int)(Math.random()*1000)+";"+i,testList,new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        }
        //manually scramble
        for (int i = 0; i < testList.length;i++){
            String temp = testList[i];
            int newPos = (int)(Math.random()*testList.length);
            testList[i]=testList[newPos];
            testList[newPos]=temp;
        }
        //run the sort
        selectionSort(testList,new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        boolean result = isSorted(testList,new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        System.out.println(result ? "success":"fail");
        return result;
    }
    public static boolean testMergeSort() {
        String[] testList = new String[1000];
        //populate
        for (int i = 0; i < 999; i++) {
            addItemWithoutBreakingSort("item #"+(int)(Math.random()*1000)+";"+i,testList,new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        }
        //manually scramble
        for (int i = 0; i < testList.length;i++){
            String temp = testList[i];
            int newPos = (int)(Math.random()*testList.length);
            testList[i]=testList[newPos];
            testList[newPos]=temp;
        }
        //run the sort
        mergeSort(testList,new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        boolean result = isSorted(testList,new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        System.out.println(result ? "success":"fail");
        return result;
    }
    private static boolean testMergeSortedLists() {
        String[] testList1 = new String[333];
        String[] testList2 = new String[999-333];
        //populate
        for (int i = 0; i < 333; i++) {
            addItemWithoutBreakingSort("item #"+(int)(Math.random()*1000)+";"+i, testList1,new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        }
        for (int i = 333; i < 999; i++) {
            addItemWithoutBreakingSort("item #"+(int)(Math.random()*1000)+";"+i, testList2,new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        }
        //run the sort
        String[] merged = mergeSortedLists(testList1,testList2, new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        boolean result = isSorted(merged,new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        for (String s : testList1) {
            result &= Arrays.toString(merged).contains(s);
        }
        for (String s : testList2) {
            result &= Arrays.toString(merged).contains(s);
        }
        System.out.println(result ? "success":"fail");
        return result;
    }
    public static <T> void addItemWithoutBreakingSort(T item, T[] list, Criteria.ComparableCriteria<T> sorting) {
        boolean placed = false;
        T prevItem = null;
        for (int i = 0; i < list.length; i++) {
            if (!placed) {
                if (sorting.compareUsingCriteria(list[i], item) <= 0) {
                    prevItem = list[i];
                    placed = true;
                    list[i] = item;
                }
            } else {
                T tmp = list[i];
                list[i] = prevItem;
                prevItem = tmp;
            }

        }
    }
    public static <T> int binarySearch(String stringDerivedFromTargetItem, T[] list, Criteria.DerivedStringComparableCriteria<T> sort) {
//        System.out.println(Arrays.toString(list));
        if (list.length==0) { return -1;}
        int leftBound = 0;
        int rightBound = list.length-1;
        int pos;
        while (Math.abs(leftBound-rightBound)>1) {
            pos = (int) (leftBound+Math.floor((rightBound-leftBound)/2d));
            if (sort.compareToPreDerivedStringUsingCriteria(list[pos],stringDerivedFromTargetItem)==0) {
                return pos;
            } else if (sort.compareToPreDerivedStringUsingCriteria(list[pos],stringDerivedFromTargetItem)<0) {
                rightBound=pos;
            } else if (sort.compareToPreDerivedStringUsingCriteria(list[pos],stringDerivedFromTargetItem)>0) {
                leftBound=pos;
            }
        }
        if (sort.compareToPreDerivedStringUsingCriteria(list[rightBound],stringDerivedFromTargetItem)==0) {
            return rightBound;
        } else if (sort.compareToPreDerivedStringUsingCriteria(list[leftBound],stringDerivedFromTargetItem)==0) {
            return leftBound;
        }
        return -1;
    }

    public static <T> int binarySearch(T itemFittingCriteria, T[] list, Criteria.ComparableCriteria<T> sorting) {
//        System.out.println(Arrays.toString(list));
        if (list.length==0) { return -1;}
        int leftBound = 0;
        int rightBound = list.length-1;
        int pos;
        while (Math.abs(leftBound-rightBound)>1) {
            pos = (int) (leftBound+Math.floor((rightBound-leftBound)/2d));
            if (sorting.compareUsingCriteria(itemFittingCriteria,list[pos])==0) {
                return pos;
            } else if (sorting.compareUsingCriteria(itemFittingCriteria,list[pos])>0) {
                rightBound=pos;
            } else if (sorting.compareUsingCriteria(itemFittingCriteria,list[pos])<0) {
                leftBound=pos;
            }
        }
        if (sorting.compareUsingCriteria(itemFittingCriteria,list[rightBound])==0) {
            return rightBound;
        } else if (sorting.compareUsingCriteria(itemFittingCriteria,list[leftBound])==0) {
            return leftBound;
        }
        return -1;
    }
    // PH1 Comment: JS: Excellent, Nice use of a filter object.  Now you write this method just once!
    public static <T> int linearSearch(T[] list, Criteria.MatchableCriteria<T> criteria) {
        for (int i = 0; i < list.length; i++) {
            T item = list[i];
            if (criteria.matchesCriteria(item)) {
                return i;
            }
        }
        return -1;
    }
    public static <T> boolean isSorted(T[] list, Criteria.ComparableCriteria<T> sortCriteria) {
        return sortBreakages(list,sortCriteria)==0;
    }
    public static <T> void selectionSort(T[] list, Criteria.ComparableCriteria<T> sortCriteria) {
        if (isSorted(list,sortCriteria)) {return;}
        //going from the start and going along
        for (int i = 0; i < list.length;i++){
            T item = list[i];
            //scan all the items after this item
            for (int j = i; j < list.length;j++){
                T item2 = list[j];
                //if the item is smaller than "this item", swap them (& update what "this item" is
                if (sortCriteria.compareUsingCriteria(item,item2)<0) {
                    //System.out.println(item+" less than "+item2);
                    list[j] = item;
                    list[i] = item2;
                    item=item2;
                }
            }
            //System.out.println(i + "      "+Arrays.toString(list));
        }
    }

    public static <T> void mergeSort(T[] list, Criteria.ComparableCriteria<T> sortCriteria) {
        if (isSorted(list,sortCriteria)) {return;}
        //start by going down the list and extracting sorted sections to their own list
        //noinspection unchecked
        T[][] splits = (T[][]) new Object[list.length][list.length];
        int splitNum = 0;
        int indexInSplit = 0;
        for (int i = 0; i < list.length;i++){
            T item = list[i];
            if (sortCriteria.compareUsingCriteria(item,(i>0 ? list[i-1]:null))>=0) {
                splitNum++;
                indexInSplit=0;
            }
            splits[splitNum][indexInSplit] = item;
            indexInSplit++;
        }
        //and then merge them.
        //*first empty the list
        Arrays.fill(list, null);
        for (T[] split : splits) {
            if (split[0] == null) {
                //end when all splits are merged, and the rest is just blank
                return;
            } else {
                list = mergeSortedLists(list, split, sortCriteria);
            }
        }
        //or end when you've merged all the splits.
    }

    private static <T> T[] mergeSortedLists(T[] list1, T[] list2, Criteria.ComparableCriteria<T> sortCriteria) {
        T[] mergedList = Arrays.copyOf(list1,list1.length+list2.length);
        Arrays.fill(mergedList, null);
        // start by selecting the list that starts lower
        int list1Index = 0;
        int list2Index = 0;
        int mergeIndex = 0;
        while (list1Index < list1.length && list2Index < list2.length) {
            if (sortCriteria.compareUsingCriteria(list1[list1Index], list2[list2Index]) >= 0) {
                mergedList[mergeIndex] = list1[list1Index];
                list1Index++;
            } else {
                mergedList[mergeIndex] = list2[list2Index];
                list2Index++;
            }
            mergeIndex++;
        }
        // Copy any remaining elements from list1 or list2 to the merged list
        while (list1Index < list1.length) {
            mergedList[mergeIndex] = list1[list1Index];
            list1Index++;
            mergeIndex++;
        }
        while (list2Index < list2.length) {
            mergedList[mergeIndex] = list2[list2Index];
            list2Index++;
            mergeIndex++;
        }

        // and now trim off the nulls
        System.arraycopy(mergedList, 0, mergedList, 0, (Math.max(list1.length, list2.length)));
        return mergedList;
    }
    public static <T> int sortBreakages(T[] list, Criteria.ComparableCriteria<T> sortCriteria) {
        int breakages = 0;
        T previousItem = null;
        for (T item : list) {
            if (previousItem!=null & sortCriteria.compareUsingCriteria(item, previousItem) > 0) {
                breakages++;
            }
            previousItem = item;
        }
        return breakages;
    }

    public static <T> void sort(T[] list,Criteria.ComparableCriteria<T> sortCriteria) {
        if (sortBreakages(list,sortCriteria)<(list.length/3)) {
            mergeSort(list,sortCriteria);
        } else {
            selectionSort(list,sortCriteria);
        }
    }
}
