package org.theBestCMISGroup.Utils.Storage.Utils;

public class Criteria<T> {
    public interface MatchableCriteria<T> {
        boolean matchesCriteria(T item);
    }
    public interface ComparableCriteria<T> {
        int compareUsingCriteria(T item1, T item2);
    }

    public abstract static class DerivedStringCriteria<T> {
        public abstract String deriveString(T object);
        public String deriveString(String stringDerivedFromAnItem) {
            return stringDerivedFromAnItem;
        }
    }
    public abstract static class DerivedStringComparableCriteria<T> extends DerivedStringCriteria<T> implements ComparableCriteria<T> {
        public int compareUsingCriteria(T item1, T item2) {
            //if item1 > item2, 1
            //if item1 == item2, 0
            //if item1 < item2, -1
            if (item1==item2) {return 0;}
            if (item1==null) {return -1;}
            if (item2==null) {return 1;}
            String string2 = deriveString(item2);
            return compareToPreDerivedStringUsingCriteria(item1,string2);
        }

        public int compareToPreDerivedStringUsingCriteria(T item1, String string2) {
            //if item1 > item2, 1
            //if item1 == item2, 0
            //if item1 < item2, -1
            if (item1==null) {return -1;}
            if (string2==null) {return 1;}
            String string1 = deriveString(item1);
            return comparePreDerivedStringsUsingCriteria(string1,string2);
        }
        public int comparePreDerivedStringsUsingCriteria(String item1, String item2) {
            //if item1 > item2, 1
            //if item1 == item2, 0
            //if item1 < item2, -1
            for (int i = 0; i < Math.max(item1.length(), item2.length()); i++){
                char char1 = i< item1.length() ? item1.toCharArray()[i]:(char)-100;
                char char2 = i< item2.length() ? item2.toCharArray()[i]:(char)-100;
                if (char1 > char2) {
                    //System.out.println("1 bigger");
                    return 1;
                } else if (char1 < char2) {
                    //System.out.println("2 bigger");
                    return -1;
                }
            }
            //System.out.println("equal");
            return 0;
        }
        public static class ToStringComparableCriteria<T> extends DerivedStringComparableCriteria<T> {
            @Override
            public String deriveString(T object) {
                return object==null ? "":object.toString();
            }
        }

    }
}
