package org.theBestCMISGroup.Utils.Storage;


import org.theBestCMISGroup.Utils.Storage.Utils.Criteria;

@SuppressWarnings("unused")
public interface SortableStorageInterface<T> extends BasicStorageInterface<T>{
    Criteria.ComparableCriteria<T> getSort();
    void setSort(Criteria.ComparableCriteria<T> newSort);
    T getItemUsingSort(T itemThatMatchesCriteria);
    T getItemUsingDerivedStringSorting(String stringDerivedFromAnItemThatMatchesCriteria);
    void sort();
    boolean isSorted();
}
