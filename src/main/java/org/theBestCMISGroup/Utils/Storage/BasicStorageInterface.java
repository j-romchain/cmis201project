package org.theBestCMISGroup.Utils.Storage;

import org.theBestCMISGroup.Utils.Storage.Utils.Criteria;
import org.theBestCMISGroup.Utils.Storage.Utils.Errors;
import org.theBestCMISGroup.Utils.Storage.Utils.ItemsWithUUIDs;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("unused")
public interface BasicStorageInterface<T> extends List<T>, Collection<T> {
    @SuppressWarnings("SameReturnValue")
    static boolean doUnitTests() {
        return false;
    }
    void addItems(T[] items) throws Errors.AdditionCriteriaException;
    void addItem(T item) throws Errors.AdditionCriteriaException;

    int getItemCount();

    T getItem(Criteria.MatchableCriteria<T> criteria);
    default T getItemByUUID(UUID uuid) {
        Criteria.MatchableCriteria<T> criteria = item -> {
            if (item == null) {
                return false;
            }
            return ((ItemsWithUUIDs.InterfaceOfItemsWithUUID) item).getUUID().equals(uuid);
        };
        try {
            return getItem(criteria);
        } catch (ClassCastException e) {
            System.out.println("Error, Item being searched does not implement ItemsWithUUIDs.InterfaceOfItemsWithUUID, or extend ItemsWithUUIDs.ItemWithUUID in order to use this function.");
            return null;
        }
    }

    EasyList<T> toEasyList();
    T[] toTypedArray();
    int indexOf(Criteria.MatchableCriteria<T> crit);
    int lastIndexOf(Criteria.MatchableCriteria<T> crit);
    @Override
    String toString();
}