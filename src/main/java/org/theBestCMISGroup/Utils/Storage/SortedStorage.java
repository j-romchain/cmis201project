package org.theBestCMISGroup.Utils.Storage;


import org.theBestCMISGroup.Utils.Storage.Utils.Criteria;
import org.theBestCMISGroup.Utils.Storage.Utils.Errors;
import org.theBestCMISGroup.Utils.Storage.Utils.SortAndSearchUtils;

import java.io.OutputStream;
import java.io.PrintStream;

@SuppressWarnings("unused")
public class SortedStorage<T> extends GenericStorage<T> implements SortableStorageInterface<T>{
    private Criteria.ComparableCriteria<T> sort = new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>();
    public static void main(String[] args) {
        //put your code here
        doUnitTests();
    }
    @SuppressWarnings("UnusedReturnValue")
    public static boolean doUnitTests() {
        boolean goodSoFar = true;
        System.out.println("testing SortedStorage");
        System.out.println("test 1");
        goodSoFar &= testCanAddItemWithDuplicateSortCriteria();
        System.out.println("test 2");
        goodSoFar &= testChangeSortCriteria();
        System.out.println("testing GenericStorage and SortAndSearchUtils, as they are crucial components");
        goodSoFar &= GenericStorage.doUnitTests();
        goodSoFar &= SortAndSearchUtils.doUnitTests();
        System.out.println("SortedStorage testing " + (goodSoFar ? "success" : "fail") + "!!!");
        return goodSoFar;
    }
    public static boolean testCanAddItemWithDuplicateSortCriteria() {
        SortedStorage<String> testSortedStorage = new SortedStorage<>(new Criteria.DerivedStringComparableCriteria<>() {
            @Override
            public String deriveString(String object) {
                return object.toCharArray()[0] + "";
            }
        });
        try {
            testSortedStorage.addItem("Mickey the Mouse");
        } catch (Errors.DuplicateItemException e) {
            System.out.println("fail");
            return false;
        }
        var sOut = System.out;
        System.setOut(new PrintStream(OutputStream.nullOutputStream()));
        boolean result = false;
        try {
            testSortedStorage.addItem("Mickey the House");
        } catch (Errors.DuplicateItemException e) {
            System.setOut(sOut);
            result = true;
        }
        System.setOut(sOut);
        result &= testSortedStorage.getItemCount()==1;
        System.out.println(result ? "success":"fail");
        return result;
    }
    public static boolean testChangeSortCriteria() {
        SortedStorage<String> testStorage = new SortedStorage<>();
        for (int i = 0; i < 999; i++) {
            try {
                testStorage.addItem("item #"+(int)(Math.random()*1000)+";"+i);
            } catch (Errors.DuplicateItemException e) {
                System.out.println("fail");
                return false;
            }
        }
        testStorage.setSort(new Criteria.DerivedStringComparableCriteria<>() {
            @Override
            public String deriveString(String item) {
                return item.substring(item.indexOf(";"));
            }
        });
        boolean result = testStorage.isSorted();
        System.out.println(result ? "success":"fail");
        return result;
    }
    public SortedStorage() {}
    public SortedStorage(Criteria.ComparableCriteria<T> combiner) {
        this.sort=combiner;
    }
    @Override
    public void addItem(T item) throws Errors.DuplicateItemException {
        if (getItemUsingSort(item)==null) {
            SortAndSearchUtils.addItemWithoutBreakingSort(item,storageArray,sort);
            itemCount++;
        } else {
            System.out.println("Error, another item that matches this item's sort value is already in Storage, refusing to add item with duplicate sort value.");
            throw new Errors.DuplicateItemException();
        }
    }
    public T getItemUsingSort(T itemFittingCriteria) {
        int pos = SortAndSearchUtils.binarySearch(itemFittingCriteria,storageArray,sort);
        return pos<0 ? null:storageArray[pos];
    }
    public T getItemUsingDerivedStringSorting(String stringDerivedFromTargetItem) {
        try {
            int pos = SortAndSearchUtils.binarySearch(stringDerivedFromTargetItem, storageArray, (Criteria.DerivedStringComparableCriteria<T>) sort);
            return pos < 0 ? null : storageArray[pos];
        } catch (ClassCastException e) {
            System.out.println("Error, sorting criteria is not a DerivedStringComparableCriteria<T>.");
            return null;
        }
    }
    public void setSort(Criteria.ComparableCriteria<T> newSortCriteria) {
        this.sort = newSortCriteria;
        sort();
    }
    public Criteria.ComparableCriteria<T> getSort() {
        return sort;
    }

    public boolean isSorted() {
        return SortAndSearchUtils.isSorted(storageArray,sort);
    }
    public void sort() {
        SortAndSearchUtils.sort(storageArray,sort);
    }
    @Override
    public String toString() {
        return "SortedStorage{" +
                "sort=" + sort +
                ", " + super.toString() + "}";
    }

    @Override
    public T set(int index, T element) {
        throw new RuntimeException("something tried setting an item by index in a sorted list. this method has to be here, but please don't use it.");
    }
    //TODO do the rest of the by indexes this way.
}
