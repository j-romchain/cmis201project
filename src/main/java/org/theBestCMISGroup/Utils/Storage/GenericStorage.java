package org.theBestCMISGroup.Utils.Storage;

import org.theBestCMISGroup.Utils.Storage.Utils.*;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.*;

public class GenericStorage<T> implements BasicStorageInterface<T> {
    @SuppressWarnings({"unchecked"})
    T[] storageArray = (T[]) new Object[1000];
    int itemCount = 0;

    public static void main(String[] args) {
        //put your code here
        doUnitTests();
    }

// PH1 Comment: JS: Outstanding set of tests. I am very impressed.
    public static boolean doUnitTests() {
        boolean goodSoFar = true;
        System.out.println("testing GenericStorage");
        System.out.println("test 1");
        goodSoFar &= testGetItemCount();
        System.out.println("test 2");
        goodSoFar &= testAddOneThousandAndFirstItem();
        System.out.println("test 3");
        goodSoFar &= testGetItemByStrangeCriteria();
        System.out.println("test 4");
        goodSoFar &= testGetItemByUUID();
        System.out.println("test 5");
        goodSoFar &= testGetItemByUUIDError();
        System.out.println("test 6");
        goodSoFar &= testCanAddExactDuplicateItem();
//        System.out.println("test 7");
        System.out.println("GenericStorage testing " + (goodSoFar ? "success" : "fail") + "!!!");
        return goodSoFar;
    }

    public static boolean testCanAddExactDuplicateItem() {
        GenericStorage<Object> testGenericStorage = new GenericStorage<>();
        var item = new Object();
        try {
            testGenericStorage.addItem(item);
        } catch (Errors.AdditionCriteriaException e) {
            System.out.println("fail");
            return false;
        }
        try {
            testGenericStorage.addItem(new Object());
        } catch (Errors.AdditionCriteriaException e) {
            System.out.println("fail");
            return false;
        }
        var sOut = System.out;
        System.setOut(new PrintStream(OutputStream.nullOutputStream()));
        boolean result = false;
        try {
            testGenericStorage.addItem(item);
        } catch (Errors.DuplicateItemException e) {
            System.setOut(sOut);
            result = true;
        } catch (Errors.AdditionCriteriaException e) {
            System.setOut(sOut);
            System.out.println("fail");
            return false;
        }
        result &= testGenericStorage.getItemCount()==2;
        System.out.println(result ? "success":"fail");
        return result;
    }

    public static boolean testGetItemCount() {
        GenericStorage<String> testStorage = new GenericStorage<>();
        boolean testSuccess = true;
        for (int i = 1; i <= 1500; i++) {
            try {
                testStorage.addItem("Lorem Ipsum dolor seit..."+i);
            } catch (Errors.AdditionCriteriaException ignored) {
                System.out.println("fail");
                return false;
            }
            testSuccess &= testStorage.getItemCount() == i;
        }
        System.out.println(testSuccess ? "success":"fail");
        return testSuccess;
    }
    public static boolean testAddOneThousandAndFirstItem() {
        GenericStorage<String> testStorage = new GenericStorage<>();
        for (int i = 1; i <= 1000; i++) {
            try {
                testStorage.addItem("Lorem Ipsum dolor seit..."+i);
            } catch (Errors.AdditionCriteriaException ignored) {
                System.out.println("fail");
                return false;
            }
        }
        try {
            testStorage.addItem("one THOUSAND!!!");
        } catch (Errors.AdditionCriteriaException ignored) {
            System.out.println("fail");
            return false;
        }
        boolean result = testStorage.getItem("one THOUSAND!!!"::equals)!=null;
        result &= testStorage.getItem("Lorem Ipsum dolor seit...1"::equals)!=null;
        result &= testStorage.getItem("Lorem Ipsum dolor seit...1000"::equals)!=null;
        System.out.println(result ? "success":"fail");
        return result;
    }
    public static boolean testGetItemByStrangeCriteria() {
        GenericStorage<String> testStorage = new GenericStorage<>();
        try {
            testStorage.addItem("Lorem Ipsum dolor seit...");
            testStorage.addItem("find MEE!!!");
            testStorage.addItem("Lorem Ipsum dolor seit am...");
        } catch (Errors.AdditionCriteriaException ignored) {
            System.out.println("fail");
            return false;
        }
        @SuppressWarnings("StringEquality")
        boolean result = testStorage.getItem("find MEE!!!"::equals)=="find MEE!!!";
        System.out.println(result ? "success":"fail");
        return result;
    }
    public static boolean testGetItemByUUID() {
        class TestItemWithUUID extends ItemsWithUUIDs.ItemWithUUID {}
        GenericStorage<TestItemWithUUID> testStorage = new GenericStorage<>();
        TestItemWithUUID test = new TestItemWithUUID();
        try {
            testStorage.addItem(new TestItemWithUUID());
            testStorage.addItem(test);
            testStorage.addItem(new TestItemWithUUID());
        } catch (Errors.AdditionCriteriaException e) {
            System.out.println("fail");
            return false;
        }
        boolean result = testStorage.getItemByUUID(test.getUUID())==test;
        System.out.println(result ? "success":"fail");
        return result;
    }
    public static boolean testGetItemByUUIDError() {
        GenericStorage<String> testStorage = new GenericStorage<>();
        String test = "blah";
        try {
            testStorage.addItem("1");
            testStorage.addItem(test);
            testStorage.addItem("2");
        } catch (Errors.AdditionCriteriaException e) {
            System.out.println("fail");
            return false;
        }
        //this was a neat trick I got from ze inter-webs!
        PrintStream SOut = System.out;
        ByteArrayOutputStream tempSOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(tempSOut));
        boolean result = testStorage.getItemByUUID(UUID.randomUUID())==null;
        System.setOut(SOut);
        result &= tempSOut.toString().contains("Error, Item being searched does not implement ItemsWithUUIDs.InterfaceOfItemsWithUUID, or extend ItemsWithUUIDs.ItemWithUUID in order to use this function.");
        System.out.println(result ? "success":"fail");
        return result;
    }
    public GenericStorage() {}
// PH1 Comment: JS: In a database context like this, you do not want to depend on anyone watching the console output.
// If you had an error log file, you could send messages there.
// You want instead to return some kind of failure code.
// While you are at that, note that you will want to distinguish the case of not having any room from the case
// of already having a duplicate, in the event you worry about the latter.- DONE

    @Override
    public void addItems(T[] items) throws Errors.AdditionCriteriaException {
        for (T item:items) {
            addItem(item);
        }
    }
    public void addItem(T item) throws Errors.AdditionCriteriaException {
        if (getItem(object -> object == item) != null) {
            System.out.println("Error, another item that matches this item exactly is already in Storage, refusing to add duplicate item.");
            throw new Errors.DuplicateItemException();
        }
        if (itemCount>=storageArray.length) {
            T[] tempArray = storageArray;
            //noinspection unchecked
            storageArray = (T[]) new Object[storageArray.length+1000];
            System.arraycopy(tempArray,0,storageArray,0,tempArray.length);
        }
        storageArray[itemCount] = item;
        itemCount += 1;
    }
    public int getItemCount() {
        return itemCount;
    }

// PH1 Comment: JS: Excellent, Nice use of a filter object.  Now you write this method just once!
    public T getItem(Criteria.MatchableCriteria<T> criteria) {
        int pos = SortAndSearchUtils.linearSearch(storageArray,criteria);
        return pos<0 ? null:storageArray[pos];
    }

    public T getItemByUUID(UUID uuid) {
        Criteria.MatchableCriteria<T> criteria =  item -> {
            if (item == null) {
                return false;
            }
            return ((ItemsWithUUIDs.InterfaceOfItemsWithUUID)item).getUUID().equals(uuid);
        };
        try {
            return getItem(criteria);
        } catch (ClassCastException e) {
            System.out.println("Error, Item being searched does not implement ItemsWithUUIDs.InterfaceOfItemsWithUUID, or extend ItemsWithUUIDs.ItemWithUUID in order to use this function.");
            return null;
        }
    }

    public void inOrderTransverse(TransversalCode<T> codeToRun) {
        for (int i = 0; i < storageArray.length; i++) {
            T item = storageArray[i];
            codeToRun.doForItem(item, i);
        }
    }

    @Override
    public String toString() {
        return "GenericStorage{" +
                "itemCount=" + itemCount +
                ", storageArray=" + Arrays.toString(storageArray) +
                '}';
    }

    @Override
    public int size() {
        return getItemCount();
    }

    @Override
    public boolean isEmpty() {
        return size()<1;
    }

    @Override
    public boolean contains(Object o) {
        return getItem((i) -> i==o)!=null;
    }

    @Override
    public Iterator<T> iterator() {
        return toEasyList().iterator();
    }
    public EasyList<T> toEasyList() {
        return new EasyList<>(storageArray);
    }

    @Override
    public T[] toTypedArray() {
        return storageArray.clone();
    }

    @Override
    public Object[] toArray() {
        return toTypedArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return toEasyList().toArray(a);
    }

    @Override
    public boolean add(T t) {
        try {
            addItem(t);
            return true;
        } catch (Errors.AdditionCriteriaException e) {
            return false;
        }
    }
    public boolean remove(Criteria.MatchableCriteria<T> crit) {
        boolean removed = false;
        for (int i = 0, storageArrayLength = storageArray.length; i < storageArrayLength; i++) {
            T t = storageArray[i];
            if (removed) {
                storageArray[i - 1] = t;
                storageArray[i] = null;
            }
            if (crit.matchesCriteria(t)) {
                storageArray[i] = null;
                itemCount--;
                removed = true;
            }
        }
        return removed;
    }

    @Override
    public boolean remove(Object o) {
        return remove((i) -> i==o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean ret = true;
        for (Object o : c) {
            ret &= contains(o);
        }
        return ret;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean ret = false;
        for (T t : c) {
            ret |= add(t);
        }
        return ret;
    }
    public boolean addAll(T[] c) {
        boolean ret = false;
        for (T t : c) {
            ret |= add(t);
        }
        return ret;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        int ind = index;
        for (T t : c) {
            add(ind,t);
            ind++;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean ret = false;
        for (Object o : c) {
            ret |= remove(o);
        }
        return ret;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean ret = false;
        for (T t : this) {
            if (!c.contains(t)) {
                ret |= remove(t);
            }
        }
        return ret;
    }

    @Override
    public void clear() {
        //noinspection unchecked
        storageArray= (T[]) new Object[1000];
        itemCount=0;
    }

    @Override
    public T get(int index) {
        return storageArray[index];
    }

    @Override
    public T set(int index, T element) {
        T old = get(index);
        storageArray[index]=element;
        return old;
    }

    @Override
    public void add(int index, T element) {
        T shiftUpItem=element;
        for (int i = index; i < itemCount-1; i++) {
            T t = storageArray[i];
            storageArray[i]=shiftUpItem;
            shiftUpItem=t;
        }
        add(shiftUpItem);
    }

    @Override
    public T remove(int index) {
        T item = get(index);
        remove(item);
        return item;
    }

    @Override
    public int indexOf(Criteria.MatchableCriteria<T> crit) {
        for (int i = 0, storageArrayLength = storageArray.length; i < storageArrayLength; i++) {
            T t = storageArray[i];
            if (crit.matchesCriteria(t)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int indexOf(Object o) {
        return indexOf((i) -> i==o);
    }

    @Override
    public int lastIndexOf(Criteria.MatchableCriteria<T> crit) {
        for (int i = storageArray.length-1; i >= 0; i--) {
            T t = storageArray[i];
            if (crit.matchesCriteria(t)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        return lastIndexOf((i) -> i==o);
    }

    @Override
    public ListIterator<T> listIterator() {
        return listIterator(0);
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        GenericStorage<T> myself=this;
        return new ListIterator<>() {
            final GenericStorage<T> me = myself;
            int ind = index;
            @Override
            public boolean hasNext() {
                return myself.size()>ind+1;
            }

            @Override
            public T next() {
                ind++;
                return me.get(ind);
            }

            @Override
            public boolean hasPrevious() {
                return ind>0;
            }

            @Override
            public T previous() {
                ind--;
                return me.get(ind+1);
            }

            @Override
            public int nextIndex() {
                return ind+1;
            }

            @Override
            public int previousIndex() {
                return ind;
            }

            @Override
            public void remove() {
                me.remove(ind);
            }

            @Override
            public void set(T t) {
                me.set(ind,t);
            }

            @Override
            public void add(T t) {
                me.add(ind,t);
            }
        };
    }
    public GenericStorage<T> subStorage(int fromIndex, int toIndex) {
        GenericStorage<T> ret = new GenericStorage<>();
        for (int i = fromIndex; i < toIndex; i++) {
            ret.add(get(i));
        }
        return ret;
    }
    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return subStorage(fromIndex,toIndex);
    }
}
