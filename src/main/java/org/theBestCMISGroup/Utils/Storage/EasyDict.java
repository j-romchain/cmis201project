package org.theBestCMISGroup.Utils.Storage;

import java.util.Collection;

public class EasyDict<K,T> {
    private final EasyList<K> keys = new EasyList<>();
    private final EasyList<T> values = new EasyList<>();

    public EasyDict() {
    }

    @SuppressWarnings({"unchecked", "UnusedReturnValue"})
    public boolean addAll(Collection<? extends K> keys, Collection<? extends T> elements) {
        boolean changed = false;
        for (int i = 0; i < Math.min(keys.size(),elements.size()); i++) {
            changed |=this.keys.add((K) keys.toArray()[i]);
            //noinspection ConstantValue
            changed |=this.values.add((T) elements.toArray()[i]);
        }
        return changed;
    }

    public T get(K key) {
        return values.get(keys.indexOf(key));
    }

    public T set(K key, T element) {
        return values.set(keys.indexOf(key),element);
    }

    public K setKey(T element, K key) {
        return keys.set(values.indexOf(element),key);
    }
    public void add(K key, T element) {
        keys.add(key);
        values.add(element);
    }

    public K remove(K key) {
        values.remove(keys.indexOf(key));
        if (keys.remove(key)) {
            return key;
        }
        return null;
    }

    public T removeElement(T element) {
        keys.remove(values.indexOf(element));
        if (values.remove(element)) {
            return element;
        }
        return null;
    }

    public int length() {
        return keys.length();
    }
    public static <T> EasyDict<T,EasyList<T>> twoDtoDict(EasyList<EasyList<T>> twoD) {
        EasyDict<T,EasyList<T>> ret = new EasyDict<>();
        EasyList<T> keys = new EasyList<>();
        for (EasyList<T> oneD:twoD) {
            keys.add(oneD.get(0));
        }
        ret.addAll(keys,twoD);
        return ret;
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder("EasyDict{");
        for (int i = 0; i < keys.size(); i++) {
            K key = keys.get(i);
            T value = values.get(i);
            ret.append("\n{").append(key).append(":").append(value).append("}");
        }
        ret.append("\n}");
        return ret.toString();
    }
}
