package org.theBestCMISGroup.Utils.Storage;

import org.theBestCMISGroup.Utils.Storage.Utils.*;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

public class TreeStorage<T> implements SortableStorageInterface<T> {
    private Criteria.ComparableCriteria<T> sort;
    private T topItem;
    private TreeStorage<T> smallSide;
    private TreeStorage<T> bigSide;

    public TreeStorage() { // warning, this starts with one item already in it.
        this(null, new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
    }

    public TreeStorage(T topItem, Criteria.ComparableCriteria<T> sort) {
        this.topItem = topItem;
        this.sort = sort;
    }

    public static void main(String[] args) {
        doUnitTests();
    }

    @SuppressWarnings("UnusedReturnValue")
    public static boolean doUnitTests() {
        boolean goodSoFar = true;
        System.out.println("testing TreeStorage");
        System.out.println("test 1");
        goodSoFar &= testGetItemCount();
        System.out.println("test 2");
        goodSoFar &= testGetItemByStrangeCriteria();
        System.out.println("test 3");
        goodSoFar &= testGetItemByUUID();
        System.out.println("test 4");
        goodSoFar &= testGetItemByUUIDError();
        System.out.println("test 5");
        goodSoFar &= testCanAddItemWithDuplicateSortCriteria();
        System.out.println("test 6");
        goodSoFar &= testChangeSortCriteria();
        System.out.println("test 7");
        goodSoFar &= testInOrderTransversal();
        System.out.println("TreeStorage testing " + (goodSoFar ? "success" : "fail") + "!!!");
        return goodSoFar;
    }

    public static boolean testGetItemCount() {
        TreeStorage<String> testStorage = new TreeStorage<>();
        boolean testSuccess = true;
        for (int i = 1; i <= 1000; i++) {
            try {
                testStorage.addItem("Lorem Ipsum dolor seit..." + i);
            } catch (Errors.AdditionCriteriaException ignored) {
                System.out.println("fail");
                return false;
            }
            testSuccess &= testStorage.getItemCount() == i + 1;//account for the top null
        }
        System.out.println(testSuccess ? "success" : "fail");
        return testSuccess;
    }

    public static boolean testGetItemByStrangeCriteria() {
        TreeStorage<String> testStorage = new TreeStorage<>();
        try {
            testStorage.addItem("Lorem Ipsum dolor seit...");
            testStorage.addItem("find MEE!!!");
            testStorage.addItem("Lorem Ipsum dolor seit am...");
        } catch (Errors.AdditionCriteriaException ignored) {
            System.out.println("fail");
            return false;
        }
        @SuppressWarnings("StringEquality")
        boolean result = testStorage.getItem(string -> (string != null && string.equals("find MEE!!!"))) == "find MEE!!!";
        System.out.println(result ? "success" : "fail");
        return result;
    }

    public static boolean testGetItemByUUID() {
        class TestItemWithUUID extends ItemsWithUUIDs.ItemWithUUID {
        }
        TreeStorage<TestItemWithUUID> testStorage = new TreeStorage<>();
        TestItemWithUUID test = new TestItemWithUUID();
        try {
            testStorage.addItem(new TestItemWithUUID());
            testStorage.addItem(test);
            testStorage.addItem(new TestItemWithUUID());
        } catch (Errors.AdditionCriteriaException e) {
            System.out.println("fail");
            return false;
        }
        boolean result = testStorage.getItemByUUID(test.getUUID()) == test;
        System.out.println(result ? "success" : "fail");
        return result;
    }

    public static boolean testGetItemByUUIDError() {
        TreeStorage<String> testStorage = new TreeStorage<>();
        String test = "blah";
        try {
            testStorage.addItem("1");
            testStorage.addItem(test);
            testStorage.addItem("2");
        } catch (Errors.AdditionCriteriaException e) {
            System.out.println("fail");
            return false;
        }
        //this was a neat trick I got from ze inter-webs!
        PrintStream SOut = System.out;
        ByteArrayOutputStream tempSOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(tempSOut));
        boolean result = testStorage.getItemByUUID(UUID.randomUUID()) == null;
        System.setOut(SOut);
        result &= tempSOut.toString().contains("Error, Item being searched does not implement ItemsWithUUIDs.InterfaceOfItemsWithUUID, or extend ItemsWithUUIDs.ItemWithUUID in order to use this function.");
        System.out.println(result ? "success" : "fail");
        return result;
    }

    public static boolean testCanAddItemWithDuplicateSortCriteria() {
        TreeStorage<String> testTreeStorage = new TreeStorage<>(null, new Criteria.DerivedStringComparableCriteria<>() {
            @Override
            public String deriveString(String object) {
                return object.toCharArray()[0] + "";
            }
        });
        try {
            testTreeStorage.addItem("Mickey the Mouse");
        } catch (Errors.DuplicateItemException e) {
            System.out.println("fail");
            return false;
        }
        var sOut = System.out;
        System.setOut(new PrintStream(OutputStream.nullOutputStream()));
        boolean result = false;
        try {
            testTreeStorage.addItem("Mickey the House");
        } catch (Errors.DuplicateItemException e) {
            System.setOut(sOut);
            result = true;
        }
        System.setOut(sOut);
        result &= testTreeStorage.getItemCount() == 2;//count the null at the top
        System.out.println(result ? "success" : "fail");
        return result;
    }

    public static boolean testChangeSortCriteria() {
        TreeStorage<String> testStorage = new TreeStorage<>();
        for (int i = 0; i < 999; i++) {
            try {
                testStorage.addItem("item #" + (int) (Math.random() * 1000) + ";" + i);
            } catch (Errors.DuplicateItemException e) {
                System.out.println("fail");
                return false;
            }
        }
        testStorage.setSort(new Criteria.DerivedStringComparableCriteria<>() {
            @Override
            public String deriveString(String item) {
                return item.substring(item.indexOf(";"));
            }
        });
        boolean result = testStorage.isSorted();
        System.out.println(result ? "success" : "fail");
        return result;
    }

    public static boolean testInOrderTransversal() {
        TreeStorage<String> testStorage = new TreeStorage<>();
        String[] testCheck = new String[1001];
        for (int i = 0; i < 1000; i++) {
            testCheck[i] = "item #" + i;
            try {
                testStorage.addItem("item #" + i);
            } catch (Errors.DuplicateItemException e) {
                System.out.println("fail");
                return false;
            }
        }
        testStorage.sort();
        SortAndSearchUtils.sort(testCheck, new Criteria.DerivedStringComparableCriteria.ToStringComparableCriteria<>());
        String[] testIteration = new String[1001];
        testStorage.inOrderTransverse(new TransversalCode<>() {
            public String doForItem(String item, int itemNum) {
                System.out.println(item + ":" + itemNum);
                testIteration[itemNum] = item;
                return item;
            }
        });
        System.out.println(Arrays.toString(testCheck));
        System.out.println(Arrays.toString(testIteration));
        boolean result = Arrays.equals(testIteration, testCheck);
        System.out.println(result ? "success" : "fail");
        return result;
    }

    T getBiggestItem() {
        if (bigSide == null) {
            return topItem;
        } else return bigSide.getBiggestItem();
    }

    T getSmallestItem() {
        if (smallSide == null) {
            return topItem;
        } else return smallSide.getSmallestItem();
    }
    T getTopItem() {
        return topItem;
    }
    @Override
    public void addItems(T[] items) throws Errors.AdditionCriteriaException {
        for (T item:items) {
            addItem(item);
        }
    }
    @Override
    public void addItem(T item) throws Errors.DuplicateItemException {
        if (sort.compareUsingCriteria(topItem, item) == 0) {
            System.out.println("Error, another item that matches this item's sort value is already in Storage, refusing to add item with duplicate sort value.");
            throw new Errors.DuplicateItemException();
        } else if (sort.compareUsingCriteria(topItem, item) > 0) {
            if (smallSide == null) {
                smallSide = new TreeStorage<>(item, sort);
            } else {
                smallSide.addItem(item);
            }
        } else {
            if (bigSide == null) {
                bigSide = new TreeStorage<>(item, sort);
            } else {
                bigSide.addItem(item);
            }
        }
    }

    @Override
    public int getItemCount() {
        return 1 + (smallSide == null ? 0 : smallSide.getItemCount()) + (bigSide == null ? 0 : bigSide.getItemCount());
    }

    @Override
    public T getItem(Criteria.MatchableCriteria<T> criteria) {
        if (criteria.matchesCriteria(topItem)) return topItem;
        T smallSideResult = smallSide == null ? null : smallSide.getItem(criteria);
        if (smallSideResult != null) return smallSideResult;
        return bigSide == null ? null : bigSide.getItem(criteria);
    }

    @Override
    public EasyList<T> toEasyList() {
        return new EasyList<>(toTypedArray());
    }

    @Override
    public T[] toTypedArray() {
        return deTree();
    }

    @Override
    public int indexOf(Criteria.MatchableCriteria<T> crit) {
        AtomicInteger ind = new AtomicInteger(-1);
        inOrderTransverse(new TransversalCode<>() {
            @Override
            public T doForItem(T item, int itemNum) {
                if (crit.matchesCriteria(item)) {
                    if (ind.get()>-1&&ind.get()>itemNum) {
                        ind.set(itemNum);
                    }
                }
                return item;
            }
        });
        return ind.get();
    }

    @Override
    public int lastIndexOf(Criteria.MatchableCriteria<T> crit) {
        AtomicInteger ind = new AtomicInteger(-1);
        inOrderTransverse(new TransversalCode<>() {
            @Override
            public T doForItem(T item, int itemNum) {
                if (crit.matchesCriteria(item)) {
                    if (ind.get()>-1&&ind.get()<itemNum) {
                        ind.set(itemNum);
                    }
                }
                return item;
            }
        });
        return ind.get();
    }

    @SuppressWarnings("unused")
    @Override
    public Criteria.ComparableCriteria<T> getSort() {
        return sort;
    }

    @Override
    public void setSort(Criteria.ComparableCriteria<T> newSort) {
        sort = newSort;
        sort();
    }@Override
    public T getItemUsingSort(T itemThatMatchesCriteria) {
        int comparatively = sort.compareUsingCriteria(topItem, itemThatMatchesCriteria);
        if (comparatively == 0) {
            return topItem;
        } else if (comparatively > 0) {
            if (smallSide == null) {
                return null;
            }
            return smallSide.getItemUsingSort(itemThatMatchesCriteria);
        } else {
            if (bigSide == null) {
                return null;
            }
            return bigSide.getItemUsingSort(itemThatMatchesCriteria);
        }
    }
    public T getItemUsingDerivedStringSorting(String stringDerivedFromAnItemThatMatchesCriteria) {
        try {
            Criteria.DerivedStringComparableCriteria<T> sort = (Criteria.DerivedStringComparableCriteria<T>)this.sort;
            int comparatively = sort.compareToPreDerivedStringUsingCriteria(topItem, stringDerivedFromAnItemThatMatchesCriteria);
            if (comparatively == 0) {
                return topItem;
            } else if (comparatively > 0) {
                if (smallSide == null) {
                    return null;
                }
                return smallSide.getItemUsingDerivedStringSorting(stringDerivedFromAnItemThatMatchesCriteria);
            } else {
                if (bigSide == null) {
                    return null;
                }
                return bigSide.getItemUsingDerivedStringSorting(stringDerivedFromAnItemThatMatchesCriteria);
            }
        } catch (ClassCastException e) {
            System.out.println("Error, sorting criteria is not a DerivedStringComparableCriteria<T>.");
            return null;
        }
    }

    @Override
    public void sort() {
        if (isSorted()) return;
        reBuildSort();
    }

    public void reBuildSort() {
        if (isSorted()) return;
        T[] reAddThese = deTree();
        topItem = reAddThese[0];
        for (int i = 1; i < reAddThese.length; i++) {
            T item = reAddThese[i];
            try {
                addItem(item);
            } catch (Errors.DuplicateItemException e) {
                System.out.println("duplicate items with this sort... ERROR.");
                throw new RuntimeException(e);
            }
        }
    }

    private T[] deTree() {
        //noinspection unchecked
        Stream<T> top = topItem == null ? Stream.empty() : Arrays.stream((T[]) new Object[]{topItem});
        Stream<T> small = smallSide == null ? Stream.empty() : Arrays.stream(smallSide.deTree());
        Stream<T> big = bigSide == null ? Stream.empty() : Arrays.stream(bigSide.deTree());
        //noinspection unchecked
        return (T[]) Stream.concat(Stream.concat(small, top), big).toArray(Object[]::new);
    }

    public boolean isSorted() {
        return (smallSide == null || (sort.compareUsingCriteria(smallSide.getBiggestItem(), topItem) <= 0) && (bigSide == null || sort.compareUsingCriteria(topItem, bigSide.getSmallestItem()) <= 0) && ((smallSide == null || smallSide.isSorted()) & (bigSide == null || bigSide.isSorted())));
    }

    public void inOrderTransverse(TransversalCode<T> codeToRun) {
        int itemsSoFar = 0;
        this.inOrderTransverse(codeToRun, itemsSoFar);
    }

    private int inOrderTransverse(TransversalCode<T> codeToRun, int itemsSoFar) {
        //big to small, because the linear list sort does it that way.
        if (bigSide != null) {
            itemsSoFar = bigSide.inOrderTransverse(codeToRun, itemsSoFar);
        }
        this.topItem = codeToRun.doForItem(this.topItem, itemsSoFar);
        itemsSoFar++;
        if (smallSide != null) {
            itemsSoFar = smallSide.inOrderTransverse(codeToRun, itemsSoFar);
        }
        return itemsSoFar;
    }

    @Override
    public String toString() {
        return "TreeStorage{" +
                ", topItem=" + topItem +
                ", smallSide=" + smallSide +
                ", bigSide=" + bigSide +
                '}';
    }

    @Override
    public int size() {
        return getItemCount();
    }

    @Override
    public boolean isEmpty() {
        return getItemCount()>0;
    }

    @Override
    public boolean contains(Object o) {
        return getItem((i)->i==o)!=null;
    }

    @Override
    public Iterator<T> iterator() {
        return toEasyList().iterator();
    }

    @Override
    public Object[] toArray() {
        return toTypedArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return toEasyList().toArray(a);
    }

    @Override
    public boolean add(T t) {
        try {
            addItem(t);
        } catch (Errors.DuplicateItemException e) {
            return false;
        }
        return true;
    }
    public T removeItem(T item) {
        int comparatively = sort.compareUsingCriteria(topItem, item);
        if (comparatively == 0) {
            if (smallSide==null) {
                if (bigSide==null) {
                    //both null
                    topItem=null;
                } else {
                    //only big side exists
                    this.topItem=bigSide.getTopItem();
                    this.smallSide=bigSide.smallSide;
                    this.bigSide=bigSide.bigSide;
                }
            } else if (bigSide==null) {
                //only small side exists
                this.topItem = smallSide.getTopItem();
                this.bigSide = smallSide.bigSide;
                this.smallSide = smallSide.smallSide;
            } else {
                //neither are null
                //lets pull up the left side.
                topItem=smallSide.getBiggestItem();
                smallSide.removeItem(topItem);
            }
            return item;
        } else if (comparatively > 0) {
            if (smallSide == null) {
                return null;
            }
            return smallSide.getItemUsingSort(item);
        } else {
            if (bigSide == null) {
                return null;
            }
            return bigSide.getItemUsingSort(item);
        }
    }
    @Override
    public boolean remove(Object o) {
        try {
            //noinspection unchecked
            return removeItem((T) o)!=null;
        } catch (ClassCastException e) {
            return false;
        }
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean ret = true;
        for (Object o : c) {
            ret&=!contains(o);
        }
        return ret;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean ret = false;
        for (T t : c) {
            ret |= add(t);
        }
        return ret;
    }
    public boolean addAll(T[] c) {
        boolean ret = false;
        for (T t : c) {
            ret |= add(t);
        }
        return ret;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean ret = false;
        for (Object o : c) {
            ret |= remove(o);
        }
        return ret;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean ret = false;
        for (T t : this) {
            if (!c.contains(t)) {
                ret |= remove(t);
            }
        }
        return ret;
    }

    @Override
    public void clear() {
        this.topItem=null;
        this.smallSide=null;
        this.bigSide=null;
    }

    @Override
    public T get(int index) {
        AtomicReference<T> item = new AtomicReference<>();
        inOrderTransverse(new TransversalCode<>() {
            @Override
            public T doForItem(T itema, int itemNum) {
                if (itemNum==index) {
                    item.set(itema);
                }
                return itema;
            }
        });
        return item.get();
    }

    @Override
    public T set(int index, T element) {
        throw new RuntimeException("something tried setting an item by index in a sorted list. this method has to be here, but please don't use it.");
    }

    @Override
    public void add(int index, T element) {
        throw new RuntimeException("something tried setting an item by index in a sorted list. this method has to be here, but please don't use it.");
    }

    @Override
    public T remove(int index) {
        T remMe = get(index);
        remove(remMe);
        return remMe;
    }

    @Override
    public int indexOf(Object o) {
        return toEasyList().indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return toEasyList().lastIndexOf(o);
    }

    @Override
    public ListIterator<T> listIterator() {
        return listIterator(0);
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        TreeStorage<T> myself=this;
        return new ListIterator<>() {
            final TreeStorage<T> me = myself;
            int ind = index;
            @Override
            public boolean hasNext() {
                return myself.size()>ind+1;
            }

            @Override
            public T next() {
                ind++;
                return me.get(ind);
            }

            @Override
            public boolean hasPrevious() {
                return ind>0;
            }

            @Override
            public T previous() {
                ind--;
                return me.get(ind+1);
            }

            @Override
            public int nextIndex() {
                return ind+1;
            }

            @Override
            public int previousIndex() {
                return ind;
            }

            @Override
            public void remove() {
                me.remove(ind);
            }

            @Override
            public void set(T t) {
                me.set(ind,t);
            }

            @Override
            public void add(T t) {
                me.add(ind,t);
            }
        };
    }
    public TreeStorage<T> subTree(T topItem) {
        int comparatively = sort.compareUsingCriteria(this.topItem, topItem);
        if (comparatively == 0) {
            return this;
        } else if (comparatively > 0) {
            if (smallSide == null) {
                return null;
            }
            return smallSide.subTree(topItem);
        } else {
            if (bigSide == null) {
                return null;
            }
            return bigSide.subTree(topItem);
        }
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return toEasyList().subList(fromIndex,toIndex);
    }
}
