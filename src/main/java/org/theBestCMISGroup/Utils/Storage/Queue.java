package org.theBestCMISGroup.Utils.Storage;

import org.theBestCMISGroup.Utils.Storage.Utils.Criteria;

import java.util.*;

public class Queue<T> implements BasicStorageInterface<T> {
    private QueuedItem<T> front;
    private QueuedItem<T> back;
    public Queue() {}
    public Queue(T firstItem) {
        enqueue(firstItem);
    }
    public Queue(T[] startItems) {
        for (T addItem : startItems) {
            enqueue(addItem);
        }
    }
    public T[] getStrungOut() {
        @SuppressWarnings("unchecked")
        T[] items = (T[]) new Object[size()];
        QueuedItem<T> item = front;
        int itemNum=0;
        while (item!=null) {
            items[itemNum]=item.item;
            item=item.nextItem;
            itemNum++;
        }
        return items;
    }
    @Override
    public void addItems(T[] items) {
        for (T item:items) {
            addItem(item);
        }
    }
    @Override
    public void addItem(T item) {
        enqueue(item);
    }

    public void enqueue(T addItem) {
        if (addItem instanceof QueuedItem.QueueTrackable) {
            ((QueuedItem.QueueTrackable)addItem).queueUp();
        }
        if (front ==null) {
            front = new QueuedItem<>(addItem);
            back = front;
        } else {
            back.nextItem= new QueuedItem<>(addItem);
            back = back.nextItem;
        }
    }
    public void enqueueAtSpot(int positionInLine, T addItem) {
        if (positionInLine<0 || positionInLine> size()) {
            throw new IndexOutOfBoundsException();
        } else {
            {
                if (positionInLine==0) {
                    front = new QueuedItem<>(front, addItem);
                } else {
                    int i=1;
                    QueuedItem<T> putUnder= front;
                    while (i<positionInLine) {
                        putUnder=putUnder.nextItem;
                        i++;
                    }
                    putUnder.nextItem= new QueuedItem<>(putUnder.nextItem, addItem);
                }
            }
            if (addItem instanceof QueuedItem.QueueTrackable) {
                ((QueuedItem.QueueTrackable)addItem).queueUp();
            }
        }
    }
    public T dequeue() {
        if (size() >=1) {
            T ret = front.item;
            front = front.nextItem;
            if (ret instanceof QueuedItem.QueueTrackable) {
                ((QueuedItem.QueueTrackable)ret).queueDown();
            }
            return ret;
        } else {
            return null;
        }
    }
    public int dequeueItem(T item) {
        //returns -1 if not found, or spot in line starting at 0 if found and removed
        int foundspot = -1;
        QueuedItem<T> previousExamSpot=null;
        QueuedItem<T> examinationSpot = front;
        for (int i = 0; i < size(); i++) {
            if (examinationSpot.item==item) {
                if (previousExamSpot==null) {
                    front=examinationSpot.nextItem;
                } else {
                    previousExamSpot.nextItem=examinationSpot.nextItem;
                }
                foundspot=i;
                if (item instanceof QueuedItem.QueueTrackable) {
                    ((QueuedItem.QueueTrackable)item).queueDown();
                }
                break;
            }
            previousExamSpot=examinationSpot;
            examinationSpot=examinationSpot.nextItem;
        }
        return foundspot;
    }
    public T getItem(Criteria.MatchableCriteria<T> itemFilter) {
        //returns null if not found, or item if found
        QueuedItem<T> examinationSpot = front;
        for (int i = 0; i < size(); i++) {
            if (itemFilter.matchesCriteria(examinationSpot.item)) {
                return examinationSpot.item;
            }
            examinationSpot=examinationSpot.nextItem;
        }
        return null;
    }
    public boolean contains(Object item) {
        T potentialContent;
        //noinspection UnreachableCode
        try {
            //noinspection unchecked
            potentialContent = (T) item;
        } catch (ClassCastException e) {
            return false;
        }
        T finalPotentialContent = potentialContent;
        return getItem(otherItem -> finalPotentialContent ==otherItem)!=null;
    }
    public EasyList<T> toEasyList() {
        EasyList<T> ret = new EasyList<>();
        QueuedItem<T> examinationSpot = front;
        while (examinationSpot!=null) {
            ret.add(examinationSpot.item);
            examinationSpot=examinationSpot.nextItem;
        }
        return ret;
    }
    public T[] toTypedArray() {
        return toEasyList().toTypedArray();
    }
    @Override
    public Object[] toArray() {
        return toTypedArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return toEasyList().toArray(a);
    }

    @Override
    public Iterator<T> iterator() {
        return Arrays.stream(toTypedArray()).iterator();
    }

    @Override
    public boolean add(T t) {
        addItem(t);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        T potentialRemove;
        //noinspection UnreachableCode
        try {
            //noinspection unchecked
            potentialRemove = (T) o;
        } catch (ClassCastException e) {
            return false;
        }
        return dequeueItem(potentialRemove)!=-1;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T t : c) {
            add(t);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        int ind=index;
        for (T t : c) {
            add(ind,t);
            ind++;
        }
        return true;
    }

    @Override
    public void add(int index, T element) {
        enqueueAtSpot(index,element);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean ret = false;
        for (Object o : c) {
            ret|=remove(o);
        }
        return ret;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean ret = false;
        for (T t : this) {
            if (!c.contains(t)) {
                ret|=this.remove(t);
            }
        }
        return ret;
    }

    @Override
    public void clear() {
        this.front=null;
        this.back=null;
    }

    @Override
    public T get(int index) {
        int ind = 0;
        QueuedItem<T> examinationSpot = front;
        while (examinationSpot!=null) {
            if (ind==index) {
                return examinationSpot.item;
            }
            examinationSpot=examinationSpot.nextItem;
            ind++;
        }
        return null;
    }

    @Override
    public T set(int index, T element) {
        enqueueAtSpot(index,element);
        return remove(index+1);
    }

    @Override
    public T remove(int index) {
        T item = get(index);
        remove(item);
        return item;
    }
    public int indexOf(Criteria.MatchableCriteria<T> crit) {
        int ind = 0;
        QueuedItem<T> examinationSpot = front;
        while (examinationSpot!=null) {
            if (crit.matchesCriteria(examinationSpot.item)) {
                return ind;
            }
            examinationSpot=examinationSpot.nextItem;
            ind++;
        }
        return -1;
    }

    @Override
    public int indexOf(Object o) {
        return indexOf(item -> item==o);
    }
    public int lastIndexOf(Criteria.MatchableCriteria<T> crit) {
        int lastFoundInd = -1;
        int ind = 0;
        QueuedItem<T> examinationSpot = back;
        while (examinationSpot!=null) {
            if (crit.matchesCriteria(examinationSpot.item)) {
                lastFoundInd=ind;
            }
            examinationSpot=examinationSpot.nextItem;
            ind++;
        }
        return lastFoundInd;
    }

    @Override
    public int lastIndexOf(Object o) {
        return lastIndexOf(item -> item==o);
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        Queue<T> myself=this;
        return new ListIterator<>() {
            int ind = index;
            final Queue<T> me = myself;
            @Override
            public boolean hasNext() {
                return myself.size()>ind+1;
            }

            @Override
            public T next() {
                ind++;
                return me.get(ind);
            }

            @Override
            public boolean hasPrevious() {
                return ind>0;
            }

            @Override
            public T previous() {
                ind--;
                return me.get(ind+1);
            }

            @Override
            public int nextIndex() {
                return ind+1;
            }

            @Override
            public int previousIndex() {
                return ind;
            }

            @Override
            public void remove() {
                me.remove(ind);
            }

            @Override
            public void set(T t) {
                me.set(ind,t);
            }

            @Override
            public void add(T t) {
                me.add(ind,t);
            }
        };
    }

    @Override
    public ListIterator<T> listIterator() {
        return listIterator(0);
    }
    public Queue<T> subQueue(int fromIndex, int toIndex) {
        return new Queue<>(toEasyList().subEasyList(fromIndex,toIndex).toTypedArray());
    }
    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return subQueue(fromIndex,toIndex);
    }

    public int getItemCount() {
        return size()+1;
    }

    @Override
    public boolean isEmpty() {
        return front==null;
    }

    public int size() {
        int size = 0;
        QueuedItem<T> item=front;
        while (item!=null) {
            item=item.nextItem;
            size++;
        }
        return size;
    }

    @Override
    public String toString() {
        return "Queue{" +
                "\nfront=" + front +
                ", \nback=" + back +
                ", \nsize=" + size() +
                '}';
    }
    public static class QueuedItem<T> {
        public T item;
        public QueuedItem<T> nextItem;
        public QueuedItem() {}
        public QueuedItem(QueuedItem<T> nextItem,T item) {
            this.nextItem=nextItem;
            this.item=item;
        }
        public QueuedItem(T item) {
            this.item=item;
        }
        @Override
        public String toString() {
            StringBuilder toSt = new StringBuilder("QueuedItemList{\n");
            QueuedItem<T> item = this;
            while (item!=null) {
                toSt.append(item.item.toString()).append(item.nextItem != null ? ",\n" : "\n");
                item=item.nextItem;
            }
            toSt.append("}");
            return toSt.toString();
        }
        public interface QueueTrackable {
            void queueUp();
            void queueDown();
        }
    }
}
