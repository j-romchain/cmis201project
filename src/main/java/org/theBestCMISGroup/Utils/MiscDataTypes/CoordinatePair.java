package org.theBestCMISGroup.Utils.MiscDataTypes;

public class CoordinatePair {
    public CoordinatePair() {
        this(0,0);
    }
    public CoordinatePair(CoordinatePair cloneMe) {
        this(cloneMe.getLongitude(),cloneMe.getLatitude());
    }
    public CoordinatePair(String[] xy) {
        this(xy[0],xy[1]);
    }
    public CoordinatePair(String longitude, String latitude) {
        this(Double.parseDouble(longitude),Double.parseDouble(latitude));
    }
    public CoordinatePair(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }
    final double longitude;
    final double latitude;
    public double getLongitude() {
        return longitude;
    }
    public double getLatitude() {
        return latitude;
    }
//    public double getDistanceTo(CoordinatePair otherPair) {
//        double xDiff = Math.abs(getLongitude()-otherPair.getLongitude());
//        double yDiff = Math.abs(getLatitude()-otherPair.getLatitude());
//        return Math.sqrt(xDiff*xDiff+yDiff*yDiff);
//    } we are dealing in latitude and longitude for the maps now...
    public double getDistanceTo(CoordinatePair otherPair) {
        final double earthRadiusInMiles = 3959; // Earth's radius in miles

        // Convert latitudes and longitudes to radians
        double lat1Rad = Math.toRadians(getLatitude());
        double lon1Rad = Math.toRadians(getLongitude());
        double lat2Rad = Math.toRadians(otherPair.getLatitude());
        double lon2Rad = Math.toRadians(otherPair.getLongitude());

        // Implement Haversine formula
        double dLat = Math.abs(lat2Rad - lat1Rad);
        double dLon = Math.abs(lon2Rad - lon1Rad);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(lat1Rad) * Math.cos(lat2Rad) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        // Distance in radians multiplied by Earth's radius in miles
        return c * earthRadiusInMiles;
    }


    @Override
    public String toString() {
        return "("+ longitude +','+ latitude +')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CoordinatePair that)) return false;
        return Double.compare(getLongitude(), that.getLongitude()) == 0 && Double.compare(getLatitude(), that.getLatitude()) == 0;
    }
}
