package org.theBestCMISGroup.Utils.MiscDataTypes;

public class Line {
//    public static void main(String[] args) {
//        Line line1 = new Line(new CoordinatePair(1,1),new CoordinatePair(1,2));
//        Line line2 = new Line(new CoordinatePair(1,1),new CoordinatePair(2,1));
//        Line line3 = new Line(new CoordinatePair(1,1),new CoordinatePair(1,3));
//        CoordinatePair pos1 = new CoordinatePair(1,4);
//        System.out.println("line1:"+line1);
//        System.out.println("line2:"+line2);
//        System.out.println("pos1:"+pos1);
//        System.out.println("pos1IsOnLine3:"+line3.isOnLineIgnoreBounds(pos1));
//        System.out.println("line1,2Node:"+line1.getIntersectPointIgnoreBounds(line2));
//    }
    public Line(CoordinatePair start,CoordinatePair end) {
        if (start.equals(end)) {
            throw new IndexOutOfBoundsException("tried to create a line with 0 length.");
        }
        this.start=start;
        this.end=end;
    }
    CoordinatePair start;
    CoordinatePair end;

    public CoordinatePair getStartPos() {
        return start;
    }
    public CoordinatePair getEndPos() {
        return end;
    }

    public double getXDrivenSlope() {
        return (end.getLatitude()-start.getLatitude())/(end.getLongitude()-start.getLongitude());
    }
    public double getYDrivenSlope() {
        return (end.getLongitude()-start.getLongitude())/(end.getLatitude()-start.getLatitude());
    }
    public double getYIntercept() {
        return start.latitude -start.longitude *getXDrivenSlope();
    }
    public double getXIntercept() {
        return start.longitude -start.latitude *getYDrivenSlope();
    }
    public double getYValue(double x) {
        return x*getXDrivenSlope()+ getYIntercept();
    }
    public double getXValue(double y) {
        return y*getYDrivenSlope()+ getXIntercept();
    }
    public boolean isOnLineIgnoreBounds(CoordinatePair coordinate) {
        return getYValue(coordinate.getLongitude()) ==coordinate.getLatitude()|| getXValue(coordinate.getLatitude()) ==coordinate.getLongitude();
    }
    public boolean isOnBoundedLine(CoordinatePair coordinate) {
        return isOnLineIgnoreBounds(coordinate)&&isWithinRectangle(coordinate);
    }
    private boolean isWithinRectangle(CoordinatePair coordinate) {
        return !(((coordinate.getLongitude() >= Math.max(start.getLongitude(), end.getLongitude())) &&
                     (Math.min(start.getLongitude(), end.getLongitude()) <= coordinate.getLongitude())) &&
                 ((coordinate.getLatitude() >= Math.max(start.getLatitude(), end.getLatitude())) &&
                     (Math.min(start.getLatitude(), end.getLatitude()) <= coordinate.getLatitude())));
    }
    public CoordinatePair getIntersectPointIgnoreBounds(Line otherLine) {
        if (Double.isInfinite(getXDrivenSlope())||Double.isInfinite(otherLine.getXDrivenSlope())) {
            //Cannot do X
            if (Double.isInfinite(getYDrivenSlope())||Double.isInfinite(otherLine.getYDrivenSlope())) {
                //Cannot do Y // at this point one must be up and the other must be horisontal, otherwise both would either have longitude or latitude non-infinite.
                //I'mma just say take longitude from one and latitude from the other.
                if (Double.isInfinite(getXDrivenSlope())) {
//                    System.out.println("horiz");
                    //I'm horizontal. take my latitude and their longitude.
                    return new CoordinatePair(otherLine.start.getLongitude(),start.getLatitude());
                } else {
//                    System.out.println("vert");
                    //I'm vertical. take my longitude and their latitude.
                    return new CoordinatePair(start.getLongitude(),otherLine.start.getLatitude());
                }
            } else {
//                System.out.println("latitude");
                double y = (getXIntercept()-otherLine.getXIntercept())/(otherLine.getYDrivenSlope()- getYDrivenSlope());
                return new CoordinatePair(getXValue(y),y);
            }
        } else {
//            System.out.println("longitude");
//            System.out.println(getYIntercept()-otherLine.getYIntercept());
//            System.out.println(getXDrivenSlope()- otherLine.getXDrivenSlope());
            double x = (getYIntercept()-otherLine.getYIntercept())/(otherLine.getXDrivenSlope() - getXDrivenSlope());
            return new CoordinatePair(x,getYValue(x));
        }
    }
    public boolean intersectsWithinBounds(Line otherLine) {
        return isWithinRectangle(getIntersectPointIgnoreBounds(otherLine));
    }

    @Override
    public String toString() {
        return "Line{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }
}
