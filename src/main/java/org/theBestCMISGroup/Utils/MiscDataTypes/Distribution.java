package org.theBestCMISGroup.Utils.MiscDataTypes;

import org.apache.commons.jexl3.JexlBuilder;
import org.apache.commons.jexl3.MapContext;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

public interface Distribution {


    // for a distribution function f(longitude) where it depicts the distribution,
    // for variables longitude=0 to longitude=b where the area under the graph f(longitude) from 0 to b is 1,
    // find the integral of the function,
    // which will be able to find the random number which will correspond to each output, given the output,
    // and then swap longitude and latitude, so that you find the output given the random number,
    // and tada, you now have a function you can feed a random number from 0 to 1 into,
    // and it will adjust the numbers to match the distribution you started with.
    double sample();
    class LinearDistribution implements Distribution {
        final double value;
        public LinearDistribution(double slope) {
            value=slope;
        }

        @Override
        public double sample() {
            return value;
        }
    }
    interface RandomDistribution extends Distribution {

        class LinearDistribution implements RandomDistribution { // it's not really random but whatevs.
            final double value;
            public LinearDistribution(double slope) {
                value=slope;
            }

            @Override
            public double sample() {
                return value;
            }
        }
        static RandomDistribution parse(List<String> data) throws ParseException {
            if (data.get(0).toLowerCase().contains("lin")) {
                return new LinearDistribution(Double.parseDouble(data.get(1)));
            } else if (data.get(0).toLowerCase().contains("exp")) {
                return new ExponentialDistribution(Double.parseDouble(data.get(1)));
            } else if (data.get(0).toLowerCase().contains("equ")) {
                return new EqualDistribution();
            } else if (data.get(0).toLowerCase().contains("cust")) {
                return () -> { // JEXL is to be used here and only here because math can get complex, and if I ever use this again I want the flexibility of ability to parse a math equasion.
                    return (double) new JexlBuilder().create().createExpression(data.get(1).replace("longitude",Math.random()+"")).evaluate(new MapContext());
                };
            } else {
                throw new ParseException(Arrays.toString(data.toArray()) +" was not an exponential, equal, linear, or custom Jexl equasion of longitude distribution and thats all we can handle parsing at the moment",0);
            }
        }
        class EqualDistribution implements RandomDistribution {
            public EqualDistribution(){}
            @Override
            public double sample() {
                return Math.random();
            }
        }
        class ExponentialDistribution implements RandomDistribution {
            final double lambda;
            final EqualDistribution equalDistribution;
            public ExponentialDistribution(double lambda) {
                this.lambda=lambda;
                equalDistribution=new EqualDistribution();
            }
            @Override
            public double sample() {
                return (-1/lambda)*Math.log(equalDistribution.sample());
            }
        }
//        public class NormalDistribution implements RandomDistribution {
//
//            @Override
//            public double sample() {
//                return 0;
//            }
//        }
    }
}
