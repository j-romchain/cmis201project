package org.theBestCMISGroup.Mapping;

import org.theBestCMISGroup.Roads.Road;
import org.theBestCMISGroup.Utils.MiscDataTypes.CoordinatePair;
import org.theBestCMISGroup.Utils.Storage.EasyList;
import org.theBestCMISGroup.Vehicle.BusStop;

import java.util.List;


public class Node {
    private EasyList<TrafficLane> outTrafficLanes = new EasyList<>();
    private EasyList<TrafficLane> inTrafficLanes = new EasyList<>();
    private EasyList<Road> connectedRoads = new EasyList<>();
    private BusStop myBusStop;
    private final CoordinatePair pos;

    public Node(CoordinatePair nodeCoord) {
        this.pos=nodeCoord;
    }
    public EasyList<TrafficLane> getWaysOut() {
        return outTrafficLanes;
    }

    public TrafficLane getFastestDirectWayOutTo(Node node) {
        return getFastestDirectWayOutTo(node.getPos());
    }

    public TrafficLane getFastestDirectWayOutTo(CoordinatePair location) {
        TrafficLane fastest = null;
        double fastestTransversalTime = Double.MAX_VALUE;
        for (TrafficLane outTrafficLane : outTrafficLanes) {
            if (outTrafficLane.getEndNode().getPos().equals(location)) {
                double tTT = outTrafficLane.timeToTransverseSeconds();
                if (outTrafficLane.timeToTransverseSeconds()<fastestTransversalTime) {
                    fastest=outTrafficLane;
                    fastestTransversalTime=tTT;
                }
            }
        }
        return fastest;
    }



    public TrafficLane getFastestDirectWayInFrom(Node node) {
        return getFastestDirectWayInFrom(node.getPos());
    }
    public TrafficLane getFastestDirectWayInFrom(CoordinatePair location) {
        TrafficLane fastest = null;
        double fastestTransversalTime = Double.MAX_VALUE;
        for (TrafficLane inTrafficLane : inTrafficLanes) {
            if (inTrafficLane.getStartNode().getPos().equals(location)) {
                double tTT = inTrafficLane.timeToTransverseSeconds();
                if (inTrafficLane.timeToTransverseSeconds()<fastestTransversalTime) {
                    fastest=inTrafficLane;
                    fastestTransversalTime=tTT;
                }
            }
        }
        return fastest;
    }
    public EasyList<TrafficLane> getWaysin() {
        return inTrafficLanes;
    }
    public EasyList<Road> getConnectedRoads() {
        return connectedRoads;
    }
    public BusStop getBusStop() {
        return myBusStop;
    }
    public CoordinatePair getPos() {
        return pos;
    }
    public void addRoad(Road addMe) {
        if (addMe.getStartNode()==this) {
            outTrafficLanes.addAll(List.of(addMe.getTrafficLanesFromStart()));
            inTrafficLanes.addAll(List.of(addMe.getTrafficLanesToStart()));
        } else if (addMe.getEndNode()==this) {
            outTrafficLanes.addAll(List.of(addMe.getTrafficLanesFromEnd()));
            inTrafficLanes.addAll(List.of(addMe.getTrafficLanesToEnd()));
        } else {
            throw new RuntimeException("something tried adding a road to a node it is not involved with. road:"+addMe+" node:"+ this);
        }
        connectedRoads.add(addMe);
    }
    public void initBusStop() {
        myBusStop=new BusStop(this);
    }

    @Override
    public boolean equals(Object o){
        if (!(o instanceof Node that)) return false;
        return (this.getPos().equals(that.getPos()));
    }
}
