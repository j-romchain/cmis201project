package org.theBestCMISGroup.Mapping;

import org.theBestCMISGroup.Utils.MiscDataTypes.CoordinatePair;
import org.theBestCMISGroup.Utils.Simulation.SimulatableGlobalTime;
import org.theBestCMISGroup.Utils.Storage.SortedStorage;
import org.theBestCMISGroup.Vehicle.Vehicle;

import java.util.Date;

public class TrafficLane {
    private final SortedStorage<Traveler> carsInLane = new SortedStorage<>((item1, item2) -> {
        if (item1==item2) {return 0;}
        if (item1==null) {return -1;}
        if (item2==null) {return 1;}
        return item1.getTravelStart().compareTo(item2.getTravelStart());
    });
    private int carsOnRoadAtSameTimeAsFirstCar = 0;
    public Node startNode;
    public Node endNode;
    public TrafficLane(Node startNode, Node endNode, int speedLimitMPH) {
        {
            double lengthInMiles = startNode.getPos().getDistanceTo(endNode.getPos());

            double hpm = 1d/(double)speedLimitMPH;
            double spm = 60*60*hpm;
            smallestTravelTime = spm*lengthInMiles;

            capacity= (int) Math.ceil(smallestTravelTime/3d);//3 seconds between cars... this feels like cheating...

        }
        this.startNode=startNode;
        this.endNode=endNode;
    }
    private Traveler getFirstCar() {
        return carsInLane.get(carsInLane.size()-1);
    }
    private void remFirstCar() {
        carsInLane.remove(carsInLane.size()-1);
    }
    private boolean checkIfCarCanGo(Date carJoinTime) {
        return SimulatableGlobalTime.getTime().after(generateReleaseTime(carJoinTime));
    }
    private Date generateReleaseTime(Date joinTime) {
        long joinMS = joinTime.getTime();
        long joinAndWaitMS = joinMS+(long)(timeToTransverseSeconds()*1000);
        return new Date(joinAndWaitMS);
    }
    private Date generateReleaseTimeNoTraffic(Date joinTime) {
        long joinMS = joinTime.getTime();
        long joinAndWaitMS = joinMS+(long)(smallestTravelTime*1000);
        return new Date(joinAndWaitMS);
    }
    private final double smallestTravelTime;//shortest time a car can go through without speeding, in seconds
    private final int capacity;//most cars that can be going at smallestTravelTime speeds.
    public void tick() {
        while (checkIfCarCanGo(getFirstCar().getTravelStart())) {
            getFirstCar().getMe().dropAt(endNode,generateReleaseTime(getFirstCar().getTravelStart()));
            remFirstCar();
        }
        //drop cars from carsInLane at end node at whatever rate is deemed worthy...
    }
    public Node getStartNode() {
        return startNode;
    }
    public Node getEndNode() {
        return endNode;
    }
    public CoordinatePair getStartPos(){
        return getStartNode().getPos();
    }
    public CoordinatePair getEndPos(){
        return getEndNode().getPos();
    }
    public double timeToTransverseSecondsNoTraffic() {
        return smallestTravelTime;
    }
    public double timeToTransverseSeconds() {
        return smallestTravelTime * Math.min(1, Math.pow(percenageOfCapacity(), 2));
    }
    private double percenageOfCapacity() {
        updateCarsAtSameTime();
        return (double) carsOnRoadAtSameTimeAsFirstCar/capacity;
    }

    private void updateCarsAtSameTime() {
        carsOnRoadAtSameTimeAsFirstCar=0;
        Date cutoff = generateReleaseTimeNoTraffic(getFirstCar().getTravelStart());
        for (Traveler traveler : carsInLane) {
            if (traveler.getTravelStart().before(cutoff)) {
                carsOnRoadAtSameTimeAsFirstCar++;
            }
        }
    }

    public void travel(Vehicle car, Date currentTime) {
        carsInLane.add(new Traveler(car,currentTime));
    }
    private static class Traveler{
        public Traveler(Vehicle vehicle, Date joinTime) {
            me=vehicle;
            travelStart=joinTime;
        }
        private final Vehicle me;
        private final Date travelStart;
        public Vehicle getMe() {
            return me;
        }
        public Date getTravelStart() {
            return travelStart;
        }
    }
}
