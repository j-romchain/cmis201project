package org.theBestCMISGroup.Mapping;

import org.theBestCMISGroup.FileManagement.Utils.OSMMapReader;
import org.theBestCMISGroup.FileManagement.Utils.SVG;
import org.theBestCMISGroup.Roads.NonInitBusRoute;
import org.theBestCMISGroup.Roads.NonInitRoad;
import org.theBestCMISGroup.Roads.Road;
import org.theBestCMISGroup.Utils.MiscDataTypes.CoordinatePair;
import org.theBestCMISGroup.Utils.Storage.EasyList;
import org.theBestCMISGroup.Utils.Storage.GenericStorage;
import org.theBestCMISGroup.Utils.Storage.TreeStorage;
import org.theBestCMISGroup.Utils.Storage.Utils.Criteria;
import org.theBestCMISGroup.Vehicle.BusRoute;

class MapLoader {
    private final EasyList<Node> nodes;
    private final EasyList<Road> roads;
    private final EasyList<BusRoute> busRoutes;
    public MapLoader(OSMMapReader roadMapOSM) {
        this(roadMapOSM, new EasyList<>());
    }

    public MapLoader(SVG roadMapSVG) {
        throw new RuntimeException("this hasn't been implemented yet"); //todo

    }
    public MapLoader(EasyList<EasyList<String>> config) {
        this(config,new EasyList<>());
    }

    public MapLoader(OSMMapReader roadMapOSM, EasyList<EasyList<EasyList<String>>> busRouteConfig) {
        TreeStorage<OSMMapReader.OSMNode> osmNodesSortedByID = new TreeStorage<>(null, new Criteria.DerivedStringComparableCriteria<OSMMapReader.OSMNode>() {
            @Override
            public String deriveString(OSMMapReader.OSMNode object) {
                return object.getID()+"";
            }
        });
        osmNodesSortedByID.addAll(roadMapOSM.getNodes());
        TreeStorage<Node> allNodes = new TreeStorage<>(null, new Criteria.DerivedStringComparableCriteria<>() {
            @Override
            public String deriveString(Node n) {
                return n.getPos().toString();
            }
        });
        for (OSMMapReader.OSMNode node : osmNodesSortedByID) {
            allNodes.add(new Node(node.getPos()));
        }
        //now that we have the nodes sorted, go through the roads and link them.
        EasyList<OSMMapReader.OSMRoad> osmRoads = roadMapOSM.getRoads();
        roads = new EasyList<>();
        for (OSMMapReader.OSMRoad osmRoad : osmRoads) {
            //start at the start node and then go through each of them...
            OSMMapReader.OSMNode lastOSMNode = osmNodesSortedByID.getItemUsingDerivedStringSorting(osmRoad.getNodeIDs()[0] + "");
            Node lastNode = allNodes.getItemUsingDerivedStringSorting(lastOSMNode.getPos().toString());
            Long[] nodeIDs = osmRoad.getNodeIDs();
            for (int i = 1; i < nodeIDs.length; i++) {
                Long nodeID = nodeIDs[i];
                OSMMapReader.OSMNode thisOSMNode = osmNodesSortedByID.getItemUsingDerivedStringSorting(nodeID + "");
                Node thisNode = allNodes.getItemUsingDerivedStringSorting(thisOSMNode.getPos().toString());
                roads.add(new Road(osmRoad, lastNode, thisNode));

                lastOSMNode = thisOSMNode;
                lastNode = thisNode;
            }
            //and that's all roads hooked up,
        }
        NonInitBusRoute[] nonInitBusRoutes = parseToNonInitBusStops(busRouteConfig);
        // and done with that processing. all nodes are in the allNodes, and all roads are in roads. now to handle the buses. I'll just make them snap to the nearest node.
        EasyList<BusRoute> busRoutes = new EasyList<>();
        for (NonInitBusRoute nonInitBusRoute : nonInitBusRoutes) {
            EasyList<Node> stops = new EasyList<>();
            for (CoordinatePair stop : nonInitBusRoute.getStops()) {
                stops.add(getNearestNode(allNodes.toTypedArray(), stop));
            }
            busRoutes.add(new BusRoute(nonInitBusRoute, stops.toTypedArray()));
        }
        //roads is already converted.
        this.nodes=allNodes.toEasyList();
        this.busRoutes=busRoutes.clone();
    }
    public EasyList<Node> getNodes() {
        return nodes.clone();
    }
    public EasyList<Road> getRoads() {
        return roads.clone();
    }

    public EasyList<BusRoute> getBusRoutes() {
        return busRoutes;
    }
    public MapLoader(EasyList<EasyList<String>> config, EasyList<EasyList<EasyList<String>>> busRoutes) {
        //just run the parser and then run the nonInit one.
        this(parseToNonInitRoads(config),parseToNonInitBusStops(busRoutes));
    }

    private static NonInitBusRoute[] parseToNonInitBusStops(EasyList<EasyList<EasyList<String>>> busRoutes) {
        EasyList<NonInitBusRoute> ret = new EasyList<>();
        for (EasyList<EasyList<String>> busRoute : busRoutes) {
            ret.add(new NonInitBusRoute(busRoute));
        }
        return ret.toArray(new NonInitBusRoute[0]);
    }

    public MapLoader(NonInitRoad[] nonInitRoads, NonInitBusRoute[] nonInitBusRoutes) {
//        and I create a list to store nodes as I make them. a sortedStorage almost purely so that I can ensure no duplicates easily.
        TreeStorage<Node> allNodes = new TreeStorage<>(null, new Criteria.DerivedStringComparableCriteria<>() {
            @Override
            public String deriveString(Node n) {
                return n.getPos().toString();
            }
        });
//        and then I go through all the pre-roads, and I start generating the nodes,
        for (NonInitRoad nonInitRoad : nonInitRoads) {
//        ensuring no dupes made by another road already by trying to add and then using get from allNodes
            allNodes.add(new Node(nonInitRoad.roadPath.getStartPos()));
            Node startNode = allNodes.getItemUsingDerivedStringSorting(nonInitRoad.roadPath.getStartPos().toString());
//             for each intersecting Node I save it to a list, and then I'll sort that list closest to furthest,
            TreeStorage<Node> applicableNodes = new TreeStorage<>(null, (item1, item2) -> {
                if (item1 == item2) {
                    return 0;
                }
                if (item1 == null) {
                    return -1;
                }
                if (item2 == null) {
                    return 1;
                }
                return (int) ((
                        startNode.getPos().getDistanceTo(item2.getPos()) -
                                startNode.getPos().getDistanceTo(item1.getPos()))
                        * 1000);
            });
            if (nonInitRoad.intersectOnNonEnds) { // this if makes it skip stuff early. its O, but It's still important.
                for (NonInitRoad otherNonInitRoad : nonInitRoads) {
                    //end only roads only get end checks, but they still get end checks.
                    if (otherNonInitRoad.intersectOnNonEnds) {
                        if (nonInitRoad.roadPath.intersectsWithinBounds(otherNonInitRoad.roadPath)) {
                            CoordinatePair intersection = nonInitRoad.roadPath.getIntersectPointIgnoreBounds(otherNonInitRoad.roadPath);
                            allNodes.add(new Node(intersection));
                            applicableNodes.add(allNodes.getItemUsingDerivedStringSorting(intersection.toString()));
                        }
                    } else { // check and add either side if either side intersects
                        // if we don't think of the ends only roads now, then roads that were listed before them that
                        // should intersect with the ends won't intersect.
                        if (nonInitRoad.roadPath.isOnBoundedLine(otherNonInitRoad.roadPath.getStartPos())) {
                            CoordinatePair intersection = otherNonInitRoad.roadPath.getStartPos();
                            allNodes.add(new Node(intersection));
                            applicableNodes.add(allNodes.getItemUsingDerivedStringSorting(intersection.toString()));
                        } else if (nonInitRoad.roadPath.isOnBoundedLine(otherNonInitRoad.roadPath.getEndPos())) {
                            CoordinatePair intersection = otherNonInitRoad.roadPath.getEndPos();
                            allNodes.add(new Node(intersection));
                            applicableNodes.add(allNodes.getItemUsingDerivedStringSorting(intersection.toString()));
                        }
                    }
                }
            }
            // its sorted close to far from start, so start going along the line.
            // start at start and then ladder climb along till the end of the list or null.
            Node lastLinkedNode = startNode;
            for (Node nextNode : applicableNodes) {
                Road link = new Road(nonInitRoad, lastLinkedNode, nextNode);
                lastLinkedNode = nextNode;
            }
            //and at last, check and do the end point if it wasn't already done.
            allNodes.add(new Node(nonInitRoad.roadPath.getEndPos()));
            Node end = allNodes.getItemUsingDerivedStringSorting(nonInitRoad.roadPath.getEndPos().toString());
            if (lastLinkedNode != end) {
                Road link = new Road(nonInitRoad, lastLinkedNode, end);
            }
        }// and that road is hooked up. we do that for all the roads, and they look ahead and make intersections for future roads to link to as well.
//        and now lets retrieve all the roads. genericStorage will block duplicates, and a sort ~~will~~ *won't make the sorting faster when the map has to load and sort it.
        GenericStorage<Road> roads = new GenericStorage<>();
        for (Node node : allNodes) {
            roads.addAll(node.getConnectedRoads());
        }
        // and done with that processing. all nodes are in the allNodes, and all roads are in roads. now to handle the buses. I'll just make them snap to the nearest node.
        EasyList<BusRoute> busRoutes = new EasyList<>();
        for (NonInitBusRoute nonInitBusRoute : nonInitBusRoutes) {
            EasyList<Node> stops = new EasyList<>();
            for (CoordinatePair stop : nonInitBusRoute.getStops()) {
                stops.add(getNearestNode(allNodes.toArray(new Node[0]), stop));
            }
            busRoutes.add(new BusRoute(nonInitBusRoute, stops.toArray(new Node[0])));
        }
        this.nodes=allNodes.toEasyList();
        this.roads=roads.toEasyList();
        this.busRoutes=busRoutes.clone();
    }
    private static NonInitRoad[] parseToNonInitRoads(EasyList<EasyList<String>> config) {
        EasyList<NonInitRoad> nonInitRoads = new EasyList<>();
        for (EasyList<String> roadConfig: config) {
            nonInitRoads.add(new NonInitRoad(roadConfig));
        }
        return nonInitRoads.toArray(new NonInitRoad[0]);
    }
    private static Node getNearestNode(Node[] nodes, CoordinatePair coordinates) {
        Node nearest = null;
        double nearestDistance = Double.MAX_VALUE;
        for (Node node : nodes) {
            if (coordinates.getDistanceTo(node.getPos())< nearestDistance) {
                nearestDistance=coordinates.getDistanceTo(node.getPos());
                nearest=node;
            }
        }
        return nearest;
    }

}
