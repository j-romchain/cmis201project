package org.theBestCMISGroup.Mapping;

import org.theBestCMISGroup.FileManagement.ConfigManagement.ConfigLoader;
import org.theBestCMISGroup.Pathing.PathFinding;
import org.theBestCMISGroup.Roads.NonInitBusRoute;
import org.theBestCMISGroup.Roads.NonInitRoad;
import org.theBestCMISGroup.Roads.Road;
import org.theBestCMISGroup.Utils.MiscDataTypes.CoordinatePair;
import org.theBestCMISGroup.Utils.Storage.EasyList;
import org.theBestCMISGroup.Utils.Storage.GenericStorage;
import org.theBestCMISGroup.Utils.Storage.TreeStorage;
import org.theBestCMISGroup.Utils.Storage.Utils.Criteria;
import org.theBestCMISGroup.Utils.Storage.Utils.TransversalCode;
import org.theBestCMISGroup.Vehicle.BusRoute;
import org.theBestCMISGroup.Vehicle.BusStop;

public class Map {
    public static Map loadMapWhateverConfigIsAvailable() {
        return switch (ConfigLoader.getRoadMapFormat()) {
            default -> loadMapFromCSVConfig();
            case SVG -> loadMapFromSVGConfig();
            case OSM -> loadMapFromOSMConfig();
        };
    }
    public static Map loadMapFromCSVConfig() {
        return new Map(new MapLoader(ConfigLoader.getRoadMapCSV(),ConfigLoader.getBusRoutes()));
    }
    public static Map loadMapFromSVGConfig() {
        return new Map(new MapLoader(ConfigLoader.getRoadMapSVG()));
    }
    public static Map loadMapFromOSMConfig() {
        return new Map(new MapLoader(ConfigLoader.getRoadMapOSM()));
    }
    public Map(NonInitRoad[] nonInitRoads, NonInitBusRoute[] nonInitBusRoutes) {
        this(new MapLoader(nonInitRoads,nonInitBusRoutes));
    }
    private Map(MapLoader ml) {
        this(ml.getNodes().toArray(new Node[0]),ml.getRoads().toArray(new Road[0]));
        for (BusRoute busRoute : ml.getBusRoutes()) {
            busRoute.finishInit(this);
        }
    }
    private Map(Node[] Nodes, Road[] roads) {
        this.nodes.addAll(Nodes);
        this.roads.addAll(roads);
        //        try {
//            this.trafficLanesByEnd.addItems(pathsAndRoads);
//        } catch (Errors.AdditionCriteriaException e) {
//            throw new RuntimeException(e);
//        }
//        try {
//            this.trafficLanesByStart.addItems(pathsAndRoads);
//        } catch (Errors.AdditionCriteriaException e) {
//            throw new RuntimeException(e);
//        }
    }
    final GenericStorage<Road> roads = new GenericStorage<>();

    final TreeStorage<Node> nodes = new TreeStorage<>(null, new Criteria.DerivedStringComparableCriteria<>() {
        @Override
        public String deriveString(Node knowed) {
            return knowed.getPos()+"";
        }
    });
//    TreeStorage<TrafficLane> trafficLanesByStart = new TreeStorage<>(null, new Criteria.DerivedStringComparableCriteria<>() {
//        @Override
//        public String deriveString(TrafficLane object) {
//            return object.getStartPos() + "";
//        }
//    });
//    TreeStorage<TrafficLane> trafficLanesByEnd = new TreeStorage<>(null, new Criteria.DerivedStringComparableCriteria<>() {
//        @Override
//        public String deriveString(TrafficLane object) {
//            return object.getEndPos() + "";
//        }
//    });
//    final TreeStorage<BusStop> busStops = new GenericStorage<>();
    public Node getNode(CoordinatePair pos) {
        return nodes.getItemUsingDerivedStringSorting(pos.toString());
    }
    public BusStop getBusStop(CoordinatePair pos) {
        return nodes.getItem(item -> item.getPos()==pos).getBusStop();
    }
    public Node getNearestNode(CoordinatePair pos) {
        final Node[] nearest = {null};
        final double[] smallestDistance = {Double.MAX_VALUE};
        nodes.inOrderTransverse(new TransversalCode<>() {
            @Override
            public Node doForItem(Node item, int itemNum) {
                if (pos.getDistanceTo(item.getPos())< smallestDistance[0]) {
                    smallestDistance[0] =pos.getDistanceTo(item.getPos());
                    nearest[0] =item;
                }
                return item;
            }
        });
        return nearest[0];
    }
    public BusStop getNearestBusStop(CoordinatePair pos) {
        final BusStop[] nearest = {null};
        final double[] smallestDistance = {Double.MAX_VALUE};
        nodes.inOrderTransverse(new TransversalCode<>() {
            @Override
            public Node doForItem(Node item, int itemNum) {
                if (pos.getDistanceTo(item.getPos())< smallestDistance[0] && item.getBusStop()!=null) {
                    smallestDistance[0] =pos.getDistanceTo(item.getPos());
                    nearest[0] =item.getBusStop();
                }
                return item;
            }
        });
        return nearest[0];
    }
    public EasyList<Node> getNodes() {
        return nodes.toEasyList();
    }
    public GenericStorage<Road> getRoads() {
        return roads;
    }

    public void tickAllRoads() {
        for (Road road : getRoads()) {
            road.tick();
        }
    }
    private PathFinding pathFinder;
    public PathFinding getPathing() {
        if (pathFinder==null) {
            this.pathFinder=new PathFinding(nodes.toEasyList());
        }
        return pathFinder;
    }
}
