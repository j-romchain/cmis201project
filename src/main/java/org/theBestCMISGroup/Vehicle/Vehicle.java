package org.theBestCMISGroup.Vehicle;

import org.theBestCMISGroup.Mapping.Node;

import java.util.Date;

public interface Vehicle {
    void dropAt(Node endNode, Date currentTime);//run by roads when the vehicles reach a node/intersection
    //added the Date currentTime, please don't remove - Gabriel
}
