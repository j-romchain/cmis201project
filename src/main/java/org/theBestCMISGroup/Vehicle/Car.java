package org.theBestCMISGroup.Vehicle;

import org.theBestCMISGroup.Mapping.Node;
import org.theBestCMISGroup.Mapping.TrafficLane;
import org.theBestCMISGroup.Pathing.Route;
import org.theBestCMISGroup.People.Person;
import org.theBestCMISGroup.Utils.MiscDataTypes.CoordinatePair;
import org.theBestCMISGroup.Utils.Storage.Utils.ItemsWithUUIDs;

import java.util.Date;

public class Car extends ItemsWithUUIDs implements Vehicle{
    //not sure how we plan to refer to vehicles, just did uuid
    private Person passenger;
    private Route carRoute;
//    private RoadTime roadTimer;
//    private Node endNode;

    public Car(Person pass, Route route) {
        passenger = pass;
        carRoute = route;
//        roadTimer = new RoadTime();
//        this.endNode = endNode;
    }

    //not sure what else is needed at the moment
    public void setCarRoute(Route newRoute) {
        carRoute = newRoute;
    }
//    public long getTimeOnRoad(Date currentTime){
//        return currentTime.getTime() - roadTimer.getTimeSpentOnRoad();}
//    public void resetRoadTimer(Date currentTime){roadTimer.startTimer(currentTime);}
    CoordinatePair nextLocation;
    public CoordinatePair getNextRouteLocation(){
        nextLocation = carRoute.carRouteRetrevial();
        return nextLocation;
    }

    @Override
    public void dropAt(Node node, Date currentTime) {
        if(node.equals(passenger.getTargetNode())){
            passenger.finishJourney();
        } else{
        TrafficLane next = node.getFastestDirectWayOutTo(getNextRouteLocation());
        if (next!=null) {
            next.travel(this, currentTime);
        } else {
            System.out.println("error at car dropAt");
            //todo handle if the next node is not just one trafficLane away
        }}
    }
    //this class is to allow for Vehicles to follow the time to traverse any given road
    //the timer will be reset by the roads upon entry to a new road
//    class RoadTime{
//        //this variable will take a snapshot of the current time when the Vehicle entered the road
//        private long timeSpentOnRoad;
//        public RoadTime(){
//            timeSpentOnRoad = 0;
//        }
//
//        public void startTimer(Date currentTime){
//            timeSpentOnRoad = currentTime.getTime();
//        }
//        public long getTimeSpentOnRoad(){ return timeSpentOnRoad;}
//    }
}