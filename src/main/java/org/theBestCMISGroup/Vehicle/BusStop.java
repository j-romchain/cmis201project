package org.theBestCMISGroup.Vehicle;

import org.theBestCMISGroup.Mapping.Node;
import org.theBestCMISGroup.Pathing.PathMapCore.Queue;
import org.theBestCMISGroup.People.Person;
import org.theBestCMISGroup.Utils.MiscDataTypes.CoordinatePair;
import org.theBestCMISGroup.Utils.Storage.EasyList;

public class BusStop {
    private final Queue peopleInLine;
    private final Node myLocation;
    private final EasyList<BusRoute> involvedRoutes;

    public BusStop(Node location) {
        involvedRoutes = new EasyList<>();
        peopleInLine = new Queue();
        myLocation=location;
    }
    public void enqueue(Person person){
        peopleInLine.enqueue(person);
    }
    public int getLineLength(){
        return peopleInLine.queueLength();
    }
    public Person nextPersonInLine(){
        return peopleInLine.personDequeue();
    }
    public CoordinatePair getPos(){
        return myLocation.getPos();
    }
    public boolean arePeopleHere(){
        return peopleInLine.isQueueEmpty();
    }

    public void addInvolvedBusRoute(BusRoute busRoute) {
        involvedRoutes.add(busRoute);
    }
    public EasyList<BusRoute> getInvolvedRoutes() {
        return involvedRoutes;
    }
    //todo
}
