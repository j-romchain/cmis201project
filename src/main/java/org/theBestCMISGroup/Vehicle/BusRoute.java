package org.theBestCMISGroup.Vehicle;

import org.theBestCMISGroup.Mapping.Map;
import org.theBestCMISGroup.Mapping.Node;
import org.theBestCMISGroup.Roads.NonInitBusRoute;
import org.theBestCMISGroup.Utils.MiscDataTypes.CoordinatePair;
import org.theBestCMISGroup.Utils.Storage.EasyList;


public class BusRoute {
    EasyList<BusStop> stops = new EasyList<>();
    Bus busThatTakesThisRoute;
    Map map;
    public BusRoute(NonInitBusRoute nonInitBusRoute, Node[] stopLocations) {
        for (int i = 0; i < stopLocations.length-1; i++) {
            Node stopLocation = stopLocations[i];
//            System.out.println("nextLocation"+ Arrays.toString(stopLocations)+i);

            Node nextLocation = (i==0)?stopLocations[stopLocations.length-1]:stopLocations[i+1];
            {
                if (stopLocation.getBusStop()==null) {
                    stopLocation.initBusStop();
                }
                stops.add(stopLocation.getBusStop());
                stopLocation.getBusStop().addInvolvedBusRoute(this);
            }
        }
        this.stopLocations=stopLocations;
    }
    private final Node[] stopLocations;
    public void finishInit(Map map) {
        this.map=map;
        busThatTakesThisRoute=new Bus(map.getPathing().CreateBusRoute(stopLocations,stopLocations[0]));
    }
    public BusStop getClosestConnectedBusStop(CoordinatePair cords){
        BusStop closest = null;
        double smallestDistance = Double.MAX_VALUE;
        for (BusStop stop : stops) {
            double dist = cords.getDistanceTo(stop.getPos());
            if (dist<smallestDistance) {
                closest=stop;
                smallestDistance=dist;
            }
        }
        return closest;
    }
}
