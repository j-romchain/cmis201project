package org.theBestCMISGroup.Vehicle;
//temp classes i made so i can test


import org.theBestCMISGroup.Mapping.Node;
import org.theBestCMISGroup.Mapping.TrafficLane;
import org.theBestCMISGroup.Pathing.Route;
import org.theBestCMISGroup.People.Person;
import org.theBestCMISGroup.Utils.MiscDataTypes.CoordinatePair;
import org.theBestCMISGroup.Utils.Storage.Utils.ItemsWithUUIDs;

import java.util.Date;
import java.util.UUID;

public class Bus extends ItemsWithUUIDs implements Vehicle {
    private UUID busId;
    public static int maxBusCapacity=50;
    //array capacity set at maxBusCapacity for avg seat capacity
    private final Person[] passengers;
    //temporary, probably will need a system for all passengers, not just bus ones
    private Route busRoute;
    private int passengerCount;
//    private RoadTime roadTimer;

    //bus constructor, right now creates uuid to identify, passenger# array, input route to first stop?
    public Bus(Route route1){
        passengers = new Person[maxBusCapacity];
        busRoute = route1;
        passengerCount = 0;
//        roadTimer = new RoadTime();
    }

    public int getNullPassengerCount(int ct){
        int count = 0;
        for(int i = 0; i < ct; i++){
            if((passengers[i] == null)){
                count++;
            }
        } return count;
    }

    //maybe needed

    //to set the next route for the bus,
    public void setBusRoute(Route newRoute){
        busRoute = newRoute;
    }

    public void printBusPassengers(){
        for(Person i : passengers){
            System.out.print(i);
        }}


    //need to discuss if person object is holding route
    public void offloadPassengers(BusStop stop, Date currentTime){
        for(int i = 0; i < passengerCount; i++){
            if(passengers[i].getTargetNode().getPos().equals(stop.getPos())) {
                passengers[i].completeTimeStamp(currentTime, "bus : " + busId);
                passengers[i].finishJourney();
            }
        }
        updatePassengers();
    }

    //onload method: check capacity and then onload from bus stop waiting line, assumes array for time being but maybe queue?
    //might need to take a route object too, not sure where thats being handled rn
    public void onloadPassengers(BusStop wait, Date currentTime){
        int capacity = maxBusCapacity-passengerCount;
        if(capacity <= 0){
            //If statement is here to catch the bus when it is already full
            if(capacity < 0){
                System.out.println("There is an error with the passengerCount system");
            }
        }
        else{
        if(wait.arePeopleHere()){
            //If statement to catch for if no one is at the busStop
        }//no passengers on bus, and less than maxBusCapacity passengers at stop, onloads every person from stop
        else if(wait.getLineLength()<=capacity){
            for(int i = passengerCount ; i < passengerCount+wait.getLineLength(); i++){
                passengers[i] = wait.nextPersonInLine();
                passengers[i].addTimeStamp(currentTime, "bus : " + busId);
                passengerCount++;
            }}//loads on all people at the bus stop if the line at the bus stop is less or equal to the space left on bus
        else{
            for(int i = passengerCount; i < maxBusCapacity; i++){
                passengers[i] = wait.nextPersonInLine();
                passengers[i].addTimeStamp(currentTime, "bus : " + busId);
                passengerCount++;
            }}//less than maxBusCapacity people on bus, and enough people at stop to reach capacity, onloads only to maxBusCapacity
        }
    }
    public void updatePassengers(){
        Person[] temp = new Person[maxBusCapacity];
        int tempIndex = 0;
        //runs for the passenger count + null passenger count, so if it is 1 null 2 null 3 4 5 it runs for 7 instead of only 5
        //adds every nonnull element to a new array, without the gaps
        for(int i = 0; i < passengerCount + getNullPassengerCount(passengerCount); i++){
            if(passengers[i] != null){
                temp[tempIndex] = passengers[i];
                tempIndex++;
            }}
        //set every element of the passengers array to null, so that they can be replaced in next loop
        for(int i = 0; i < maxBusCapacity; i++){
            passengers[i] = null;
        }
        //making passengers array correct
        for(int i = 0; i < tempIndex+1; i++){
            passengers[i] = temp[i];
        }
    }
//    public long getRoadTimer(){return roadTimer.getTimeSpentOnRoad();}
//    public void resetRoadTimer(Date currentTime){roadTimer.startTimer(currentTime);}
    public CoordinatePair getNextStop() {
        return busRoute.busRouteRetrevial();
    }

    @Override
    public void dropAt(Node node, Date currentTime) {
        CoordinatePair tempCords = getNextStop();
        if (node.getPos().equals(tempCords)) {
            offloadPassengers(node.getBusStop(), currentTime);
            onloadPassengers(node.getBusStop(), currentTime);

            //do stuff when you arrive at a stop
        }
        //do stuff to navigate to next stop
        TrafficLane next = node.getFastestDirectWayOutTo(getNextStop());
        if (next!=null) {
            next.travel(this, currentTime);
        } else {
            System.out.println("error at bus dropAt");
            //todo handle if the next node is not just one trafficLane away
        }
    }
//    class RoadTime{
//        //this variable will take a snapshot of the current time when the Vehicle entered the road
//        private long timeSpentOnRoad;
//        public RoadTime(){
//            timeSpentOnRoad = 0;
//        }
//
//        public void startTimer(Date currentTime){
//            timeSpentOnRoad = currentTime.getTime();
//        }
//        public long getTimeSpentOnRoad(){ return timeSpentOnRoad;}
//    }
}

