package org.theBestCMISGroup;


import org.theBestCMISGroup.FileManagement.ConfigManagement.ConfigLoader;
import org.theBestCMISGroup.Utils.MiscDataTypes.Distribution;
import org.theBestCMISGroup.Utils.Simulation.Simulation;

import java.text.ParseException;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Traffic simulatable = new Traffic();
        long simulationLength = 500000; //This number represents minutes
        double timeIncrement = 1.5;//this number represents minutes
        Distribution.RandomDistribution spawnRate;
        try {
            spawnRate = Distribution.RandomDistribution.parse(ConfigLoader.getConfig().get(4));
        } catch (ParseException e) {
            throw new RuntimeException("the config for the spawn rate was bad.");
        }
        Simulation<Traffic> sim = new Simulation<Traffic>(new Date(0), (long) (timeIncrement*1000*60), spawnRate, simulatable,simulationLength*1000*60);
        // and sim will run using the methods in Traffic/ simulatable, incrementing the time and organizing all that stuff for you.
    }
}