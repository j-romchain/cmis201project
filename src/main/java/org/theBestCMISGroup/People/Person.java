package org.theBestCMISGroup.People;


import org.theBestCMISGroup.Mapping.Map;
import org.theBestCMISGroup.Mapping.Node;
import org.theBestCMISGroup.Statistics.DataCollection.DataCollector;
import org.theBestCMISGroup.Utils.Storage.Utils.ItemsWithUUIDs;
import org.theBestCMISGroup.Vehicle.BusStop;

import java.util.Date;

public class Person extends ItemsWithUUIDs {
    private final Node spawnNode;
    private final Node targetNode;
    private final double trafficHate;
    private final double claustrophobiaModifier;
    private TimeTracker[] timeTracker;
    Map map;
    DataCollector data;

    public Person(Node spawnNode, Node targetNode, Map map,DataCollector data){
        this.spawnNode = spawnNode;
        this.targetNode = targetNode;
        trafficHate = Math.random();
        claustrophobiaModifier = Math.random();
        timeTracker = new TimeTracker[2];
        this.data=data;
        this.map=map;
    }

    public Node getTargetNode() {return targetNode;}
    public Node getSpawnNode(){return spawnNode;}

    public boolean DoITakeTheBus(){
        BusStop endPointBusStop = map.getNearestBusStop(targetNode.getPos());
        BusStop startPointBusStop = map.getNearestBusStop(spawnNode.getPos());
        if ((targetNode.getPos().getDistanceTo(endPointBusStop.getPos()) <= .015) &&
                (spawnNode.getPos().getDistanceTo(startPointBusStop.getPos()) <= 0.15)) {
            if (DecisionAlgorithm(endPointBusStop, startPointBusStop)){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    //this method is used to decide if a person wants to ride the bus or not
    //If they want to ride the bus the method outputs true
    private Boolean DecisionAlgorithm(BusStop endBusStop, BusStop startBusStop){

        //Equation value = reasons for bus - reasons for car
        double value = ((1 - (endBusStop.getLineLength()/25)) *
        (1 - (spawnNode.getPos().getDistanceTo(startBusStop.getPos())/.13))) - ((1- trafficHate) * (claustrophobiaModifier));
        if(value > 0){
            return true;
        }
        else
            return false;
    }
    public void addTimeStamp(Date currentTime, String locationOfWait){ //adds a new timeTracker object to array list
        int TimeStampIndex = 0;
        int lengthOfArray = timeTracker.length - 1;
        if(timeTracker[lengthOfArray] != null){
            timeTracker = arrayExpansion(timeTracker);    //should be occasionally used, but necessary
        }
        while(timeTracker[TimeStampIndex] != null){
            TimeStampIndex++;
        }
        timeTracker[TimeStampIndex] = new TimeTracker(currentTime, locationOfWait);
    }
    public void completeTimeStamp(Date currentTime, String locationOfWait){  //invoked when a job moves between queues/services
        int timeStampIndex = 0;
        while(timeTracker[timeStampIndex] != null){
            if(timeTracker[timeStampIndex].getLocationOfWait() == locationOfWait){
                break;}
            else
                timeStampIndex++;
        }
        timeTracker[timeStampIndex].setEndTime(currentTime);
    }

    public TimeTracker[] timeStampDump(){ //This method is to trim any extra space from timeTracker before passing forward
        int timeStampIndex = 0;
        while(timeTracker[timeStampIndex] != null){
            timeStampIndex++;
        }
        TimeTracker[] tempArray = new TimeTracker[timeStampIndex];
        for(int i = 0; i < tempArray.length; i++){
            tempArray[i] = timeTracker[i];
        }
        return tempArray;
    }
    private TimeTracker[] arrayExpansion(TimeTracker[] oldArray){  //expands any timeTracker arrays that need more room
        TimeTracker[] newArray = new TimeTracker[oldArray.length + 10];
        for(int i = 0; i < oldArray.length; i++){
            newArray[i] = oldArray[i];
        }
        return newArray;
    }

    public void finishJourney() {
        data.loadDataOfCompletedPerson(this);
    }
}
