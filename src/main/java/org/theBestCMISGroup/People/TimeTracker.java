package org.theBestCMISGroup.People;


import java.util.Date;

public class TimeTracker {
    //This class emulates the way people track the amount of time they spend within each section of a queueing system
    //This will include any part of the system where people are required to wait, so service and queue times are included
    //Each TimeTracker will be differentiated through the name given to each time stamp
    //The format will be queueWait:locationOfQueue, ServiceWait:TypeOfService. for queue waits and service waits respectively
    //The TimeStamps will be connected like a linked list allowing for future flexibility when job routing becomes more complicated
    private Date startTime, endTime;
    //private double totalWaitTime;

    private String locationOfWait;
    private double timeElapsed;

    public TimeTracker(Date startTime, String locationOfWait){
        this.startTime = startTime;
        endTime = new Date(0);
        this.locationOfWait = locationOfWait;
        timeElapsed = Double.MAX_VALUE;
    }
    public void setEndTime(Date endTime){
        this.endTime = endTime;
        timeElapsed = endTime.getTime() - startTime.getTime();
    }

    public double getTimeElapsed() {
        return timeElapsed;
    }

    public String getLocationOfWait(){
        return locationOfWait;
    }

}