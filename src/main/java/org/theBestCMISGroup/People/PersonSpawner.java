package org.theBestCMISGroup.People;

import org.theBestCMISGroup.Mapping.Map;
import org.theBestCMISGroup.Mapping.Node;
import org.theBestCMISGroup.Statistics.DataCollection.DataCollector;
import org.theBestCMISGroup.Utils.MiscDataTypes.CoordinatePair;

import java.util.Random;

public class PersonSpawner {
    Map map;
    DataCollector data;
    // this method could be responsible for spawning in the people and/or creating teh busses/cars where they need to be
    Random ran = new Random();
    //boundaries of map are Longitude:-77.5 to -77.35  latitude:39.35 to 39.5
    public PersonSpawner(Map map,DataCollector data) {
        this.map=map;
        this.data=data;
    }
    public void spawn(){
        Person person = generatePerson();
        if(person.DoITakeTheBus()){
            map.getNearestBusStop(person.getSpawnNode().getPos()).enqueue(person);
        }else{
            //TODO make call to spawn person as car at start Node
        }
    }

    public Person generatePerson(){
        CoordinatePair startNodeCords = new CoordinatePair(ran.nextDouble(-77.5,-77.35), ran.nextDouble(39.35,39.5));
        CoordinatePair endNodeCords = new CoordinatePair(ran.nextDouble(-77.5,-77.35), ran.nextDouble(39.35,39.5));
        Node startNode = map.getNearestNode(startNodeCords);
        Node endNode = map.getNearestNode(endNodeCords);
        return new Person(startNode, endNode,map,data);
    }

}
