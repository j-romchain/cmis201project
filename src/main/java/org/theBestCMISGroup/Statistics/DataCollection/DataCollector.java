package org.theBestCMISGroup.Statistics.DataCollection;

import org.theBestCMISGroup.People.Person;
import org.theBestCMISGroup.People.TimeTracker;

public class DataCollector {
    //TODO will be completed towards the end when we start to build the simulation loop, Hopefully later today
    private DataBucket[] buckets;
    private int bucketsLength = 20;

    public DataCollector(){
        buckets = new DataBucket[bucketsLength];
    }

    public void calculate(){
        int index = 0;
        while(buckets[index] != null) {
            double standardDeviation = standardDeviationCalculator(buckets[index]);
            System.out.println("Mean Wait time for : "+ buckets[index].getLocationName() + " is : " + buckets[index].getMeanWaitTime() +
                    " and the Standard Deviation is : "+ standardDeviation);
            index++;
        }
    }

    public void loadDataOfCompletedPerson(Person person){


        if(buckets[bucketsLength - 1] != null){
            buckets = arrayExpansion(buckets);
        }
        for(TimeTracker timeTracker : person.timeStampDump()){
            if(nameVerification(timeTracker.getLocationOfWait(), buckets)){
                int bucketIndex = 0;
                while(buckets[bucketIndex].getLocationName() != timeTracker.getLocationOfWait()){
                    bucketIndex++;
                }
                buckets[bucketIndex].add(timeTracker);
            }
            else{
                int index = 0;
                while(buckets[index] != null){ index ++;}
                buckets[index] = new DataBucket(200, timeTracker.getLocationOfWait());
                buckets[index].add(timeTracker);
            }
        }
    }


    private double standardDeviationCalculator(DataBucket timeStampBucket){
        double runningTotal = 0.0;
        double meanWaitTime = timeStampBucket.getMeanWaitTime();
        int index = 0;
        TimeTracker[] timeTrackers = timeStampBucket.getTimeStampList();
        while(timeTrackers[index] != null){
            runningTotal += Math.pow((timeTrackers[index].getTimeElapsed() - meanWaitTime),2);
            index++;
            if(index >= timeTrackers.length)
                break;
        }
        return Math.pow(runningTotal/timeStampBucket.getNumberOfJobs(),.5);
    }

    private boolean nameVerification(String timeStampName, DataBucket[] nameList){ //checks to see if name is on list
        int index = 0;
        while(nameList[index] != null){
            if(timeStampName == nameList[index].getLocationName())
                return true;
            index++;
        }
        return false;
    }

    private DataBucket[] arrayExpansion(DataBucket[] oldArray){
        DataBucket[] newArray = new DataBucket[oldArray.length + 10];
        for(int i = 0; i < oldArray.length; i++){
            newArray[i] = oldArray[i];
        }
        return newArray;
    }

    class DataBucket{
        private double totalWaitTime; //used for average wait time
        private int numberOfPeople; //used for both
        private int arrayIndex;
        private TimeTracker[] timeTrackerList; //used for calculating standard deviations
        private String locationName; //used to distinguish each StatObject from each other

        protected DataBucket(int timeStampsSize, String locationName){
            totalWaitTime = 0;
            numberOfPeople = 0;
            arrayIndex = 0;
            timeTrackerList = new TimeTracker[timeStampsSize];
            this.locationName = locationName;
        }
        protected String getLocationName(){
            return locationName;
        }

        protected void add(TimeTracker timeTracker){
            if(timeTrackerList[timeTrackerList.length - 1] != null){
                timeTrackerList = arrayExpansion(timeTrackerList);
            }
            totalWaitTime += (timeTracker.getTimeElapsed())/1000; //converts the milliseconds into seconds
            numberOfPeople += 1;
            timeTrackerList[arrayIndex] = timeTracker;
            arrayIndex += 1;
        }
        protected TimeTracker[] getTimeStampList(){
            return timeTrackerList;
        }
        protected double getMeanWaitTime(){
            return totalWaitTime/numberOfPeople;
        }
        protected int getNumberOfJobs(){
            return numberOfPeople;
        }
        private TimeTracker[] arrayExpansion(TimeTracker[] oldArray){
            TimeTracker[] newArray = new TimeTracker[oldArray.length + 200];
            for(int i = 0; i < oldArray.length; i++){
                newArray[i] = oldArray[i];
            }
            return newArray;
        }
    }
}
