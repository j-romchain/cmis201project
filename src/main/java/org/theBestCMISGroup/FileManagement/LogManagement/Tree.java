package org.theBestCMISGroup.FileManagement.LogManagement;

import org.theBestCMISGroup.Utils.Storage.EasyList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.text.ParseException;
import java.util.Date;
import java.util.Scanner;

public class Tree {
    //this represents a single log file
    private EasyList<Branch> branches;
    private final int treeNum;
    private final Grove myGrove;
    private File myFile;
    private String contents;
    public Tree(File logFile) throws FileNotFoundException, ParseException {
        this(new Grove(logFile), logFile);
    }

    public Tree(Grove grove, File logFile) throws FileNotFoundException {
        this(grove, logFile.getName());
    }

    public Tree(Grove grove, String fileID) throws FileNotFoundException {
        this(grove, parseTreeNum(fileID));
    }

    public static int parseTreeNum(String fileID) {
        return Integer.parseInt(fileID.substring(fileID.indexOf('#')+1,fileID.indexOf('.')));
    }

    public Tree(Grove grove, int treeNum) throws FileNotFoundException {
        this.myGrove = grove;
        this.treeNum = treeNum;
        init();
    }
    private void init() throws FileNotFoundException {
        this.myFile=new File(getPath());
        if (!myFile.exists()) {
            throw new FileNotFoundException("A log file must be made before you can referece it like this. Try running Tree.plantTree() or Logger.createLog() if you want to create one. The path would be \""+getPath()+"\"");
        }
        if (!myFile.isFile()) {
            throw new FileNotFoundException("There is a folder where a file should be. Please remove the folder and try creating the file again by running Tree.plantTree() or Logger.createLog(). The path should be \""+getPath()+"\"");
        }
    }
    public static String getPath(String forestName,Date growthDate,int treeNum) {
        return Grove.getPathPart(forestName,growthDate) + "-#" +treeNum+".log.txt";
    }
    public String getPath() {
        if (myFile==null) {
            return getPath(getGrove().getForest().getForestName(),getGrove().getGrowthDate(),treeNum);
        }
        return myFile.getAbsolutePath();
    }

    public Grove getGrove() {
        return myGrove;
    }
    public static Tree plantTree(Grove grove, int treeNum) throws IOException {
        File tree =new File(getPath(grove.getForest().getForestName(),grove.getGrowthDate(),treeNum));
        if (tree.exists()&&!tree.isFile()) {
            throw new FileAlreadyExistsException("Cannot create the log file because a directory is in the way. The path would be \""+ tree.getAbsolutePath()+"\"");
        }
        if (tree.exists()|| tree.createNewFile()) {
            return new Tree(grove,treeNum);
        } else {
            throw new FileNotFoundException("The log file could not be created. Most likely because of a directory not existing somewhere where a directory should be or a relevant file changing while the code was running. Ensure you can create this directory: \""+ tree.getAbsolutePath()+"\"");
        }
    }
    public static Tree createTree(String forestName, Date growthDate, int treeNum) throws IOException {
        return plantTree(Grove.createGrove(forestName,growthDate), treeNum);
    }

    public void update() {
        contents = "";
        branches = new EasyList<>();
        Scanner fileReader;
        try {
            fileReader = new Scanner(myFile);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("the log file did exist, but now it doesn't. filename:\""+myFile.getAbsolutePath()+"\"");
        }
        if (fileReader.hasNextLine()) {
            contents+=fileReader.nextLine();
        }
        while (fileReader.hasNextLine()) {
            //noinspection StringConcatenationInLoop
            contents+="\n"+fileReader.nextLine();
        }
        for (String log:contents.split("\n")){
            if (!branches.contains(abranch -> abranch.isPart(myFile,log))) {
                try {
                    branches.add(new Branch(this,log.split(":")[0]));
                } catch (ParseException ignored) {}
            }
        }

    }

    public boolean is(File treeBranchOrLogFile) {
        return treeBranchOrLogFile.getAbsolutePath().equals(getFile().getAbsolutePath());
    }

    public int getTreeNum() {
        return treeNum;
    }

    public File getFile() {
        return myFile;
    }

    public EasyList<Branch> getBranches() {
        update();
        return branches.clone();
    }

    public String getContents() {
        update();
        return contents;
    }

    public Branch getBranch(Date growthDate) {
        return new Branch(this, growthDate);
    }
}
