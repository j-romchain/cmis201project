package org.theBestCMISGroup.FileManagement.LogManagement;

import javax.naming.NameNotFoundException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

public class Log {
    //this represents a single line in a log file
    private final Branch myBranch;
    private final int logNum;
    private String logContent;

    public Log(File logFile, String logID) throws ParseException, FileNotFoundException, NameNotFoundException {
        this(new Branch(logFile,logID), logID);
    }

    public Log(File logFile, Date growthDate, int logNum) throws ParseException, FileNotFoundException, NameNotFoundException {
        this(new Branch(logFile,growthDate),logNum);
    }


    public Log(Branch branch, String logID) throws NameNotFoundException {
        this(branch, parseLogNum(logID));
    }

    public static int parseLogNum(String logID) {
        return Integer.parseInt(logID.substring(logID.indexOf('#')+1,logID.indexOf(']')));
    }

    public Log(Branch branch, int logNum) throws NameNotFoundException {
        this.myBranch = branch;
        this.logNum = logNum;
        init();
    }

    private void init() throws NameNotFoundException {
        this.logContent = null;
        String branchLines = myBranch.getContents();
        for (String line : branchLines.split("\n")) {
            if (is(myBranch.getTree().getFile(), getLogID())) {
                logContent = line.substring(line.indexOf(":"),line.length()-1);
            }
        }
        if (logContent==null) {
            throw new NameNotFoundException("A log line could not be found with the file path:\""+getPath()+"\" and log ID:\""+getLogID()+"\"");
        }
    }
    public static String getPath(String forestName, Date growthDate, int treeNum) {
        return Branch.getPath(forestName,growthDate,treeNum);
    }
    public String getPath() {
        return getBranch().getPath();
    }

    public static String getLogID(Date growthDate, int logNum) {
        return Branch.getLogIDPart(growthDate)+" #"+logNum+"]";
    }

    public String getLogID() {
        return getLogID(getBranch().getGrowthDate(),logNum);
    }

    public Branch getBranch() {
        return myBranch;
    }

    public static Log createLog(String forestName, Date treeGrowthDate, int treeNum, Date branchGrowthDate, int logNum) throws IOException, NameNotFoundException {
        return new Log(Branch.createBranch(forestName, treeGrowthDate, treeNum,branchGrowthDate), logNum);
    }
    public boolean is(File logFile, String logLine) {
        return myBranch.isPart(logFile,logLine) && logLine.contains(getLogID());
    }

    public int getLogNum() {
        return logNum;
    }

    public String getLogContent() {
        return logContent;
    }
}
