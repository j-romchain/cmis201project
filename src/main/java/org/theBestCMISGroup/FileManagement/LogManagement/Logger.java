package org.theBestCMISGroup.FileManagement.LogManagement;

public class Logger {
    // might as well be humorous, this is all about wood, the log is part of the branch which is part of the tree which is part of the grove which is part of the forest.
    // some might call a variation of this class the lumberjack. it would grow trees and cut down old ones, and either just burn them, or potentially stack them in a lumberyard or somesuch, or perhaps both...
    // a "log" is a single line.
    // a "branch" is a set of lines that were sent at the same time code.
    // a "tree" is an entire log file.
    // a "grove" is a set of log files that were sent on the same day.
    // a "forest" is a folder of log files for a given program or aspect.
    // a log with program/aspect name "prog", date d:07 m:08 y:2009, the 4rth one on that date, with a time of 12:34 military time, and number 11 at that time,
    // will be found in the file "logs/prog/log-07-08-09-#4.log.txt", with a prefix of "[12:34 #11]:"
}
