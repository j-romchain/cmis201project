package org.theBestCMISGroup.FileManagement.LogManagement;

import org.theBestCMISGroup.Utils.Storage.EasyList;

import javax.naming.NameNotFoundException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Branch {
    //this represents the group of logs made in one second in a log file
    private EasyList<Log> logs;
    private final Date growthDate;
    private final Tree myTree;
    private String contents;

    public Branch(File logFile, String logID) throws ParseException, FileNotFoundException {
        this(new Tree(logFile), logID);
    }

    public Branch(File logFile, Date growthDate) throws ParseException, FileNotFoundException {
        this(new Tree(logFile), growthDate);
    }


    public Branch(Tree tree, String logID) throws ParseException {
        this(tree, parseLogTime(logID));
    }

    public static Date parseLogTime(String logID) throws ParseException {
        return new SimpleDateFormat("[HH:mm").parse(logID);
    }

    public Branch(Tree tree, Date growthDate) {
        this.myTree = tree;
        this.growthDate=growthDate;
        init();
    }

    private void init() {
        this.contents = "";
        String wholeFile = myTree.getContents();
        for (String line:wholeFile.split("\n")) {
            if (isPart(myTree.getFile(), line)) {
                //noinspection StringConcatenationInLoop
                contents+=(contents!=null?"\n":"")+line;
            }
        }
    }

    public static String getLogIDPart(Date growthDate) {
        return new SimpleDateFormat("[HH:mm").format(growthDate);
    }

    public String getLogIDPart() {
        return getLogIDPart(growthDate);
    }
    public static String getPath(String forestName, Date growthDate, int treeNum) {
        return Tree.getPath(forestName,growthDate,treeNum);
    }
    public String getPath() {
        return getTree().getPath();
    }

    public Tree getTree() {
        return myTree;
    }

    public static Branch createBranch(String forestName, Date treeGrowthDate, int treeNum, Date branchGrowthDate) throws IOException {
        return new Branch(Tree.createTree(forestName, treeGrowthDate,treeNum), branchGrowthDate);
    }

    public void update() {
        logs = new EasyList<>();
        for (String log : contents.split("\n")) {
            try {
                logs.add(new Log(this, log.split(":")[0]));
            } catch (NameNotFoundException ignored) {}
        }
    }

    public boolean isPart(File logFile,String logLine) {
        return myTree.is(logFile)&&logLine.contains(getLogIDPart());
    }

    public Date getGrowthDate() {
        return growthDate;
    }

    public EasyList<Log> getLogs() {
        return logs;
    }

    public String getContents() {
        update();
        return contents;
    }

    public Log getLog(int logNum) throws NameNotFoundException {
        return new Log(this, logNum);
    }
}
