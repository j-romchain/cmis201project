package org.theBestCMISGroup.FileManagement.LogManagement;

import org.theBestCMISGroup.Utils.Storage.EasyList;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Grove {
    //this is a set of log files that were made on the same day
    private EasyList<Tree> trees;
    private final Date growthDate;
    private final Forest myForest;
    public Grove(File logFile) throws FileNotFoundException, ParseException {
        this(new Forest(logFile.getAbsoluteFile().getParent()),logFile);
    }
    public Grove(Forest forest, File logFile) throws ParseException {
        this(forest,logFile.getName());
    }
    public Grove(Forest forest, String fileID) throws ParseException {
        this(forest, parseGroveDate(fileID));
    }
    public static Date parseGroveDate(String fileID) throws ParseException {
        //noinspection SuspiciousDateFormat
        return new SimpleDateFormat("log-dd-MM-YY").parse(fileID);
    }
    public Grove(Forest forest, Date growthDate) {
        this.myForest=forest;
        this.growthDate=growthDate;
    }
    public static Grove createGrove(String forestName,Date growthDate) throws FileAlreadyExistsException, FileNotFoundException {
        return new Grove(Forest.createForest(forestName),growthDate);
    }
    public void update() {
        trees = new EasyList<>();
        try {
            //noinspection DataFlowIssue
            for (File logFile : myForest.getForestFolder().listFiles()) {
                if (isPart(logFile)){
                    try {
                        trees.add(new Tree(this, logFile));
                    } catch (FileNotFoundException ignored) {}
                }
            }
        } catch (NullPointerException ignored) {
        }
    }
    public static String getPathPart(String forestName, Date growthDate) {
        //noinspection SuspiciousDateFormat
        return Forest.getPath(forestName) + "/log-" + new SimpleDateFormat("dd-MM-YY").format(growthDate);
    }
    public String getPathPart() {
        return getPathPart(myForest.getForestName(),growthDate);
    }
    public Forest getForest() {
        return myForest;
    }

    public boolean isPart(File treeBranchOrLogFile) {
        return treeBranchOrLogFile.getAbsolutePath().contains(getPathPart());
    }

    public Date getGrowthDate() {
        return growthDate;
    }

    protected EasyList<File> getGroveFiles() {
        EasyList<File> ret = new EasyList<>();
        for (Tree tree:trees){
            ret.add(tree.getFile());
        }
        return ret;
    }

    public EasyList<Tree> getTrees() {
        update();
        return trees.clone();
    }

    public Tree getTree(int treeNum) throws FileNotFoundException {
        return new Tree(this, treeNum);
    }

}
