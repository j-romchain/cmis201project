package org.theBestCMISGroup.FileManagement.LogManagement;

import org.theBestCMISGroup.Utils.Storage.EasyList;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;
import java.text.ParseException;
import java.util.Date;

public class Forest {
    //this represents a folder full of log files
    private EasyList<Grove> groves;
    private final String forestName;
    private final File forestFolder;

    public Forest(File logFile) throws FileNotFoundException {
        this(logFile.getAbsoluteFile().getParent());
    }
    public Forest(String forestName) throws FileNotFoundException {
        this.forestName=forestName;
        forestFolder=new File("logs/"+ forestName);
        if (!forestFolder.exists()) {
            throw new FileNotFoundException("A log folder must be made before you can referece it like this. Try running Forest.createForest() or Logger.createLogDir() if you want to create one. The path would be \""+forestFolder.getAbsolutePath()+"\"");
        }
        if (!forestFolder.isDirectory()) {
            throw new FileNotFoundException("There is a file where a folder should be. Please remove the file and try creating the logging directory again by running Forest.createForest() or Logger.createLogDir(). The path should be \""+forestFolder.getAbsolutePath()+"\"");
        }
    }
    public static Forest createForest(String forestName) throws FileAlreadyExistsException, FileNotFoundException {
        File forestFolder=new File(getPath(forestName));
        if (forestFolder.exists()&&!forestFolder.isDirectory()) {
            throw new FileAlreadyExistsException("Cannot create the Directory for the log file, because another file is in the way. The path would be \""+forestFolder.getAbsolutePath()+"\"");
        }
        if (forestFolder.exists()||forestFolder.mkdirs()) {
            return new Forest(forestName);
        } else {
            throw new FileNotFoundException("The directory for the log files could not be created. Most likely because of a file being somewhere where a directory should be. Ensure you can create this directory: \""+forestFolder.getAbsolutePath()+"\"");
        }
    }
    public static String getPath(String forestName) {
        return "logs/"+ forestName;
    }
    public String getPath() {
        if (forestFolder==null) {
            return getPath(forestName);
        }
        return forestFolder.getPath();
    }
    public void update() {
        groves=new EasyList<>();//resets list and then for each file that doesn't have a grove for it already, it adds a new grove.
        try {
            //noinspection DataFlowIssue
            for (File logFile : forestFolder.listFiles()) {
                if (!groves.contains((agrove) -> agrove.isPart(logFile))) {
                    try {
                        groves.add(new Grove(this, logFile));
                    } catch (ParseException ignored){}
                }
            }
        } catch (NullPointerException ignored) {}
    }
    public boolean isPart(File groveTreeBranchOrLogFile){
        return groveTreeBranchOrLogFile.getAbsolutePath().contains(forestFolder.getAbsolutePath());
    }

    public String getForestName() {
        return forestName;
    }

    protected File getForestFolder() {
        return forestFolder;
    }

    public EasyList<Grove> getGroves() {
        update();
        return groves.clone();
    }
    public Grove getGrove(Date date) {
        return new Grove(this,date);
    }
}
