package org.theBestCMISGroup.FileManagement.Utils;

import org.theBestCMISGroup.Main;

import java.io.*;
import java.net.URL;
import java.util.Scanner;

import static java.util.Objects.requireNonNull;

public class FileFunctions {
    public static void delFile(File file) throws IOException {
        if (file.isDirectory()&&file.listFiles()!=null) {
            for (File subfile : requireNonNull(file.listFiles())) {
                delFile(subfile);
            }
        }
        file.delete();
        if (file.exists()) {
            throw new IOException("couldn't delete the file/dir \"" + file + "\" for some reason.");
        }
    }
    public static void createEmptyDir(File dir) throws IOException {
        delFile(dir);
        dir.mkdirs();
        dir.mkdir();
        if (!dir.isDirectory()) {
            throw new IOException("couldn't (re)create the empty dir \"" + dir + "\" for some unknown reason.");
        }
    }
    public static void createBlankFile(File file) throws IOException {
        delFile(file);
        File parent = file.getParentFile();
        if (parent!=null){parent.mkdirs();}
        file.createNewFile();
        if (!file.isFile()&& !new Scanner(file).hasNext()) {
            throw new IOException("couldn't (re)create the empty file \"" + file + "\" for some unknown reason.");
        }
    }
    public static void createTextFileWithContents(File file, String contents) throws IOException {
        createBlankFile(file);
        appendToTextFile(file,contents);
    }
    public static void appendToTextFile(File file, String appendem) throws IOException {
        FileWriter saveFileWriter = new FileWriter(file,true);
        saveFileWriter.write(appendem);
        saveFileWriter.close();
    }
    public static String getTextFileContents(File file) throws FileNotFoundException {
        FileReader fileReader = new FileReader(file);
        Scanner fileScanner = new Scanner(fileReader);
        return unravelScanner(fileScanner);
    }
    public static String getResourceContents(String resourceName) throws IOException {
        URL resource = Main.class.getClassLoader().getResource(resourceName);
        if (resource == null) {
            throw new NullPointerException("couldn't load resource \""+resourceName+"\" because it doesn't seem to exist.");
        }
        return getURLContents(resource);
    }
    public static String getURLContents(URL resource) throws IOException {
        InputStream resourceStream = (InputStream) resource.getContent();
        Scanner resourceScanner = new Scanner(resourceStream);
        return unravelScanner(resourceScanner);
    }
    public static String unravelScanner(Scanner scanner) {
        StringBuilder contents = new StringBuilder();
        if (scanner.hasNextLine()) {
            contents.append(scanner.nextLine());
        }
        while (scanner.hasNextLine()) {
            contents.append("\n");
            contents.append(scanner.nextLine());
        }
        return contents.toString();
    }
}
