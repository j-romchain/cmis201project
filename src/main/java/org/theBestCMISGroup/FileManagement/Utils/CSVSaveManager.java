package org.theBestCMISGroup.FileManagement.Utils;

import org.theBestCMISGroup.Utils.Storage.EasyList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

@SuppressWarnings("unchecked")
public class CSVSaveManager {
    public static void save(EasyList<EasyList<String>> data, File saveFile) throws IOException {
        EasyList<String>[] listData = data.toTypedArray();
        String[][] listData2d = new String[listData.length][];
        for (int i = 0; i < listData.length; i++) {
            listData2d[i]=listData[i].toTypedArray();
        }
        save(listData2d,saveFile);
    }
    public static void save(String[][] data, File saveFile) throws IOException {
        saveCustomDelimiter(data,saveFile,",");
    }
    public static void saveCustomDelimiter(String[][] data, File saveFile, String delimiter) throws IOException {
        FileFunctions.createTextFileWithContents(saveFile,toSVString(data,delimiter,"\n"));
    }
    public static String toSVString(String[][] data,String delimiter1,String delimiter2) {
        StringBuilder ret = new StringBuilder();
        for (String[] row:data) {
            for (String value:row) {
                ret.append(value).append(delimiter2);
            }
            ret.append("\n");
        }
        return ret.toString();
    }
    public static EasyList<EasyList<String>> fromSVString(String SV, String delimiter1,String delimiter2) {
        EasyList<EasyList<String>> ret = new EasyList<>();
        EasyList<String> lines = new EasyList<>();
        lines.addAll(List.of(SV.split(delimiter2)));
        for (int i = 0; i < lines.length(); i++) {
            ret.add(new EasyList<>());
            String line = lines.get(i);
            ret.get(ret.length()-1).addAll(List.of(line.split(delimiter1)));
        }
        if (ret.length()==1&&ret.get(0).length()==1) {
            return new EasyList<>();
        }
        return ret;
    }
    public static EasyList<EasyList<String>> load(File saveFile) throws FileNotFoundException {
        return loadCustomDelimiter(saveFile,",");
    }
    public static String[][] loadArray(File saveFile) throws FileNotFoundException {
        return loadCustomDelimiterArray(saveFile,",");

    }
    public static EasyList<EasyList<String>> loadCustomDelimiter(File saveFile, String delimiter) throws FileNotFoundException {
        return fromSVString(FileFunctions.getTextFileContents(saveFile),delimiter,"\n");
    }
    public static String[][] loadCustomDelimiterArray(File saveFile, String delimiter) throws FileNotFoundException {
        EasyList<String>[] listData = loadCustomDelimiter(saveFile,delimiter).toTypedArray();
        String[][] listData2d = new String[listData.length][];
        for (int i = 0; i < listData.length; i++) {
            listData2d[i]=listData[i].toTypedArray();
        }
        return listData2d;
    }
    public static <T> T[][] flip2dlist(T[][] flipme) {
        EasyList<T[]> ret = new EasyList<>();
        for (int i = 0; i < flipme.length; i++) {
            for (int j = 0; j < flipme[i].length; j++) {
                if (ret.length()<j) {
                    ret.add((T[]) new Object[flipme.length]);
                }
                ret.get(j)[i]=flipme[i][j];
            }
        }
        return ret.toTypedArray();
    }

    public static <T> EasyList<EasyList<T>> flip2dlist(EasyList<EasyList<T>> flipme) {
        EasyList<EasyList<T>> ret = new EasyList<>();
        for (int i = 0; i < flipme.length(); i++) {
            for (int j = 0; j < flipme.get(i).length(); j++) {
                if (ret.length()<j) {
                    ret.add(new EasyList<>());
                }
                ret.get(j).set(i,flipme.get(i).get(j));
            }
        }
        return ret;
    }
}