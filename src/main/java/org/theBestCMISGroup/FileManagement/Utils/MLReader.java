package org.theBestCMISGroup.FileManagement.Utils;

import org.theBestCMISGroup.Utils.Storage.EasyDict;
import org.theBestCMISGroup.Utils.Storage.EasyList;

import java.io.File;
import java.io.FileNotFoundException;

public class MLReader {
    public static void main(String[] args) throws FileNotFoundException {
//        System.out.println(new ParsedContents("""
//                before<tag type="start">middle<tag type="self ending,middle"/></tag>after"""));
        System.out.println(loadFile(new File("Docs/testMap.osm")));
    }
//scan for <, then scan for >. and you have a marker
    //do this for all, and then a tag is made of 2 markers, they can be the same. the end one has a "/"
    //a tag can contain tags.
    //a tag has properties
        // to parse, start by finding all start tags
            // and then in a loop, until there are no remaining end markers,
                //starting with self
                //for each end marker,
                    //check if is a start for my type of marker
                        //if so, then pair and remove both self and pair from list
                //and go one more level up and repeat
    public static MLReader loadFile(File mlFile) throws FileNotFoundException {
        return new MLReader(FileFunctions.getTextFileContents(mlFile));
    }
    public MLReader() {this("");}
    public MLReader(String mlString) {
        rootTag= MLTag.createMLTag(MLTag.MLTagMarker.createMLTagMarker("root", MLTag.MLTagMarker.MarkerEndType.start), MLTag.MLTagMarker.createMLTagMarker("root", MLTag.MLTagMarker.MarkerEndType.end),mlString);
    }
    public MLTag rootTag;

    public MLTag getRootTag() {
        return rootTag;
    }

    @Override
    public String toString() {
        return "ML{"+rootTag+"}";
    }
    public static class ParsedContents {
        private final String preMarkersPart;
        private final String startMarkerPart;
        private String betweenMarkersPart;
        private boolean selfEnd;
        private final String endMarkerPart;
        private final String afterMarkersPart;
        public ParsedContents(String contents) {
            String unassignedPart = contents;
            //look for start of tag, then
            preMarkersPart = unassignedPart.substring(0,unassignedPart.indexOf("<"));
            unassignedPart = unassignedPart.substring(unassignedPart.indexOf("<"));
            startMarkerPart = unassignedPart.substring(0, unassignedPart.indexOf(">")+1);
            unassignedPart = unassignedPart.substring(unassignedPart.indexOf(">")+1);
            selfEnd=MLTag.MLTagMarker.MarkerEndType.ofRaw(startMarkerPart)==MLTag.MLTagMarker.MarkerEndType.selfEnding;
//            System.out.println("startpart"+startMarkerPart);
            String cantFindEndMarkerBackup = unassignedPart;
            if (selfEnd) {
                betweenMarkersPart="";
                endMarkerPart=startMarkerPart;
                afterMarkersPart=unassignedPart;
            } else {
                String startMarkerType = MLTag.MLTagMarker.markerType(startMarkerPart);
                betweenMarkersPart="";
                String endPartTemp="";
                int layers = 1;
                while (unassignedPart.contains("<")) {// if there is still another marker
                    betweenMarkersPart += unassignedPart.substring(0,unassignedPart.indexOf("<"));//first append the stuff before trimming
                    unassignedPart=unassignedPart.substring(unassignedPart.indexOf("<"));//and then trim it down to the "<"
                    if (unassignedPart.startsWith("</"+startMarkerType)) {
                        //end type
                        layers--;
                    } else if (unassignedPart.startsWith("<"+startMarkerType)) {
                        //is type, start or self end
                        layers++;
                        if (unassignedPart.charAt(unassignedPart.indexOf(">")-1)=='/') {
                            //self end
                            layers--;
                        }// else {
                            //start
                        //}
                    }// else {
                        //isn't type
                    //}
                    if (layers==0) {
                        //one more layer ended then layers started.
                        endPartTemp=unassignedPart.substring(0,unassignedPart.indexOf(">")+1);
                        unassignedPart=unassignedPart.substring(unassignedPart.indexOf(">")+1);
                        //between markers was already considered at the start of the loop.
                        break;
                    }
                    unassignedPart=unassignedPart.substring(unassignedPart.indexOf("<")+1);//trim off the start so it will read properly on the next round
                    betweenMarkersPart += "<";//account for the trimming
                }
                if (endPartTemp.isEmpty()) {
                    System.out.println("start marker found with no end marker, reading tag type:"+startMarkerType);
                    System.out.println("treating like self ending.");
                    selfEnd = true;
                    betweenMarkersPart="";
                    endMarkerPart=startMarkerPart;
                    unassignedPart=cantFindEndMarkerBackup;
                } else {
                    endMarkerPart=endPartTemp;
                }
                afterMarkersPart=unassignedPart;
            }
        }

        @Override
        public String toString() {
            return "ParsedContents{" +
                    "preMarkersPart='" + preMarkersPart + '\'' +
                    ", startMarkerPart='" + startMarkerPart + '\'' +
                    ", betweenMarkersPart='" + betweenMarkersPart + '\'' +
                    ", selfEnd=" + selfEnd +
                    ", endMarkerPart='" + endMarkerPart + '\'' +
                    ", afterMarkersPart='" + afterMarkersPart + '\'' +
                    '}';
        }
    }
    public static class MLTag {
        public static MLTag parse(String contents) {
//            System.out.println(temp+"makeTagParseTag");
            ParsedContents parsed = new ParsedContents(contents);
            return new MLTag(
                    MLTagMarker.parse(parsed.startMarkerPart),
                    MLTagMarker.parse(parsed.endMarkerPart), parseContents(parsed.betweenMarkersPart));
        }
        private MLTag(MLTagMarker startMark, MLTagMarker endMark, MLTag[] contents) {
            this.startMark=startMark;
            this.endMark=endMark;
            this.contents=new EasyList<>(contents);
        }
        private static MLTag[] parseContents(String contents) {
//            System.out.println(temp+"parseContents");
            EasyList<MLTag> parsedTags = new EasyList<>();
            String nonAssigned = contents;
            if (!contents.contains("<")) {
                return new MLTag[0];
            }
            while (contents.contains("<")) {
                //while there is still a marker,
                //parse that tag
//                System.out.println("contents:\""+contents+'"');
                parsedTags.add(MLTag.parse(contents));
                //and then jump to where it would have ended, and repeat until no more tags
                contents=new ParsedContents(contents).afterMarkersPart;
            }
            return parsedTags.toArray(new MLTag[0]);
        }
        MLTagMarker startMark;
        MLTagMarker endMark;
        EasyList<MLTag> contents;

        public static MLTag createMLTag(MLTagMarker startMark, MLTagMarker endMark, String contents) {
//            System.out.println(temp+"makeTagParseContents");
            return new MLTag(startMark,endMark,MLTag.parseContents(contents));
        }

        public static MLTag createMLTag(MLTagMarker startMark, MLTagMarker endMark) {
//            System.out.println(temp+"makeTagBlankContents");
            return new MLTag(startMark,endMark,new MLTag[0]);
        }

        public static MLTag createMLTag(MLTagMarker startMark, MLTagMarker endMark, MLTag[] contents) {
//            System.out.println(temp+"makeTagGivenContents");
            return new MLTag(startMark, endMark, contents);
        }

        @Override
        public String toString() {
            return "MLTag{" +
                    "startMark=" + startMark +
                    (startMark.getEndType()== MLTagMarker.MarkerEndType.selfEnding ? "": ", endMark=" + endMark) +
                    ", contents=" + contents +
                    '}';
        }

        public static class MLTagMarker {
            String markerType;
            MarkerEndType endType;
            EasyDict<String,String> properties;

            public static MLTagMarker createMLTagMarker(String markerType, MarkerEndType endType) {
//                System.out.println(temp+"makeMarkerNoValues");
                return new MLTagMarker(markerType,endType,new EasyDict<>());
            }

            public static MLTagMarker createMLTagMarker(String markerType, MarkerEndType endType, EasyDict<String, String> properties) {
//                System.out.println(temp+"makeMarkerGivenProperties");
                return new MLTagMarker(markerType, endType, properties);
            }

            @Override
            public String toString() {
                return "MLTagMarker{" +
                        "markerType='" + markerType + '\'' +
                        ", endType=" + endType +
                        ", properties=" + properties +
                        '}';
            }

            public static enum MarkerEndType {
                start,
                end,
                selfEnding;
                public static MarkerEndType ofTrimmed(String trimmedMarker){
                    return trimmedMarker.endsWith("/")? selfEnding: (trimmedMarker.startsWith("/")?end:start);
                }

                public static MarkerEndType ofRaw(String rawMarker) {
                    return ofTrimmed(trim(rawMarker));
                }
            }
            public static MLTagMarker parse(String rawMarker) {
//                System.out.println(temp+"makeMarkerParse");
                String type = markerType(rawMarker);
                MarkerEndType end = MarkerEndType.ofRaw(rawMarker);
                EasyDict<String,String> vals =parseValues(trim(rawMarker));
                return new MLTagMarker(type,end,vals);
            }
            private MLTagMarker(String markerType, MarkerEndType endType, EasyDict<String, String> properties) {
                this.markerType=markerType;
                this.endType=endType;
                this.properties=properties;
            }

            public String getMarkerType() {
                return markerType;
            }

            public MarkerEndType getEndType() {
                return endType;
            }

            public EasyDict<String, String> getProperties() {
                return properties;
            }

            private static String markerType(String rawMarker) {
                String trimmed = trim(rawMarker);
                String justFirstVar = trimmed.contains(" ") ?trimmed.substring(0,trimmed.indexOf(" ")):trimmed;
                if (justFirstVar.contains("/")) {
                    return justFirstVar.substring(1);
                }
                return justFirstVar;
            }
            private static MarkerEndType markerEndTypeRaw(String rawMarker){
                return MarkerEndType.ofTrimmed(trim(rawMarker));
            }
            private static String trim(String rawMarker) {
                String noStartPart = rawMarker.contains("<") ? rawMarker.substring(rawMarker.indexOf("<")+1):rawMarker;
                return noStartPart.contains(">") ? noStartPart.substring(0,noStartPart.indexOf(">")):noStartPart;
            }

            private static EasyDict<String,String> parseValues(String trimmedMarker) {
//                System.out.println(temp+"parseValues");
                if (!trimmedMarker.contains("=")) {
//                    System.out.println("none");
                    return new EasyDict<>();
                }
                String valuesOnly;
                {//getting rid of non values, first the id, and then the slash if there is one at the end
                    valuesOnly=trimmedMarker.substring(trimmedMarker.indexOf(" "));
                    if (valuesOnly.endsWith("/")) {
                        valuesOnly=valuesOnly.substring(0,valuesOnly.length()-1);
                    }
                }
                EasyDict<String,String> values = new EasyDict<>();
                while (!valuesOnly.isEmpty()) {
//                    System.out.println(valuesOnly);
                    valuesOnly=valuesOnly.trim();//trim, take from here to the =, then take from the quote to the quote, and repeat.
                    if (valuesOnly.contains("=")) {
                        String key = valuesOnly.substring(0,valuesOnly.indexOf("="));//start to = is key
                        valuesOnly=valuesOnly.substring(valuesOnly.indexOf("=")+1);//remove that bit
                        if (!valuesOnly.isEmpty()){//error if no value
                            char quoteType = valuesOnly.charAt(0);//get start quote mark type
                            valuesOnly=valuesOnly.substring(1);//remove start quote from it
                            if (valuesOnly.contains(quoteType+"")){//error if no end quote
                                String value = valuesOnly.substring(0,valuesOnly.indexOf(quoteType));//value goes between and not including quotes
                                valuesOnly=valuesOnly.substring(valuesOnly.indexOf(quoteType)+1);
                                values.add(key,value);
                            } else {
                                System.out.println("found a value without a close Quote... \""+valuesOnly+"\"");
                                values.add(key,valuesOnly);
                                valuesOnly="";
                            }
                        } else {
                            System.out.println("found key without a value... \""+key+"\"");
                            values.add(key,"");
                        }
                    } else {
                        if (!valuesOnly.trim().isEmpty()){
                            System.out.println("no more =, and I read the last value already, but theres more text... putting in the pair: {noKeyA:\""+valuesOnly+"\"}");
                            values.add("noKeyA",valuesOnly);
                            valuesOnly="";
                        }
//                        System.out.println("found an inexplicable miscellaneous charachter at the end of the properties section of a marker in the ml, ignoring it.");
                    }
                    valuesOnly=valuesOnly.trim();
                }
                return values;
            }

        }
    }
}
