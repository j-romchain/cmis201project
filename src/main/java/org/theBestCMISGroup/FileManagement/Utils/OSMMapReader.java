package org.theBestCMISGroup.FileManagement.Utils;

import org.theBestCMISGroup.Utils.MiscDataTypes.CoordinatePair;
import org.theBestCMISGroup.Utils.Storage.EasyList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class OSMMapReader extends MLReader {
    EasyList<OSMRoad> roads = new EasyList<>();
    EasyList<OSMNode> nodes = new EasyList<>();
    public OSMMapReader(File roadMapOSMFile) throws IOException, ParserConfigurationException, SAXException {
        // Parse the OSM XML file
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(roadMapOSMFile);
        // Extract nodes and roads
        parseNodes(doc);
        parseRoads(doc);
    }

    public EasyList<OSMNode> getNodes() {
        return nodes;
    }
    public EasyList<OSMRoad> getRoads() {
        return roads;
    }

    private void parseNodes(Document doc) {
        NodeList nodesList = doc.getElementsByTagName("node");
        for (int i = 0; i < nodesList.getLength(); i++) {
            Node node = nodesList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                nodes.add(new OSMNode(node));
            }
        }
    }
    private void parseRoads(Document doc) {
        NodeList waysList = doc.getElementsByTagName("way");
        for (int i = 0; i < waysList.getLength(); i++) {
            Node way = waysList.item(i);
            if (way.getNodeType() == Node.ELEMENT_NODE) {
                Element wayElement = (Element) way;
                roads.add(new OSMRoad(wayElement));
            }
        }
    }
    public static class OSMRoad {
        private final EasyList<Long> nodeIDs = new EasyList<>();
        private int speedLimMPH = 25;
        private int laneCount;
        private String laneDirections;
        public OSMRoad(Element wayElement) {
            // Extract node ID's
            NodeList nodeRefTags = wayElement.getElementsByTagName("nd");
            for (int j = 0; j < nodeRefTags.getLength(); j++) {
                Element tagElement = (Element) nodeRefTags.item(j);
                nodeIDs.add(Long.parseLong(tagElement.getAttribute("ref")));
            }
            // Extract data from tags
            NodeList tags = wayElement.getElementsByTagName("tag");
            label:
            for (int j = 0; j < tags.getLength(); j++) {
                Element tagElement = (Element) tags.item(j);
                switch (tagElement.getAttribute("k")) {
                    case "lanes":
                        laneCount = Integer.parseInt(tagElement.getAttribute("v"));
                        break label;
                    case "note:lanes":
                        laneDirections = tagElement.getAttribute("v");
                        break label;
                    case "maxSpeed":
                        String speedString = tagElement.getAttribute("v");
                        if (speedString.contains("MPH")) {
                            speedLimMPH = Integer.parseInt(speedString.replace("MPH", ""));
                        } else if (speedString.contains("KPH")) {
                            speedLimMPH = (int) (Integer.parseInt(speedString.replace("KPH", "")) * 0.6213711922);
                        }
                        break label;
                }
            }
        }

        public int lanesFromStart(CoordinatePair start, CoordinatePair end) {
            String laneSpecifier = getLaneDirections();
            if (laneSpecifier.isEmpty()) {
                return (int) Math.ceil(getLaneCount()/2d);
            } else {
                String leftSpecification = laneSpecifier.substring(0,laneSpecifier.indexOf(","));
                Direction leftDirection = Direction.of(leftSpecification);
                char[] charArray = leftSpecification.toCharArray();
                String numbers ="123456789";
                StringBuilder leftLaneCountStr = new StringBuilder();
                for (char c : charArray) {
                    for (char number : numbers.toCharArray()) {
                        if (c == number) {
                            leftLaneCountStr.append(c);
                        }
                    }
                }
                int leftLaneCount=Integer.parseInt(leftLaneCountStr.toString());
                if (leftDirection.canBe(start,end)) {
                    return leftLaneCount;
                } else {
                    return getLaneCount()-leftLaneCount;
                }
            }
        }
        public int lanesFromEnd(CoordinatePair start, CoordinatePair end) {
            return laneCount-lanesFromStart(start, end);
        }

        public int getSpeedLimMPH() {
            return speedLimMPH;
        }

        public int getLaneCount() {
            return laneCount;
        }
        public String getLaneDirections() {
            return laneDirections;
        }

        public Long[] getNodeIDs() {
            return nodeIDs.toTypedArray();
        }

        private static enum Direction {NORTH,SOUTH,EAST,WEST;
            static Direction of(String dir) {
                if (dir.toLowerCase().contains("north")) {
                    return NORTH;
                } else if (dir.toLowerCase().contains("south")) {
                    return SOUTH;
                } else if (dir.toLowerCase().contains("east")) {
                    return EAST;
                } else if (dir.toLowerCase().contains("west")) {
                    return WEST;
                } else {
                    throw new RuntimeException("noDirection");
                }
            }
            Boolean canBe(CoordinatePair from,CoordinatePair to) {
                double longitudeDifference = to.getLongitude()-from.getLongitude();
                if (longitudeDifference>180) {
                    longitudeDifference-=360;
                }
                switch (this) {
                    case NORTH -> {
                        return to.getLatitude() > from.getLatitude();
                    }
                    case SOUTH -> {
                        return to.getLatitude() < from.getLatitude();
                    }
                    case EAST -> {
                        return longitudeDifference>0;
                    }
                    case WEST -> {
                        return longitudeDifference<0;
                    }
                    default -> throw new RuntimeException("noDirection");
                }
            }
            static Direction of(CoordinatePair from,CoordinatePair to) {
                if (to.getLatitude()>from.getLatitude()) {
                    return NORTH;
                } else if (to.getLatitude()<from.getLatitude()) {
                    return SOUTH;
                } else {
                    double longitudeDifference = to.getLongitude()-from.getLongitude();
                    if (longitudeDifference>180) {
                        longitudeDifference-=360;
                    }
                    if (longitudeDifference>0) {
                        return EAST;
                    } else if (longitudeDifference<0) {
                        return WEST;
                    } else {
                        throw new RuntimeException("noDirection");
                    }
                }
            }
        }
    }
    public static class OSMNode {

        private final CoordinatePair pos;
        private final long id;

        public OSMNode(Node node) {
            Element nodeElemen = (Element) node;
            this.id = Long.parseLong(nodeElemen.getAttribute("id"));
            double lat = Double.parseDouble(nodeElemen.getElementsByTagName("lat").item(0).getTextContent());
            double lon = Double.parseDouble(nodeElemen.getElementsByTagName("lon").item(0).getTextContent());
            this.pos = new CoordinatePair(lat, lon);
        }

        public long getID() {
            return id;
        }
        public CoordinatePair getPos() {
            return pos;
        }
    }
}
