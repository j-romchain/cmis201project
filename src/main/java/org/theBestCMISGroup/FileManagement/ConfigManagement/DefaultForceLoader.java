package org.theBestCMISGroup.FileManagement.ConfigManagement;

import org.theBestCMISGroup.FileManagement.Utils.FileFunctions;

import java.io.File;
import java.io.IOException;

public class DefaultForceLoader {
    public static void main(String[] args) {
        //(force)loads defaults by default
        loadDefaultAllConfig();
    }
    public static void loadDefaultBusRoutes() {
        File busRoutesFolder = new File("config/BusRoutes");
        try {
            FileFunctions.createEmptyDir(busRoutesFolder);
        } catch (IOException e) {
            System.out.println("couldn't load default bus config because I couldn't (re)create the base directory, pretending that I did.");
            return;
        }
        String routes;
        try {
            routes = FileFunctions.getResourceContents("defaultConfig/defaultBusRoutes");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        for (String route : routes.split("\n")) {
            String routeDefaultContents;
            try {
                routeDefaultContents = FileFunctions.getResourceContents("defaultConfig/defaultBusRoutes/" + route);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            File routeFile = new File(busRoutesFolder.getPath() + "/" + route);
            try {
                FileFunctions.createTextFileWithContents(routeFile, routeDefaultContents);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void loadDefaultConfigFile() {
        String fileDefaultContents;
        try {
            fileDefaultContents = FileFunctions.getResourceContents("defaultConfig/defaultConfig.csv");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        File file = new File("config/Config.csv");
        try {
            FileFunctions.createTextFileWithContents(file, fileDefaultContents);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void loadDefaultRoadMapFile() {
        String fileDefaultContents;
        try {
            fileDefaultContents = FileFunctions.getResourceContents("defaultConfig/defaultRoadMap.csv");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        File file = new File("config/RoadMap.csv");
        try {
            FileFunctions.createTextFileWithContents(file, fileDefaultContents);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void loadConfigInfo() {
        String fileDefaultContents;
        try {
            fileDefaultContents = FileFunctions.getResourceContents("defaultConfig/configInfo.txt");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        File file = new File("config/configInfo.txt");
        try {
            FileFunctions.createTextFileWithContents(file, fileDefaultContents);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void loadDefaultAllConfig() {
        loadConfigInfo();
        loadDefaultConfigFile();
        loadDefaultRoadMapFile();
        loadDefaultBusRoutes();
    }
}