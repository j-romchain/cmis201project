package org.theBestCMISGroup.FileManagement.ConfigManagement;

import org.theBestCMISGroup.FileManagement.Utils.OSMMapReader;
import org.theBestCMISGroup.FileManagement.Utils.SVG;
import org.theBestCMISGroup.FileManagement.Utils.SVSaveManager;
import org.theBestCMISGroup.Utils.Storage.EasyList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class ConfigLoader {
    public static final File configFolder = new File("config");
    public static final File configFile = new File(configFolder.getPath()+"/Config.csv");
    public static final File roadMapCSVFile = new File(configFolder.getPath()+"/RoadMap.csv");
    public static final File roadMapSVGFile = new File(configFolder.getPath()+"/RoadMap.svg");
    public static final File roadMapOSMFile = new File(configFolder.getPath()+"/RoadMap.osm");
    public static final File busRoutesFolder = new File(configFolder.getPath()+"/BusRoutes");
    public static EasyList<EasyList<String>> getConfig() {
        if (!configFile.isFile()) {
            System.out.println("config file either is not a file or does not exist. attempting to replace it with default at " + configFile.getAbsolutePath());
            DefaultForceLoader.loadDefaultConfigFile();
        }
        try {
            return SVSaveManager.load(configFile);
        } catch (IOException e) {
            throw new RuntimeException("couldn't access the contents of the config file at " + configFile.getAbsolutePath());
        }
    }
    private static void loadDefaultRoadIfNoneExist() {
        if (!roadMapCSVFile.isFile() && !roadMapSVGFile.isFile() && !roadMapOSMFile.isFile()) {
            System.out.println("there is no road config file, or if there is there's something else wrong. attempting to force insert/replace with the default road config at " + configFile.getAbsolutePath());
            DefaultForceLoader.loadDefaultRoadMapFile();
        }
    }
    public enum RoadMapFormat {
        CSV,
        SVG,
        OSM
    }
    public static RoadMapFormat getRoadMapFormat() {
        if (roadMapCSVFile.isFile()) {
            return RoadMapFormat.CSV;
        } else if (roadMapSVGFile.isFile()) {
            return RoadMapFormat.SVG;
        } else if (roadMapOSMFile.isFile()) {
            return RoadMapFormat.OSM;
        } else {
            DefaultForceLoader.loadDefaultRoadMapFile();
            return RoadMapFormat.CSV;
        }
    }
    public static EasyList<EasyList<String>> getRoadMapCSV() {
        if (!roadMapCSVFile.isFile()) {
            return null;
        }
        try {
            return SVSaveManager.load(roadMapCSVFile);
        } catch (IOException e) {
            throw new RuntimeException("couldn't access the contents of the road map file at " + roadMapCSVFile.getAbsolutePath());
        }
    }
    public static SVG getRoadMapSVG() {
        if (!roadMapSVGFile.isFile()) {
            return null;
        }
        try {
            return SVG.load(roadMapSVGFile);
        } catch (IOException e) {
            throw new RuntimeException("couldn't access the contents of the road map file at " + roadMapSVGFile.getAbsolutePath());
        }
    }
    public static OSMMapReader getRoadMapOSM() {
        if (!roadMapOSMFile.isFile()) {
            return null;
        }
        try {
            return new OSMMapReader(roadMapOSMFile);
        } catch (IOException | ParserConfigurationException | SAXException e) {
            throw new RuntimeException("couldn't access or parse or something the road map file at " + roadMapOSMFile.getAbsolutePath());
        }
    }
    public static EasyList<EasyList<EasyList<String>>> getBusRoutes() {
        if (!busRoutesFolder.isDirectory()) {
            System.out.println("bus routes config is not a folder or does not exist. attempting to replace it with default at "+busRoutesFolder.getAbsolutePath());
            DefaultForceLoader.loadDefaultBusRoutes();
        }
        File[] routes = busRoutesFolder.listFiles();
        EasyList<EasyList<EasyList<String>>> out = new EasyList<>();
        if (routes == null) {
            throw new RuntimeException("bus routes config is not a folder somehow. this error should be impossible by the way. path is "+busRoutesFolder.getAbsolutePath());
        }
        for (File route : routes) {
            EasyList<EasyList<String>> routeContents;
            try {
                routeContents = SVSaveManager.load(route);
            } catch (IOException e) {
                throw new RuntimeException("couldn't access the contents of the bus route config at "+ route.getAbsolutePath());
            }
            out.add(routeContents);
        }
        return out;
    }
}
