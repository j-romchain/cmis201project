package org.theBestCMISGroup.Roads;

import org.theBestCMISGroup.FileManagement.Utils.OSMMapReader;
import org.theBestCMISGroup.Mapping.Node;
import org.theBestCMISGroup.Mapping.TrafficLane;
import org.theBestCMISGroup.Utils.Storage.EasyList;

public class Road {
    private final EasyList<TrafficLane> allLanes = new EasyList<>();
    private final EasyList<TrafficLane> lanesFromStart = new EasyList<>();
    private final EasyList<TrafficLane> lanesFromEnd = new EasyList<>();
    private final Node startNode;
    private final Node endNode;
    public Road(NonInitRoad nonInitRoad, Node startNode, Node endNode) {
        this(1,nonInitRoad.oneWay?0:1,nonInitRoad.speedLimit, startNode,endNode);
    }
    public Road(OSMMapReader.OSMRoad osmRoad, Node startNode, Node endNode) {
        this(osmRoad.lanesFromStart(startNode.getPos(),endNode.getPos()),osmRoad.lanesFromEnd(startNode.getPos(),endNode.getPos()), osmRoad.getSpeedLimMPH(), startNode,endNode);
    }

    public Road(int lanesFromStart, int lanesFromEnd, int speedlim, Node startNode,Node endNode) {
        this.startNode = startNode;//save a link to the node at each end, and put a link to self in each node. IN THAT ORDER, so the node doesn't reject us.
        startNode.addRoad(this);
        this.endNode = endNode;
        endNode.addRoad(this);
        for (int i = 0; i < lanesFromStart; i++) {
            addLane(new TrafficLane(startNode,endNode,speedlim));
        }
        for (int i = 0; i < lanesFromEnd; i++) {
            addLane(new TrafficLane(endNode,startNode,speedlim));
        }
    }
    private void addLane(TrafficLane lane) {
        allLanes.add(lane);
        if (lane.getStartNode().equals(getStartNode())){
            lanesFromStart.add(lane);
        } else {
            lanesFromEnd.add(lane);
        }
    }

    public TrafficLane[] getTrafficLanesFromStart() {
        return lanesFromStart.toArray(new TrafficLane[0]);
    }

    public TrafficLane[] getTrafficLanesFromEnd() {
        return lanesFromEnd.toArray(new TrafficLane[0]);
    }

    public TrafficLane[] getTrafficLanesToEnd() {
        return getTrafficLanesFromStart();
    }

    public TrafficLane[] getTrafficLanesToStart() {
        return getTrafficLanesFromEnd();
    }

    public Node getStartNode() {
        return startNode;
    }
    public Node getEndNode() {
        return endNode;
    }

    public void tick() {
        for (TrafficLane lane : allLanes) {
            lane.tick();
        }
    }
}
