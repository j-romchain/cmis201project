package org.theBestCMISGroup.Roads;

import org.theBestCMISGroup.Utils.MiscDataTypes.CoordinatePair;
import org.theBestCMISGroup.Utils.MiscDataTypes.Line;
import org.theBestCMISGroup.Utils.Storage.EasyList;

public class NonInitRoad {
    //
    public final Line roadPath;
    public final int speedLimit;
    public final boolean oneWay;
    public final boolean intersectOnNonEnds;

    public NonInitRoad(EasyList<String> roadConfig) {
        this.roadPath=new Line(
                new CoordinatePair(Double.parseDouble(roadConfig.get(0)),Double.parseDouble(roadConfig.get(1))),
                new CoordinatePair(Double.parseDouble(roadConfig.get(2)),Double.parseDouble(roadConfig.get(3))));
        this.speedLimit=Integer.parseInt(roadConfig.get(4));
        this.oneWay=Boolean.parseBoolean(roadConfig.get(5));
        this.intersectOnNonEnds=Boolean.parseBoolean(roadConfig.get(6));
    }

    public double getRoadLengthMiles(){
        return roadPath.getStartPos().getDistanceTo(roadPath.getEndPos());
    }
}
