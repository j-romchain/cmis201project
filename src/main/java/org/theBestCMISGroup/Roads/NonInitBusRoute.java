package org.theBestCMISGroup.Roads;

import org.theBestCMISGroup.Utils.MiscDataTypes.CoordinatePair;
import org.theBestCMISGroup.Utils.Storage.EasyList;

public class NonInitBusRoute {
    public NonInitBusRoute(EasyList<EasyList<String>> busRoute) {
        for (EasyList<String> strings : busRoute) {
            stops.add(new CoordinatePair(strings.get(0),strings.get(1)));
        }
    }
    EasyList<CoordinatePair> stops = new EasyList<>();

    public CoordinatePair[] getStops() {
        return stops.toArray(new CoordinatePair[0]);
    }
}
