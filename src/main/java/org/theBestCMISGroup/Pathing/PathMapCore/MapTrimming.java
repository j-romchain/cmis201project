package org.theBestCMISGroup.Pathing.PathMapCore;

import java.util.HashMap;


public class MapTrimming {

    public HashMap generateTrimmedMap(String startKey, String endKey, HashMap<String, KVMObject> fullMap){
        KVMObject startPoint = fullMap.get(startKey);
        KVMObject endPoint = fullMap.get(endKey);
        double perpendicularM;
        LinearEquation upperBound;
        LinearEquation lowerBound;
        //this makes the M I will need for generating the boundaries later
        if(endPoint.getLatitude() == startPoint.getLatitude()){
            perpendicularM = -((endPoint.getLongitude() - startPoint.getLongitude())/ (endPoint.getLatitude() + .5) - startPoint.getLatitude());
        }
        else{
        perpendicularM = -((endPoint.getLongitude() - startPoint.getLongitude())/ endPoint.getLatitude() - startPoint.getLatitude());
        }
        if(startPoint.getLatitude() > endPoint.getLatitude()){
            upperBound = new LinearEquation(perpendicularM, startPoint.getLongitude(), startPoint.getLatitude());
            lowerBound = new LinearEquation(perpendicularM, endPoint.getLongitude(), endPoint.getLatitude());
        }
        else{
            upperBound = new LinearEquation(perpendicularM, endPoint.getLongitude(), endPoint.getLatitude());
            lowerBound = new LinearEquation(perpendicularM, startPoint.getLongitude(), startPoint.getLatitude());
        }
        return BuildTrimmedMap(upperBound, lowerBound, startPoint, endKey, fullMap);
    }

    private HashMap BuildTrimmedMap(LinearEquation upperBound, LinearEquation lowerBound, KVMObject startPoint, String endKey, HashMap<String, KVMObject> fullMap){
        //start with building next check queue
        //next start search loop with start KVM
        //have loop run until flag boolean is flipped
        //After loop add ring of 3 points around end and start point to trimmed map
        Queue nextCheckQueue = new Queue();
        boolean isEndPointNotInMap = true;
        KVMObject nextPoint;
        nextCheckQueue.enqueue(startPoint);
        HashMap<String, KVMObject> trimmedMap = new HashMap<>();
        while(isEndPointNotInMap){
            nextPoint = nextCheckQueue.dequeue();
            String[] nodeKeys = nextPoint.getConnectedNodesKeys();
            for(String node : nodeKeys){
                if(!trimmedMap.containsKey(node) && NodePositionCheck(upperBound, lowerBound, fullMap.get(node))){
                    trimmedMap.put(node, fullMap.get(node));
                    nextCheckQueue.enqueue(fullMap.get(node));
                }
                if(node.equals(endKey)){
                    isEndPointNotInMap = false;
                }
            }
        }
        Queue cleanUpPoints = new Queue();
        String[] nodeKeys = startPoint.getConnectedNodesKeys();
        for(String node : nodeKeys){
            cleanUpPoints.enqueue(fullMap.get(node));
        }
        nodeKeys = fullMap.get(endKey).getConnectedNodesKeys();
        for(String node : nodeKeys){
            cleanUpPoints.enqueue(fullMap.get(node));
        }
        while(cleanUpPoints.isQueueEmpty()){
            nextPoint = nextCheckQueue.dequeue();
            nodeKeys = nextPoint.getConnectedNodesKeys();
            for(String node : nodeKeys){
                if(!trimmedMap.containsKey(node)){
                    trimmedMap.put(node, fullMap.get(node));
                }
            }
        }
        return trimmedMap;
    }

    private boolean NodePositionCheck(LinearEquation upperBound, LinearEquation lowerBound, KVMObject node){
        if(node.getLongitude() <= upperBound.output(node.getLatitude()) && node.getLongitude() >= lowerBound.output(node.getLatitude())){
            return true;
        }
        else
            return false;

    }

    private class LinearEquation{
        double m;
        double b;
        LinearEquation(double m, double b){
            this.m = m;
            this.b = b;
        }
        LinearEquation(double m, double x, double y){
            this.m = m;
            b = FindB(x,y);
        }

        double FindB(double x, double y){
            return (y - (x *m));
        }
        double output(double x){
            return ((x * m) +b);
        }

    }
}
