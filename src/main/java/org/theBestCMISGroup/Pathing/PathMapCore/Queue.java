package org.theBestCMISGroup.Pathing.PathMapCore;

import org.theBestCMISGroup.People.Person;
import org.theBestCMISGroup.Utils.MiscDataTypes.CoordinatePair;
import org.theBestCMISGroup.Vehicle.Bus;
import org.theBestCMISGroup.Vehicle.Car;

public class Queue {
    private QueueObject head, tail;
    public Queue(){
        head = null;
        tail = null;
    }

    public void enqueue(KVMObject point){
        if(head == null){                 //If head is null that means queue is empty, so tail and head need to be
            head = new QueueObject(new Job(point));  //set to the first object in queue
            tail = head;
        }
        else{
            tail.nextQueue = new QueueObject(new Job(point));
            tail = tail.nextQueue;
        }
    }
    //TODO this method will change depending on how the handlers want pathing information
    public void enqueue(CoordinatePair cords){
        if(head == null){                 //If head is null that means queue is empty, so tail and head need to be
            head = new QueueObject(new Job(cords));  //set to the first object in queue
            tail = head;
        }
        else{
            tail.nextQueue = new QueueObject(new Job(cords));
            tail = tail.nextQueue;
        }
    }

    public void enqueue(Person person){
        if(head == null){                 //If head is null that means queue is empty, so tail and head need to be
            head = new QueueObject(new Job(person));  //set to the first object in queue
            tail = head;
        }
        else{
            tail.nextQueue = new QueueObject(new Job(person));
            tail = tail.nextQueue;
        }
    }
    public void enqueue(Car car){
        if(head == null){                 //If head is null that means queue is empty, so tail and head need to be
            head = new QueueObject(new Job(car));  //set to the first object in queue
            tail = head;
        }
        else{
            tail.nextQueue = new QueueObject(new Job(car));
            tail = tail.nextQueue;
        }
    }
    public void enqueue(Bus bus){
        if(head == null){                 //If head is null that means queue is empty, so tail and head need to be
            head = new QueueObject(new Job(bus));  //set to the first object in queue
            tail = head;
        }
        else{
            tail.nextQueue = new QueueObject(new Job(bus));
            tail = tail.nextQueue;
        }
    }
    public void reverseEnqueue(CoordinatePair cords){
        if(head == null){                 //If head is null that means queue is empty, so tail and head need to be
            head = new QueueObject(new Job(cords));  //set to the first object in queue
            tail = head;
        }
        else{
            QueueObject holdOver = head;
            head = new QueueObject(new Job(cords));
            head.nextQueue = head;
        }
    }
    public KVMObject dequeue(){
        //This job should never be called when the queue is empty.
        //If it is called when the queue is empty then there is a logic flow error earlier in the program
        Job returnJob;
        if(head.getJob() == tail.getJob()){
            returnJob = head.getJob();
            head = null;
            tail = null;
            return returnJob.getPointToCheck();
        }
        returnJob = head.getJob();  //if head has null job, will exit code here
        head = head.nextQueue;
        return returnJob.getPointToCheck();
    }
    public CoordinatePair cordDequeue(){
        Job returnJob;
        if(head.getJob() == tail.getJob()){
            returnJob = head.getJob();
            head = null;
            tail = null;
            return returnJob.getCords();
        }
        returnJob = head.getJob();  //if head has null job, will exit code here
        head = head.nextQueue;
        return returnJob.getCords();
    }

    public Person personDequeue(){
        Job returnJob;
        if(head.getJob() == tail.getJob()){
            returnJob = head.getJob();
            head = null;
            tail = null;
            return returnJob.getPerson();
        }
        returnJob = head.getJob();  //if head has null job, will exit code here
        head = head.nextQueue;
        return returnJob.getPerson();
    }
    public Car carDequeue(){
        Job returnJob;
        if(head.getJob() == tail.getJob()){
            returnJob = head.getJob();
            head = null;
            tail = null;
            return returnJob.getCar();
        }
        returnJob = head.getJob();  //if head has null job, will exit code here
        head = head.nextQueue;
        return returnJob.getCar();
    }
    public Bus busDequeue(){
        Job returnJob;
        if(head.getJob() == tail.getJob()){
            returnJob = head.getJob();
            head = null;
            tail = null;
            return returnJob.getBus();
        }
        returnJob = head.getJob();  //if head has null job, will exit code here
        head = head.nextQueue;
        return returnJob.getBus();
    }
    public boolean isQueueEmpty(){ //checks to see if head is null to check is queue is empty
        if(head == null)
            return true;
        else
            return false;
    }
    public void combineQueues(Queue queueToAdd){
        //this method adds the queueToAdd to the end of the queue this method happens within
        tail.nextQueue = queueToAdd.head;
    }

    public int queueLength(){
        //queueSize is set to one because the while loop will end before adding the tail item to count
        int queueSize = 1;
        QueueObject index = head;
        while(index.getNextQueue() != null){
            queueSize++;
            index = index.nextQueue;
        }
        return queueSize;
    }

    protected class QueueObject{
        private Job job;
        private QueueObject nextQueue;
        public QueueObject(Job job){
            this.job = job;
            nextQueue = null;
        }
        public Job getJob(){
            return job;
        }
        public QueueObject getNextQueue(){
            return nextQueue;
        }
    }
}
