package org.theBestCMISGroup.Pathing.PathMapCore;

import org.theBestCMISGroup.People.Person;
import org.theBestCMISGroup.Utils.MiscDataTypes.CoordinatePair;
import org.theBestCMISGroup.Vehicle.Bus;
import org.theBestCMISGroup.Vehicle.Car;

public class Job {
    private KVMObject pointToCheck;
    //TODO this method will change depending on how the handlers want pathing information
    private CoordinatePair cords;
    private Person person;
    private Car car;
    private Bus bus;

    public Job(KVMObject pointToCheck){
        this.pointToCheck = pointToCheck;
    }
    //TODO this method will change depending on how the handlers want pathing information
    public Job(CoordinatePair cords){this.cords = cords;}
    public Job(Person person){this.person = person;}
    public Job(Car car){this.car = car;}
    public Job(Bus bus){this.bus = bus;}

    public KVMObject getPointToCheck() {
        return pointToCheck;
    }
    //TODO this method will change depending on how the handlers want pathing information
    public CoordinatePair getCords() {return cords;}

    public Person getPerson() {return person;}
    public Car getCar(){return car;}
    public Bus getBus(){return bus;}
}
