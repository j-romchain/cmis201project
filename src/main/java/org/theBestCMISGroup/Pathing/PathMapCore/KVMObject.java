package org.theBestCMISGroup.Pathing.PathMapCore;

import org.theBestCMISGroup.Mapping.TrafficLane;
import org.theBestCMISGroup.Mapping.Node;
import org.theBestCMISGroup.Roads.Road;
import org.theBestCMISGroup.Utils.MiscDataTypes.CoordinatePair;

public class KVMObject {
    //This object is for keeping certain information for the pathing HashMap
    //TODO if UUID is used for Nodes the name's data type might change

    private String nameOfLocation;
    private CoordinatePair cords;
    private String[] connectedNodesKeys;
    private Road[] roadReferences;
    private TrafficLane[] trafficLaneReferences;
    private Node node;

    public KVMObject(String nameOfLocation, CoordinatePair cords, String[] connectedNodesKeys, Road[] roadReferences, TrafficLane[] trafficLaneReferences, Node node){
        this.nameOfLocation = nameOfLocation;
        this.cords = cords;
        this.connectedNodesKeys = connectedNodesKeys;
        this.roadReferences = roadReferences;
        this.trafficLaneReferences = trafficLaneReferences;
        this.node = node;
    }
    public String getNameOfLocation() {return nameOfLocation;}
    public void setNameOfLocation(String nameOfLocation) {this.nameOfLocation = nameOfLocation;}
    public double getLongitude() {return cords.getLatitude();}
    public double getLatitude() {return cords.getLongitude();}
    public CoordinatePair getCords(){return cords;}
    public String[] getConnectedNodesKeys() {return connectedNodesKeys;}
    public void setConnectedNodesKeys(String[] connectedNodes) {this.connectedNodesKeys = connectedNodes;}
    public Road[] getRoadReferences() {return roadReferences;}
    public void setRoadReferences(Road[] roadReferences) {this.roadReferences = roadReferences;}
    public TrafficLane[] getTrafficLaneReferences() {return trafficLaneReferences;}
    public void setTrafficLaneReferences(TrafficLane[] trafficLaneReferences) {this.trafficLaneReferences = trafficLaneReferences;}

    public Node getNode() {return node;}
}
