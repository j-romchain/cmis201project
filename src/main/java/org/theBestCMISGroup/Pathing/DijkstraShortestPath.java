package org.theBestCMISGroup.Pathing;

import org.theBestCMISGroup.Mapping.Node;
import org.theBestCMISGroup.Mapping.TrafficLane;
import org.theBestCMISGroup.Pathing.PathMapCore.KVMObject;

import java.util.Collection;
import java.util.HashMap;

public class DijkstraShortestPath {

    public Route RunDijkstraShortestPathAlgorithm(HashMap<String ,KVMObject> map, String startKey, String endKey){
        //setting up all needed tables for the algorithm
        HashMap<String, String> settledNodes = new HashMap<>();
        HashMap<String, String> unsettledNodes = new HashMap<>();
        HashMap<String, TableObject> pathingTable = GenerateBlankPathTable(map);
        //setting up starting variables for algorithm
        unsettledNodes.put(startKey, startKey);
        boolean isEndPointNotInPath = true;
        String nextNodeToEvaluate = startKey;
        String keyStorage = ""; //this String will be used and explained later in the code
        pathingTable.get(startKey).setShortestPathFromStart(0.0);
        while(isEndPointNotInPath){
            //evaluates current shortest key
            for(TrafficLane trafficLane : map.get(nextNodeToEvaluate).getTrafficLaneReferences()){
                //checks to see if there is a new shortest path to the node at the end of the current trafficLane
                if(IsNewPathShorter((pathingTable.get(nextNodeToEvaluate).getShortestPathFromStart())+createWeightedValue(trafficLane),
                        pathingTable.get(trafficLane.getEndPos().toString()))){
                    pathingTable.get(trafficLane.getEndPos().toString()).setShortestPathFromStart(
                            pathingTable.get(nextNodeToEvaluate).getShortestPathFromStart()+createWeightedValue(trafficLane));
                }
                //adds all nodes that are not un either settled or unsettled nodes to unsettled nodes
                if(!settledNodes.containsKey(trafficLane.getEndPos().toString())){
                    if(!unsettledNodes.containsKey(trafficLane.getEndPos().toString())) {
                        unsettledNodes.put(trafficLane.getEndPos().toString(),trafficLane.getEndPos().toString());
                    }
                }
                if(!settledNodes.containsKey(trafficLane.getEndPos().toString())){
                    keyStorage = trafficLane.getEndPos().toString();
                }
            }//for loop
            //removes evaluated node from unsettledNodes and moves it to settledNodes
            unsettledNodes.remove(nextNodeToEvaluate);
            settledNodes.put(nextNodeToEvaluate,nextNodeToEvaluate);
            //changes nextNodeToEvaluate to new unsettledNode value to prevent reevaluation of already settled node
            nextNodeToEvaluate = keyStorage;
            //to be used to update the nextNodeToEvaluate for the following block of code
            if(!unsettledNodes.containsKey(nextNodeToEvaluate)){
                nextNodeToEvaluate = DijkstrasLoopPrevention(unsettledNodes);
            }

            //sets next shortest key
            for(String key : unsettledNodes.values()){
                if(pathingTable.get(key).getShortestPathFromStart() < pathingTable.get(nextNodeToEvaluate).getShortestPathFromStart()){
                    nextNodeToEvaluate = key;
                }
            }

            //check for end point
            if(settledNodes.containsKey(endKey)){
                isEndPointNotInPath = false;
            }
        }//while loop
        String nextPointInRoute = endKey;
        Route outputRoute = new Route();
        outputRoute.RouteAdd(map.get(nextPointInRoute).getCords());
        while(!nextPointInRoute.equals(startKey)){ //Loop used to build the route
            outputRoute.RouteAdd(map.get(pathingTable.get(nextPointInRoute).getPreviousNodeKey()).getCords());
            nextPointInRoute = pathingTable.get(nextPointInRoute).getPreviousNodeKey();
        }

        Node endPointOfTrafficLane = pathingTable.get(endKey).getNode();
        Node startPointOfTrafficLane = pathingTable.get(pathingTable.get(endKey).previousNodeKey).node;
        double timeToTraverse = 0.0;
        while(!startPointOfTrafficLane.getPos().toString().equals(startKey)){ //Loop to make the estimate time to traverse
            //TODO change if trafficLanes no longer have time to traverse
            timeToTraverse += startPointOfTrafficLane.getFastestDirectWayOutTo(endPointOfTrafficLane).timeToTransverseSeconds();
            endPointOfTrafficLane = startPointOfTrafficLane;
            startPointOfTrafficLane = pathingTable.get(pathingTable.get(endKey).previousNodeKey).node;
        }
        outputRoute.setEstimatedTimeToTraverse((long)timeToTraverse);
        return outputRoute;
    }

    //This method is used to gather any information to properly weight a road
    private double createWeightedValue(TrafficLane trafficLane){
        //TODO make sure this method gets full implemented
        return trafficLane.timeToTransverseSeconds();
    }

    private HashMap<String, TableObject> GenerateBlankPathTable(HashMap<String ,KVMObject> map){
        HashMap<String, TableObject> output = new HashMap<>();
        for(KVMObject object : map.values()){
            output.put(object.getNameOfLocation(), new TableObject(object.getNode()));
        }
        return output;
    }

    private boolean IsNewPathShorter(double valuesOfNewPath, TableObject object){
        if(valuesOfNewPath < object.shortestPathFromStart){
            return true;
        }
        else
            return false;

    }

    private String DijkstrasLoopPrevention(HashMap<String, String> table){
        Collection<String> tempArray = table.values();
        String output = tempArray.iterator().next();
        return output;
    }
    private class TableObject{
        //this class is for use in the HashMap for Dijkstra's table
        private String previousNodeKey;
        private double shortestPathFromStart;
        private Node node;

        TableObject(Node node){
            previousNodeKey = "";
            shortestPathFromStart = Double.MAX_VALUE;
            this.node = node;
        }

        public String getPreviousNodeKey() {
            return previousNodeKey;
        }

        public void setPreviousNodeKey(String previousNode) {
            this.previousNodeKey = previousNode;
        }

        public double getShortestPathFromStart() {
            return shortestPathFromStart;
        }

        public void setShortestPathFromStart(double shortestPathFromStart) {
            this.shortestPathFromStart = shortestPathFromStart;
        }
        public Node getNode() {
            return node;
        }
    }
}
