package org.theBestCMISGroup.Pathing;

import org.theBestCMISGroup.Pathing.PathMapCore.Queue;
import org.theBestCMISGroup.Utils.MiscDataTypes.CoordinatePair;

public class Route {
    private Queue nodeRoute;
    private long estimatedTimeToTraverse;

    //routes will be built with the end point at the start and the start as the tail object
    Route (){
        nodeRoute = new Queue();
        estimatedTimeToTraverse = 0;
    }
    public Queue getNodeRoute(){return nodeRoute;}
    public void RouteAdd(CoordinatePair cords){
        nodeRoute.reverseEnqueue(cords);
    }
    public CoordinatePair carRouteRetrevial(){
        return nodeRoute.cordDequeue();
    }
    public CoordinatePair busRouteRetrevial(){
        CoordinatePair tempCords = nodeRoute.cordDequeue();
        nodeRoute.enqueue(tempCords);
        return tempCords;
    }
    public void setEstimatedTimeToTraverse(long estimatedTimeToTraverse){
        this.estimatedTimeToTraverse = estimatedTimeToTraverse;
    }

    public void CombineRoutes(Route routeToAdd){
        //this method adds routeToAdd to the end of the route this method call is happening in
        nodeRoute.combineQueues(routeToAdd.getNodeRoute());
    }
}
