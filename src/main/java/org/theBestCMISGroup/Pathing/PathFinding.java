package org.theBestCMISGroup.Pathing;

import org.theBestCMISGroup.Mapping.TrafficLane;
import org.theBestCMISGroup.Mapping.Node;
import org.theBestCMISGroup.Pathing.PathMapCore.KVMObject;
import org.theBestCMISGroup.Roads.Road;
import org.theBestCMISGroup.Utils.Storage.EasyList;

import java.util.HashMap;

public class PathFinding {

    private HashMap<String, KVMObject> fullMap;
    private DijkstraShortestPath DSP = new DijkstraShortestPath();

    public PathFinding(EasyList<Node> fullNodeMap){
        fullMap = Initalize(fullNodeMap);
    }

    private HashMap<String, KVMObject> Initalize(EasyList<Node> fullNodeMap){
        fullMap = new HashMap<>(100);
        for(Node node : fullNodeMap){
            TrafficLane[] tempArray = node.getWaysOut().toArray(new TrafficLane[0]);
            String[] tempStringArray = new String[tempArray.length];
            int index = 0;
            //TODO this might change depending on how trafficLanes act in the final product
            for(TrafficLane trafficLane : tempArray){
                tempStringArray[index] = tempArray[0].getEndNode().getPos().toString();
            }
            fullMap.put(node.getPos().toString(), new KVMObject(node.getPos().toString(), node.getPos(), tempStringArray
                    ,node.getConnectedRoads().toArray(new Road[0]), node.getWaysOut().toArray(new TrafficLane[0]), node));
        }
        return fullMap;
    }

    //TODO this method will change depending on how the handlers want pathing information
    public Route CreateCarRoute(Node startPoint, Node endPoint){
        return DSP.RunDijkstraShortestPathAlgorithm(fullMap,startPoint.getPos().toString(),endPoint.getPos().toString());
    }
    public Route CreateBusRoute(Node[] busRouteStops, Node startPoint){
        Node[] routedNodes = new Node[busRouteStops.length];
        int routeNodeIndex = 0;
        Node nextNodeToRoute = clostestNode(busRouteStops, routedNodes,startPoint);
        routedNodes[routeNodeIndex] = nextNodeToRoute;
        routeNodeIndex++;
        Route busRoute = DSP.RunDijkstraShortestPathAlgorithm(fullMap, startPoint.getPos().toString(), nextNodeToRoute.getPos().toString());
        while(!AreAllNodesRouted(routedNodes, busRouteStops)){
            nextNodeToRoute = clostestNode(busRouteStops, routedNodes,startPoint);
            busRoute.CombineRoutes(DSP.RunDijkstraShortestPathAlgorithm(fullMap, startPoint.getPos().toString(), nextNodeToRoute.getPos().toString()));
            routedNodes[routeNodeIndex] = nextNodeToRoute;
            routeNodeIndex++;
        }
        return busRoute;
    }

    private Node clostestNode(Node[] array, Node[] nodesAlreadyRouted,Node startPoint){
        Node closestNode = null;
        double clostestNodeDistance = Double.MAX_VALUE;
        for(Node node : array){
            if(startPoint.getPos().getDistanceTo(node.getPos()) < clostestNodeDistance && isNodeNotInList(nodesAlreadyRouted, node)){
                clostestNodeDistance = startPoint.getPos().getDistanceTo(node.getPos());
                closestNode = node;
            }
        }
        return closestNode;
    }
    private boolean AreAllNodesRouted(Node[] routedNodes, Node[] allNodes){
        boolean matchFound;
        for(Node routedNode : routedNodes){
            matchFound = false;
            for(Node node : allNodes){
                if(routedNode.getPos().equals(node)){
                    matchFound = true;
                }}
            if(!matchFound){
                return false;
            }}
        return true;
    }
    private boolean isNodeNotInList(Node[] array, Node node){
        for(Node n : array){
            if(n!=null && n.getPos().equals(node.getPos())){
                return false;
            }
        }
        return true;
    }
}
