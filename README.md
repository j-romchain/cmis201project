This is a group CMIS 202 project about traffic simulation for the objective of reducing traffic.

For Group Members: 
to initialize the connection to Intellij:

1. install git if it isn't allready installed. (https://git-scm.com/)
2. install Intellij if it isn't allready installed. (make sure it's community or ultimate based on if you own ultimate) (https://www.jetbrains.com/idea/?var=1)
3. make sure Intellij uses the manual installed git as opposed to its own git.
4. install glab. (https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/#install-the-cli)
5. login to glab. (https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/#authenticate-with-gitlab) (token works better than web) (see https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html to do a token)
6. open intellij and clone/open from version controll https://gitlab.com/j-romchain/cmis202project.git .
7. hit alt+9 and then the Remote and origin dropdowns in the left pane.
8. right click and select "Checkout" on the "Branch" with your gitlab username.
9. you may now edit code, and commit and push it 

Alternatively: (might not work)

1. install Intellij if it isn't allready installed. (make sure it's community or ultimate based on if you own ultimate) (https://www.jetbrains.com/idea/?var=1)
2. open intellij and clone/open from version controll https://gitlab.com/j-romchain/cmis202project.git .
3. click the gitlab icon on the left sidebar
4. click login and follow instructions.